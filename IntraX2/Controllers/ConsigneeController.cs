﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Data;
using System.Data.Entity;
using IntraX2.Models;

namespace IntraX2.Controllers
{
    public class ConsigneeController : BaseController
    {
        //instantiate pathfinderEntities data model entity container, defined in the Models.
        private pathfinderEntities db = new pathfinderEntities();

        //set database connection
        SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ConnectionString);
        
        // GET: /Consignee/

        public ActionResult Index()
        {
            var StateListResult = statesList.GetStates();
            var StateSelectList = new SelectList(StateListResult, "StateCode", "StateName");
            ViewBag.StatesList = StateSelectList;

            var CountryListResult = countryList.GetCountryList();
            var CountrySelectList = new SelectList(CountryListResult, "CountryId", "CountryName");
            ViewBag.CountryList = CountrySelectList;

            /*
            var ConsigneeListResult = consigneeList.GetAllConsignees();
            var ConsigneeSelectList = new SelectList(ConsigneeListResult, "consignee_id", "name");
            ViewBag.ConsigneeList = ConsigneeSelectList;
            */

            return View(new IntraX2.Models.consigneeModel());
        }

         // POST: /Consignee/Index
        [HttpPost]
        public ActionResult Index(consigneeModel cm, FormCollection fc)
        {
            if (Convert.ToString(fc["newConsigneeFlag"]) == "Y")
            {
                db.Entry(cm.m_consignee).State = EntityState.Added;
                db.SaveChanges();
            }
            else
            {
                db.Entry(cm.m_consignee).State = EntityState.Modified;
                db.SaveChanges();
            }
          
            return RedirectToAction("Index", "Consignee");
        }

        [HttpPost]
        public string UpdateLocation(decimal consigneeId, int locationId, string location, string address1, string address2, string city, string state, string zip)
        {
            string msg;
            consignee_locations clObj = db.consignee_locations.Find(consigneeId, locationId);
            if (clObj != null) //record found
            {
                clObj.name = location;
                clObj.address_1 = address1;
                clObj.address_2 = address2;
                clObj.city = city;
                clObj.state = state;
                clObj.zip = zip;

                db.SaveChanges();
                msg = "Row saved successfully";
            }
            else //add new record
            {
                consignee_locations newRow = new consignee_locations();
                newRow.consignee_id = consigneeId;
                newRow.seq_number = locationId;
                newRow.name = location;
                newRow.address_1 = address1;
                newRow.address_2 = address2;
                newRow.city = city;
                newRow.state = state;
                newRow.zip = zip;

                /*insert row to database*/
                try
                {
                    if (ModelState.IsValid)
                    {
                        db.consignee_locations.Add(newRow);
                        db.SaveChanges();
                        msg = "Row added successfully";
                    }
                    else
                    {
                        msg = "Validation data not successfull";
                    }
                }
                catch (Exception ex)
                {
                    msg = "Error occured:" + ex.Message;
                }
            }
            return msg;
        }

        //soft delete a consignee
        [HttpPost]
        public string DeleteConsignee(decimal consigneeId)
        {
            string msg = "";

            try
            {
                if (ModelState.IsValid)
                {
                    //execute the stored procedure to soft delete the carrier and it's associated loctions and contacts
                    db.DeleteConsignee(consigneeId);

                    msg = "Row deleted successfully";
                }
                else
                {
                    msg = "Validation data not successfull";
                }
            }
            catch (Exception ex)
            {
                msg = "Error occured:" + ex.Message;
            }

            return msg;
        }

        //soft delete a location
        [HttpPost]
        public string DeleteLocation(int consigneeId, int locationId)
        {
            string msg = "";
            consignee_locations clObj = db.consignee_locations.Find(consigneeId, locationId);
            if (clObj != null)
            {
                clObj.record_status = "0";
            }

            try
            {
                if (ModelState.IsValid)
                {
                    db.SaveChanges();
                    msg = "Delete successful";
                }
                else
                {
                    msg = "Validation data not successful";
                }
            }
            catch (Exception ex)
            {
                msg = "Error occured:" + ex.Message;
            }

            return msg;
        }

        public ActionResult GetMaxConsigneeLocationSeq(int consigneeId)
        {
            DataTable dtSeq = new DataTable();
            var maxval = "";

            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ToString()))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("GetMaxConsigneeLocationSeq", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@intConsigneeID", SqlDbType.Int).Value = consigneeId;
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    dtSeq.Load(reader);
                    maxval = Convert.ToString(dtSeq.Rows[0]["sequence"]);
                }

                reader.Close();
                cmd.Dispose();
            }; //eo using

            var jsonData = new
            {
                maxval = Convert.ToInt16(maxval) + 1
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        } //eo GetMaxConsigneeLocationSeq

        [HttpPost]
        public string UpdateContact(decimal consigneeId, int locationId, int contactId, string phone, string ext, string fax, string email, string name)
        {
            string msg;
            consignee_location_contacts clcObj = db.consignee_location_contacts.Find(consigneeId, locationId, contactId);
            if (clcObj != null) //record found
            {
                clcObj.phone = phone;
                clcObj.extension = ext;
                clcObj.fax = fax;
                clcObj.email = email;
                clcObj.name = name;
                msg = "Row saved successfully";
            }
            else //add new record
            {
                consignee_location_contacts newRow = new consignee_location_contacts();
                newRow.consignee_id = consigneeId;
                newRow.seq_number = locationId;
                newRow.contact_id = contactId;
                newRow.name = name;
                newRow.phone = phone;
                newRow.extension = ext;
                newRow.fax = fax;
                newRow.email = email;

                /*insert row to database*/
                try
                {
                    if (ModelState.IsValid)
                    {
                        db.consignee_location_contacts.Add(newRow);
                        msg = "Row added successfully";
                    }
                    else
                    {
                        msg = "Validation data not successfull";
                    }
                }
                catch (Exception ex)
                {
                    msg = "Error occured:" + ex.Message;
                }
            }

            db.SaveChanges();
            return msg;
        }

        //soft delete a contact
        [HttpPost]
        public string DeleteContact(int consigneeId, int locationId, int contactId)
        {
            string msg = "";
            consignee_location_contacts clcObj = db.consignee_location_contacts.Find(consigneeId, locationId, contactId);
            if (clcObj != null)
            {
                clcObj.record_status = "0";
            }

            try
            {
                if (ModelState.IsValid)
                {
                    db.SaveChanges();
                    msg = "Delete successful";
                }
                else
                {
                    msg = "Validation data not successful";
                }
            }
            catch (Exception ex)
            {
                msg = "Error occured:" + ex.Message;
            }

            return msg;
        }

        public ActionResult GetMaxConsigneeContactSeq(int consigneeId, int locationId)
        {
            var nextSeqNum = "";

            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ToString()))
            {
                System.Data.Objects.ObjectParameter RetNextSeqNum = new System.Data.Objects.ObjectParameter("intNextVal", typeof(int)); //variable intNextVal is defined in SP as OUTPUT
                db.GetMaxConsigneeContactSeq(consigneeId, locationId, RetNextSeqNum);
                nextSeqNum = Convert.ToString(RetNextSeqNum.Value);
            }; //eo using

            var jsonData = new
            {
                nextSeqNum = nextSeqNum
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        } //eo GetMaxConsigneeLocationSeq

        public ActionResult GetConsigneeDetail(Int32 consigneeId)
        {
            DataTable dtConsignee = new DataTable();
            var setupBy = "";
            var setupDate = "";
            var name = "";
            
            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ToString()))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("GetConsigneeDetail", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@intConsigneeID", SqlDbType.Int).Value = consigneeId;
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    dtConsignee.Load(reader);
                    setupDate = Convert.ToString(dtConsignee.Rows[0]["setupDate"]);
                    setupBy = Convert.ToString(dtConsignee.Rows[0]["setup_by"]);
                    name = Convert.ToString(dtConsignee.Rows[0]["name"]);
                }
                reader.Close();
                cmd.Dispose();
            }; //eo using

            var jsonData = new
            {
                consigneeID = Convert.ToString(consigneeId),
                setupDate = setupDate,
                setupBy = setupBy,
                name = name
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        } //eo GetCarrierDetail

        public ActionResult GetConsigneeLocationDetail(Int32 consigneeId, Int16 locationId)
        {
            DataTable dtLocation = new DataTable();
            var name = "";
            var address1 = "";
            var address2 = "";
            var city = "";
            var state = "";
            var zip = "";
            var country = "";

            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ToString()))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("GetConsigneeLocationDetail", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@intConsigneeID", SqlDbType.Int).Value = consigneeId;
                cmd.Parameters.Add("@intLocationID", SqlDbType.Int).Value = locationId;
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    dtLocation.Load(reader);
                    name = Convert.ToString(dtLocation.Rows[0]["name"]);
                    address1 = Convert.ToString(dtLocation.Rows[0]["address_1"]);
                    address2 = Convert.ToString(dtLocation.Rows[0]["address_2"]);
                    city = Convert.ToString(dtLocation.Rows[0]["city"]);
                    state = Convert.ToString(dtLocation.Rows[0]["state"]);
                    zip = Convert.ToString(dtLocation.Rows[0]["zip"]);
                    country = Convert.ToString(dtLocation.Rows[0]["country"]);
                }
                reader.Close();
                cmd.Dispose();
            }; //eo using

            var jsonData = new
            {
                name = name,
                address1 = address1,
                address2 = address2,
                city = city,
                state = state,
                zip = zip,
                country = country
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        } //eo GetConsigneeLocationDetail

        public ActionResult GetConsigneeLocationContactDetail(Int32 consigneeId, Int16 locationId, Int16 contactId)
        {
            DataTable dtContact = new DataTable();
            var phone = "";
            var extension = "";
            var fax = "";
            var email = "";

            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ToString()))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("GetConsigneeLocationContactDetail", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@intConsigneeID", SqlDbType.Int).Value = consigneeId;
                cmd.Parameters.Add("@intLocationID", SqlDbType.Int).Value = locationId;
                cmd.Parameters.Add("@intContactID", SqlDbType.Int).Value = contactId;
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    dtContact.Load(reader);
                    phone = Convert.ToString(dtContact.Rows[0]["phone"]);
                    extension = Convert.ToString(dtContact.Rows[0]["extension"]);
                    fax = Convert.ToString(dtContact.Rows[0]["fax"]);
                    email = Convert.ToString(dtContact.Rows[0]["email"]);
                }
                reader.Close();
                cmd.Dispose();
            }; //eo using

            var jsonData = new
            {
                phone = phone,
                extension = extension,
                fax = fax,
                email = email
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        } //eo GetConsigneeLocationContactDetail

        //get consignee list
        public JsonResult GetConsigneeList()
        {
            var list = db.basp_GetConsigneeListData().ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        //get locations of the selected consignee
        public JsonResult GetConsigneeLocations(int consigneeId)
        {
            var list = db.GetConsigneeAllLocations(consigneeId).ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        //get contacts of the select consignee location
        public JsonResult GetConsigneeLocationContacts(int consigneeId, int locationId)
        {
            var list = db.GetConsigneeLocationContacts(consigneeId, locationId).ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetSequenceNumber()
        {
            //Get the next carrier id sequence number
            System.Data.Objects.ObjectParameter RetSeqNum = new System.Data.Objects.ObjectParameter("intSeqNum", typeof(int)); //variable intSeqNum is defined in SP as OUTPUT
            db.GetSeqNumber(RetSeqNum);
            var jsonData = new
            {
                maxval = Convert.ToInt32(RetSeqNum.Value) + 1
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        } //eo GetMaxCarrierSeq
    }
}
