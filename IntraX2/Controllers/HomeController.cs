﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IntraX2.Models;
using System.Data.SqlClient;

namespace IntraX2.Controllers
{
    public class HomeController : BaseController
    {
        //instantiate pathfinderEntities data model entity container, defined in the Models.
        private pathfinderEntities db = new pathfinderEntities();

        //set database connection
        SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ConnectionString);
        
        //create an empty cookies
        HttpCookie userIDCookie = new HttpCookie("userIDCookie");
        HttpCookie userCookie = new HttpCookie("userCookie");
        HttpCookie userPassCookie = new HttpCookie("userPassCookie");
        HttpCookie userLoginIDCookie = new HttpCookie("userLoginIDCookie");
        HttpCookie userNameCookie = new HttpCookie("userNameCookie");
        HttpCookie userRoleIDCookie = new HttpCookie("userRoleIDCookie");
        
        public ActionResult Index()
        {
            //userIDCookie.Value = "x0";
            //Response.Cookies.Add(userIDCookie);
            ViewBag.Message = "";

            //create and assign an action to the cookie
            HttpCookie actionCookie = new HttpCookie("actionCookie");
            actionCookie.Value = "home";
            Response.Cookies.Add(actionCookie);
            
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Login()
        {
            ViewBag.Message = "";
            //create an empty cookie for customer name
            //HttpCookie userIDCookie = new HttpCookie("userIDCookie");
            userIDCookie.Value = "x0";
            Response.Cookies.Add(userIDCookie);

            //create and assign an action to the cookie
            HttpCookie actionCookie = new HttpCookie("actionCookie");
            actionCookie.Value = "login";
            Response.Cookies.Add(actionCookie);

            return View();
        }

        [HttpPost, ValidateInput(false)]
        [AllowAnonymous]
        public ActionResult Login(Login model)
        {
            if (ModelState.IsValid)
            {
                if (model.IsUserExist(model.UserID, model.UserPassword))
                {
                    //assign values to cookies
                    userIDCookie.Value = Convert.ToString(model.UserID);
                    userIDCookie.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(userIDCookie);

                    userCookie.Value = Convert.ToString(model.UserID);
                    userCookie.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(userCookie);

                    userPassCookie.Value = Convert.ToString(model.UserPassword);
                    userPassCookie.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(userPassCookie);

                    userLoginIDCookie.Value = Convert.ToString(model.LoginID);
                    userLoginIDCookie.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(userLoginIDCookie);

                    userNameCookie.Value = Convert.ToString(model.LoginID);
                    userNameCookie.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(userNameCookie);

                    userRoleIDCookie.Value = Convert.ToString(model.UserRoleID);
                    userRoleIDCookie.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(userRoleIDCookie);
                    
                    return RedirectToAction("Index", "Home");
                }
            }
            // If we got this far, something failed, redisplay form
            //ModelState.AddModelError("", "The user name or password provided is incorrect.");
            ModelState.AddModelError("", "");
            return View(model);
        }

        public ActionResult GetSequenceNumber()
        {
            //Get the next carrier id sequence number
            System.Data.Objects.ObjectParameter RetSeqNum = new System.Data.Objects.ObjectParameter("intSeqNum", typeof(int)); //variable intSeqNum is defined in SP as OUTPUT
            db.GetSeqNumber(RetSeqNum);
            var jsonData = new
            {
                maxval = Convert.ToInt32(RetSeqNum.Value) + 1
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        } //eo GetMaxCarrierSeq
    }
}
