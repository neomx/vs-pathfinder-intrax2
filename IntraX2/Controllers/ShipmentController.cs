﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using System.Data.Entity;
using System.Net;
using IntraX2.CarrierConnectService;
using IntraX2.RateWareServices;
using Newtonsoft.Json;
using System.Diagnostics; //for Debug.Writeline
using Microsoft.Reporting.WebForms;
using System.IO;
using IntraX2.Models;

namespace IntraX2.Controllers
{
    public class ShipmentController : BaseController
    {
        //instantiate pathfinderEntities data model entity container, defined in the Models.
        private pathfinderEntities db = new pathfinderEntities();

        //set database connection
        SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ConnectionString);

        [HttpPost]
        public string UpdateCustomerInvoicedData(string booking, DateTime invoicedDate)
        {
            string msg;
            shipment shipmentObj = db.shipments.Find(booking);
            if (shipmentObj != null)
            {
                shipmentObj.customer_invoiced = true;
                shipmentObj.customer_invoice_date = invoicedDate;
                
                try
                {
                    if (ModelState.IsValid)
                    {
                        db.SaveChanges();
                    }
                    else
                    {
                        msg = "Validation data not successfull";
                    }
                }
                catch (Exception ex)
                {
                    msg = "Error occured:" + ex.Message;
                }
            }
            msg = "Row saved successfully";
            return msg;
        }

        [HttpPost]
        public string UpdateStatusInformation(string booking, string status, DateTime requestedPU, DateTime actualPU, DateTime deliveryETA, DateTime actualDelivery)
        {
            string msg;
            shipment shipmentObj = db.shipments.Find(booking);
            if (shipmentObj != null)
            {
                shipmentObj.status = status;
                shipmentObj.requested_pickup = requestedPU;
                shipmentObj.actual_pu_date = actualPU;
                shipmentObj.delivery_eta = deliveryETA;
                shipmentObj.actual_delivery_date = actualDelivery;

                try
                {
                    if (ModelState.IsValid)
                    {
                        db.SaveChanges();
                    }
                    else
                    {
                        msg = "Validation data not successfull";
                    }
                }
                catch (Exception ex)
                {
                    msg = "Error occured:" + ex.Message;
                }
            }
            msg = "Row saved successfully";
            return msg;
        }

        [HttpPost]
        public string CreateConsigneeLocationContact(Int32 ConsigneeID, Int32 LocationID, string Name, string Phone, string Extension, string Fax, string Email)
        {
            string msg = "";
            Int32 maxval = 0;

            /*get the max consignee location contact sequence*/
            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ToString()))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("GetMaxConsigneeContactSeq", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@intConsigneeID", SqlDbType.Int).Value = ConsigneeID;
                cmd.Parameters.Add("@intLocationID", SqlDbType.Int).Value = LocationID;
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    if (!reader.IsDBNull(reader.GetOrdinal("sequence")))
                    {
                        maxval = reader.GetInt32(0);
                    }
                    else
                    {
                        maxval = 0;
                    }
                }
                reader.Close();
                cmd.Dispose();
            };
            maxval = maxval + 1;

            consignee_location_contacts consContactRow = new consignee_location_contacts();
            consContactRow.consignee_id = ConsigneeID;
            consContactRow.seq_number = LocationID;
            consContactRow.contact_id = maxval;
            consContactRow.name = Name;
            consContactRow.phone = Phone;
            consContactRow.extension = Extension;
            consContactRow.fax = Fax;
            consContactRow.email = Email;

            /*insert row to table*/
            try
            {
                if (ModelState.IsValid)
                {
                    db.consignee_location_contacts.Add(consContactRow);
                    db.SaveChanges();
                    msg = "Row added successfully";
                }
                else
                {
                    msg = "Validation data not successfull";
                }
            }
            catch (Exception ex)
            {
                msg = "Error occured:" + ex.Message;
            }
            return msg;
        }

        [HttpPost]
        public string CreateConsigneeLocation(Int32 ConsigneeID, string Name, string Address1, string Address2, string City, string State, string Zip, string Country, string Notes)
        {
            string msg = "";
            Int32 maxval = 0;

            /*get the max shipper location contact sequence*/
            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ToString()))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("GetMaxConsigneeLocationSeq", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@intConsigneeID", SqlDbType.Int).Value = ConsigneeID;
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    if (!reader.IsDBNull(reader.GetOrdinal("sequence")))
                    {
                        maxval = reader.GetInt32(0);
                    }
                    else
                    {
                        maxval = 0;
                    }
                }
                reader.Close();
                cmd.Dispose();
            };
            maxval = maxval + 1;

            consignee_locations locationRow = new consignee_locations();
            locationRow.consignee_id = ConsigneeID;
            locationRow.seq_number = maxval;
            locationRow.name = Name;
            locationRow.address_1 = Address1;
            locationRow.address_2 = Address2;
            locationRow.city = City;
            locationRow.state = State;
            locationRow.zip = Zip;
            locationRow.country = Country;
            locationRow.notes = Notes;

            /*insert row to table*/
            try
            {
                if (ModelState.IsValid)
                {
                    db.consignee_locations.Add(locationRow);
                    db.SaveChanges();
                    msg = "Row added successfully";
                }
                else
                {
                    msg = "Validation data not successfull";
                }
            }
            catch (Exception ex)
            {
                msg = "Error occured:" + ex.Message;
            }
            return msg;
        }

        [HttpPost]
        public string CreateShipperLocationContact(Int32 ShipperID, Int32 LocationID, string Name, string Phone, string Extension, string Fax, string Email)
        {
            string msg = "";
            Int32 maxval = 0;

            /*get the max shipper location contact sequence*/
            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ToString()))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("GetMaxShipperContactSeq", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@intShipperID", SqlDbType.Int).Value = ShipperID;
                cmd.Parameters.Add("@intLocationID", SqlDbType.Int).Value = LocationID;
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    if (!reader.IsDBNull(reader.GetOrdinal("sequence")))
                    {
                        maxval = reader.GetInt32(0);
                    }
                    else
                    {
                        maxval = 0;
                    }
                }
                reader.Close();
                cmd.Dispose();
            };
            maxval = maxval + 1;

            shipper_location_contacts shpContactRow = new shipper_location_contacts();
            shpContactRow.shipper_id = ShipperID;
            shpContactRow.seq_number = LocationID;
            shpContactRow.contact_id = maxval;
            shpContactRow.name = Name;
            shpContactRow.phone = Phone;
            shpContactRow.extension = Extension;
            shpContactRow.fax = Fax;
            shpContactRow.email = Email;

            /*insert row to table*/
            try
            {
                if (ModelState.IsValid)
                {
                    db.shipper_location_contacts.Add(shpContactRow);
                    db.SaveChanges();
                    msg = "Row added successfully";
                }
                else
                {
                    msg = "Validation data not successfull";
                }
            }
            catch (Exception ex)
            {
                msg = "Error occured:" + ex.Message;
            }
            return msg;
        }

        [HttpPost]
        public string CreateShipperLocation(Int32 ShipperID, string Name, string Address1, string Address2, string City, string State, string Zip, string Country, string Notes)
        {
            string msg = "";
            Int32 maxval = 0;

            /*get the max shipper location sequence*/
            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ToString()))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("GetMaxShipperLocationSeq", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@intShipperID", SqlDbType.Int).Value = ShipperID;
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    if (!reader.IsDBNull(reader.GetOrdinal("sequence")))
                    {
                        maxval = reader.GetInt32(0);
                    }
                    else
                    {
                        maxval = 0;
                    }
                }
                reader.Close();
                cmd.Dispose();
            };
            maxval = maxval + 1;

            shipper_locations locationRow = new shipper_locations();
            locationRow.shipper_id = ShipperID;
            locationRow.seq_number = maxval;
            locationRow.name = Name;
            locationRow.address_1 = Address1;
            locationRow.address_2 = Address2;
            locationRow.city = City;
            locationRow.state = State;
            locationRow.zip = Zip;
            locationRow.country = Country;
            locationRow.notes = Notes;
            
            /*insert row to table*/
            try
            {
                if (ModelState.IsValid)
                {
                    db.shipper_locations.Add(locationRow);
                    db.SaveChanges();
                    msg = "Row added successfully";
                }
                else
                {
                    msg = "Validation data not successfull";
                }
            }
            catch (Exception ex)
            {
                msg = "Error occured:" + ex.Message;
            }
            return msg;
        }

        [HttpPost]
        public string CreateCustomerContact(Int32 CustomerID, string Name, string Phone, string Extension, Int32 RoleID, string Title, string Fax, string Email, string Notes, Int32 UserID)
        {    
            string msg = "";
            Int32 maxval = 0;
           
            /*get the max customer contact sequence*/
            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ToString()))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("GetMaxCustomerContactSeq", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@intCustomerID", SqlDbType.Int).Value = CustomerID;
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    if (!reader.IsDBNull(reader.GetOrdinal("sequence")))
                    {
                        maxval = reader.GetInt32(0);
                    }
                    else
                    {
                        maxval = 0;
                    }
                }
                                
                reader.Close();
                cmd.Dispose();
            };
            maxval = maxval + 1;
            
            CustomerContact contactRow = new CustomerContact();
            contactRow.CustomerContactId = maxval;
            contactRow.CustomerId = CustomerID;
            contactRow.CustContactCodeTypeId = RoleID;
            contactRow.Name = Name;
            contactRow.Phone = Phone;
            contactRow.Ext = Extension;
            contactRow.Fax = Fax;
            contactRow.Email = Email;
            contactRow.JobTitle = Title;
            contactRow.Notes = Notes;
            contactRow.IsActive = true;
            contactRow.CreatedBy = UserID;
            contactRow.CreatedDate = DateTime.Now;
            
            /*insert row to table*/
            try
            {
                if (ModelState.IsValid)
                {
                    db.CustomerContacts.Add(contactRow);
                    db.SaveChanges();
                    msg = "Row added successfully";
                }
                else
                {
                    msg = "Validation data not successfull";
                }
            }
            catch (Exception ex)
            {
                msg = "Error occured:" + ex.Message;
            }
            return msg;
        }
       
        //get locations of the selected shipper
        public JsonResult GetShipmentsByParams(DateTime fromdate, DateTime todate, string status, string mode, string exclude, string customer, string shipper, string consignee, string carrier, string salesrep)
        {
            if (status.Length == 0)
            {
                status = "ALL";
            }
            
            if (mode.Length == 0 )
            {
                mode = "0";
            }

            if (exclude.Length == 0)
            {
                exclude = "NONE";
            }

            if (customer.Length == 0)
            {
                customer = "0";
            }

            if (shipper.Length == 0)
            {
                shipper = "0";
            }

            if (consignee.Length == 0)
            {
                consignee = "0";
            }

            if (carrier.Length == 0)
            {
                carrier = "0";
            }

            if (salesrep.Length == 0)
            {
                salesrep = "0";
            }

            var list = db.GetShipmentsListByParams(fromdate, todate, status, mode, exclude, customer, shipper, consignee, carrier, salesrep).ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public string GetUserFilter(String user, String password)
        {
            //build datatable
            DataTable dtFilter = CreateDataTable("SELECT filter FROM saved_filters WHERE username='" + user + "' AND password='" + password + "'");

            if (dtFilter.Rows.Count > 0)
            {
                return Convert.ToString(dtFilter.Rows[0]["filter"]);
            }
            else {
                return "";
            }
        }

        [HttpPost]
        public string UpdateUserFilter(List<String> User, List<String> Password, List<String> Filter)
        {
            saved_filters filterObj = db.saved_filters.Find(Convert.ToString(User[0]), Convert.ToString(Password[0]), 1);
            if (filterObj == null)
            {
                saved_filters filterRow = new saved_filters();
                filterRow.username = Convert.ToString(User[0]);
                filterRow.password = Convert.ToString(Password[0]);
                filterRow.sequence = 1;
                filterRow.filter = Convert.ToString(Filter[0]);
                db.saved_filters.Add(filterRow);
                db.SaveChanges();
            }
            else {
                filterObj.username = Convert.ToString(User[0]);
                filterObj.password = Convert.ToString(Password[0]);
                filterObj.sequence = 1;
                filterObj.filter = Convert.ToString(Filter[0]);
                db.SaveChanges();
            }
            var msg = "Row saved successfully";
            return msg;
        }
        
        //get statuses for dropdown
        public JsonResult GetStatusesList()
        {
            var list = db.GetStatusesList().ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        //get shipments for carrier bills
        public JsonResult GetShipmentCarrierBills()
        {
            var list = db.GetShipmentsForCarrierBill().ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        
        //get shipments for invoice
        public JsonResult GetShipmentInvoices()
        {
            var list = db.GetShipmentsForInvoice().ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        //get the shipments for tracking view
        public JsonResult GetShipments(string fromdate, string todate)
        {
            var list = db.GetShipmentsList(Convert.ToDateTime(fromdate), Convert.ToDateTime(todate)).ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        //get the pending shipments
        public JsonResult GetShipmentPending()
        {
            var list = db.GetShipmentPendingList().ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetInvoiceSequences(String booking, Boolean select)
        {
            List<invoiceSequence> sequences = new List<invoiceSequence>();
            
            //build invoice sequences datatable
            DataTable dtSequences = CreateDataTable("SELECT invoice_sequence, bill_to_id, bill_to_customer_id FROM shipment_consignee WHERE pli_book_number=" + booking + " GROUP BY invoice_sequence, bill_to_id, bill_to_customer_id");
            foreach (DataRow row in dtSequences.Rows)
            {
                var invoiceSeq = Convert.ToInt16(row["invoice_sequence"]);
                var billType = Convert.ToString(row["bill_to_id"]);
                var billToID = Convert.ToInt32(row["bill_to_customer_id"]);
                var billPartyID = 0;
                var billPartyLocationID = 0;
                if (billType == "CONS")
                {
                    DataTable dtBillTo = CreateDataTable("SELECT TOP 1 consignee_id, location_id FROM shipment_consignee WHERE pli_book_number=" + booking + " AND invoice_sequence =" + invoiceSeq);
                    if (dtBillTo.Rows.Count > 0)
                    {
                        billPartyID = Convert.ToInt32(dtBillTo.Rows[0]["consignee_id"]);
                        billPartyLocationID = Convert.ToInt16(dtBillTo.Rows[0]["location_id"]);
                    }
                }
                else if (billType == "SHP")
                {
                    DataTable dtBillTo = CreateDataTable("SELECT TOP 1 shipper_id, location_id FROM shipment_shipper WHERE pli_book_number=" + booking);
                    if (dtBillTo.Rows.Count > 0)
                    {
                        billPartyID = Convert.ToInt32(dtBillTo.Rows[0]["shipper_id"]);
                        billPartyLocationID = Convert.ToInt16(dtBillTo.Rows[0]["location_id"]);
                    }
                }
                
                invoiceSequence sequenceObj = new invoiceSequence()
                {
                    pli_book_number = booking,
                    select = select,
                    invoice_sequence = Convert.ToInt16(invoiceSeq),
                    bill_to_id = billType,
                    bill_to_customer_id = Convert.ToInt32(billToID),
                    bill_party_id = Convert.ToInt32(billPartyID),
                    bill_party_location_id = Convert.ToInt16(billPartyLocationID)
                };    
                sequences.Add(sequenceObj);
            }

            var jsonData = new
            {
                rows = sequences
            };
            
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        // GET: /Report/
        public ActionResult ReportRouteLadder(string id, string booking)
        {
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Reports/"), "RouteLadder.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("ErrorView1");
            }

            //datasource is the entire table
            //List<SMCDataModule> cm = new List<SMCDataModule>();
            //cm = db.SMCDataModules.ToList();

            //datasource is SP with parameters
            ReportDataSource rd1 = new ReportDataSource("RouteLadderHeaderDS", db.GetRouteLadderHeader(booking).ToList());
            lr.DataSources.Add(rd1);
            ReportDataSource rd2 = new ReportDataSource("RouteLadderCommodityDS", db.GetRouteLadderCommodity(booking).ToList());
            lr.DataSources.Add(rd2);
            ReportDataSource rd3 = new ReportDataSource("RouteLadderConsigneesDS", db.GetRouteLadderConsignees(booking).ToList());
            lr.DataSources.Add(rd3);
            ReportDataSource rd4 = new ReportDataSource("RouteLadderCarrierDS", db.GetRouteLadderCarrier(booking).ToList());
            lr.DataSources.Add(rd4);
            string reportType = id;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =

           "<DeviceInfo>" +
           "  <OutputFormat>" + id + "</OutputFormat>" +
           "  <PageWidth>8.5in</PageWidth>" +
           "  <PageHeight>11in</PageHeight>" +
           "  <MarginTop>0.25in</MarginTop>" +
           "  <MarginLeft>0.25in</MarginLeft>" +
           "  <MarginRight>0.25in</MarginRight>" +
           "  <MarginBottom>0.25in</MarginBottom>" +
           "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);
            
            return File(renderedBytes, mimeType);
        }

        // GET: /Report/
        public ActionResult ReportInvoiceSingle(string id, string booking, string date)
        {
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Reports/"), "InvoiceSingle.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("ErrorView1");
            }

            //datasource is the entire table
            //List<SMCDataModule> cm = new List<SMCDataModule>();
            //cm = db.SMCDataModules.ToList();

            //datasource is SP with parameters
            ReportDataSource rd1 = new ReportDataSource("ShipmentConfirmHeaderDS", db.GetShipmentConfirmationHeader(booking).ToList());
            lr.DataSources.Add(rd1);
            ReportDataSource rd2 = new ReportDataSource("ShipmentConfirmShipperDS", db.GetShipmentConfirmationShipper(booking).ToList());
            lr.DataSources.Add(rd2);
            ReportDataSource rd3 = new ReportDataSource("ShipmentConfirmConsigneeDS", db.GetShipmentConfirmationConsignee(booking).ToList());
            lr.DataSources.Add(rd3);
            ReportDataSource rd4 = new ReportDataSource("ShipmentConfirmCommodityDS", db.GetShipmentConfirmationCommodity(booking).ToList());
            lr.DataSources.Add(rd4);
            ReportDataSource rd5 = new ReportDataSource("ShipmentConfirmCustomerDS", db.GetShipmentConfirmationCustomer(booking).ToList());
            lr.DataSources.Add(rd5);
            ReportDataSource rd6 = new ReportDataSource("ShipmentConfirmAccDS", db.GetShipmentConfirmationAccessorial(booking).ToList());
            lr.DataSources.Add(rd6);

            //define parameter to pass into report
            //all parameters need to be defined in the Parameters section of the report
            ReportParameter p1 = new ReportParameter("p1", date);
            lr.SetParameters(new ReportParameter[] { p1 });
            
            string reportType = id;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =

           "<DeviceInfo>" +
           "  <OutputFormat>" + id + "</OutputFormat>" +
           "  <PageWidth>8.5in</PageWidth>" +
           "  <PageHeight>11in</PageHeight>" +
           "  <MarginTop>0.25in</MarginTop>" +
           "  <MarginLeft>0.25in</MarginLeft>" +
           "  <MarginRight>0.25in</MarginRight>" +
           "  <MarginBottom>0.25in</MarginBottom>" +
           "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);

            return File(renderedBytes, mimeType);
        }

        // GET: /Report/
        public ActionResult ReportInvoice(string id, string booking, int sequence, string billtype, int customerid, int billpartyid, int billpartyseq)
        {
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Reports/"), "Invoice.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("ErrorView1");
            }

            //datasource is the entire table
            //List<SMCDataModule> cm = new List<SMCDataModule>();
            //cm = db.SMCDataModules.ToList();

            //datasource is SP with parameters
            ReportDataSource rd1 = new ReportDataSource("ShipmentConfirmHeaderDS", db.GetShipmentConfirmationHeader(booking).ToList());
            lr.DataSources.Add(rd1);
            ReportDataSource rd2 = new ReportDataSource("ShipmentConfirmShipperDS", db.GetShipmentConfirmationShipper(booking).ToList());
            lr.DataSources.Add(rd2);
            ReportDataSource rd3 = new ReportDataSource("InvoiceConsigneeDS", db.GetInvoiceConsignee(booking,sequence).ToList());
            lr.DataSources.Add(rd3);
            ReportDataSource rd4 = new ReportDataSource("ShipmentConfirmCommodityDS", db.GetShipmentConfirmationCommodity(booking).ToList());
            lr.DataSources.Add(rd4);
            ReportDataSource rd5 = new ReportDataSource("ShipmentConfirmCustomerDS", db.GetShipmentConfirmationCustomer(booking).ToList());
            lr.DataSources.Add(rd5);
            ReportDataSource rd6 = new ReportDataSource("ShipmentConfirmAccDS", db.GetShipmentConfirmationAccessorial(booking).ToList());
            lr.DataSources.Add(rd6);
            ReportDataSource rd7 = new ReportDataSource("InvoiceBillToDS", db.GetInvoiceBillTo(booking,billtype,customerid,billpartyid,billpartyseq).ToList());
            lr.DataSources.Add(rd7);
            string reportType = id;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =

           "<DeviceInfo>" +
           "  <OutputFormat>" + id + "</OutputFormat>" +
           "  <PageWidth>8.5in</PageWidth>" +
           "  <PageHeight>11in</PageHeight>" +
           "  <MarginTop>0.25in</MarginTop>" +
           "  <MarginLeft>0.25in</MarginLeft>" +
           "  <MarginRight>0.25in</MarginRight>" +
           "  <MarginBottom>0.25in</MarginBottom>" +
           "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);

            return File(renderedBytes, mimeType);
        }

        // GET: /Report/
        public ActionResult ReportBOL(string id, string booking)
        {
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Reports/"), "BillOfLading.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("ErrorView1");
            }

            //datasource is the entire table
            //List<SMCDataModule> cm = new List<SMCDataModule>();
            //cm = db.SMCDataModules.ToList();

            //datasource is SP with parameters
            ReportDataSource rd1 = new ReportDataSource("ShipmentConfirmHeaderDS", db.GetShipmentConfirmationHeader(booking).ToList());
            lr.DataSources.Add(rd1);
            ReportDataSource rd2 = new ReportDataSource("ShipmentConfirmShipperDS", db.GetShipmentConfirmationShipper(booking).ToList());
            lr.DataSources.Add(rd2);
            ReportDataSource rd3 = new ReportDataSource("ShipmentConfirmConsigneeDS", db.GetShipmentConfirmationConsignee(booking).ToList());
            lr.DataSources.Add(rd3);
            ReportDataSource rd4 = new ReportDataSource("ShipmentConfirmCommodityDS", db.GetShipmentConfirmationCommodity(booking).ToList());
            lr.DataSources.Add(rd4);
            ReportDataSource rd5 = new ReportDataSource("BOLAccessorialDS", db.GetBOLAccessorial(booking).ToList());
            lr.DataSources.Add(rd5);
            string reportType = id;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =

           "<DeviceInfo>" +
           "  <OutputFormat>" + id + "</OutputFormat>" +
           "  <PageWidth>8.5in</PageWidth>" +
           "  <PageHeight>11in</PageHeight>" +
           "  <MarginTop>0.25in</MarginTop>" +
           "  <MarginLeft>0.25in</MarginLeft>" +
           "  <MarginRight>0.25in</MarginRight>" +
           "  <MarginBottom>0.25in</MarginBottom>" +
           "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);

            return File(renderedBytes, mimeType);
        }

        // GET: /Report/
        public ActionResult ReportShipmentConfirm(string id, string booking)
        {
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Reports/"), "ShipmentConfirmation.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("ErrorView1");
            }

            //datasource is the entire table
            //List<SMCDataModule> cm = new List<SMCDataModule>();
            //cm = db.SMCDataModules.ToList();

            //datasource is SP with parameters
            ReportDataSource rd1 = new ReportDataSource("ShipmentConfirmHeaderDS", db.GetShipmentConfirmationHeader(booking).ToList());
            lr.DataSources.Add(rd1);
            ReportDataSource rd2 = new ReportDataSource("ShipmentConfirmShipperDS", db.GetShipmentConfirmationShipper(booking).ToList());
            lr.DataSources.Add(rd2);
            ReportDataSource rd3 = new ReportDataSource("ShipmentConfirmConsigneeDS", db.GetShipmentConfirmationConsignee(booking).ToList());
            lr.DataSources.Add(rd3);
            ReportDataSource rd4 = new ReportDataSource("ShipmentConfirmCommodityDS", db.GetShipmentConfirmationCommodity(booking).ToList());
            lr.DataSources.Add(rd4);
            ReportDataSource rd5 = new ReportDataSource("ShipmentConfirmCarrierDS", db.GetShipmentConfirmationCarrier(booking).ToList());
            lr.DataSources.Add(rd5);
            string reportType = id;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =

           "<DeviceInfo>" +
           "  <OutputFormat>" + id + "</OutputFormat>" +
           "  <PageWidth>8.5in</PageWidth>" +
           "  <PageHeight>10.5in</PageHeight>" +
           "  <MarginTop>0.25in</MarginTop>" +
           "  <MarginLeft>0.25in</MarginLeft>" +
           "  <MarginRight>0.25in</MarginRight>" +
           "  <MarginBottom>0.25in</MarginBottom>" +
           "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);

            return File(renderedBytes, mimeType);
        }

        // GET: /Report/
        public ActionResult Report(string id, int codeID)
        {
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Reports/"), "Report1.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("ErrorView1");
            }

            //datasource is the entire table
            //List<SMCDataModule> cm = new List<SMCDataModule>();
            //cm = db.SMCDataModules.ToList();
            
            //datasource is SP with parameters
            ReportDataSource rd = new ReportDataSource("DataSet1", db.GetShipperNMFC(codeID).ToList());
            lr.DataSources.Add(rd);
            string reportType = id;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =

           "<DeviceInfo>" +
           "  <OutputFormat>" + id + "</OutputFormat>" +
           "  <PageWidth>8.5in</PageWidth>" +
           "  <PageHeight>11in</PageHeight>" +
           "  <MarginTop>0.5in</MarginTop>" +
           "  <MarginLeft>1in</MarginLeft>" +
           "  <MarginRight>1in</MarginRight>" +
           "  <MarginBottom>0.5in</MarginBottom>" +
           "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);

            return File(renderedBytes, mimeType);
        }

        // GET: /Report/
        public ActionResult ReportTestShipmentItems(string id, string booking)
        {
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Reports/"), "Report2.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("ErrorView1");
            }

            //datasource is the entire table
            //List<SMCDataModule> cm = new List<SMCDataModule>();
            //cm = db.SMCDataModules.ToList();

            //datasource is SP with parameters
            ReportDataSource rd = new ReportDataSource("DataSet3", db.TestGetShipmentItems(booking).ToList());
            lr.DataSources.Add(rd);
            string reportType = id;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =

           "<DeviceInfo>" +
           "  <OutputFormat>" + id + "</OutputFormat>" +
           "  <PageWidth>8.5in</PageWidth>" +
           "  <PageHeight>11in</PageHeight>" +
           "  <MarginTop>0.5in</MarginTop>" +
           "  <MarginLeft>1in</MarginLeft>" +
           "  <MarginRight>1in</MarginRight>" +
           "  <MarginBottom>0.5in</MarginBottom>" +
           "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);


            return File(renderedBytes, mimeType);
        }
        
        // GET: /TestReport1/
        public ActionResult TestReport1()
        {
            var ac = db.SMCDataModules.ToList();
            return View(ac);
        }

        // GET: /TestGrid/
        public ActionResult TestGrid()
        {
            return View();
        }
        
        // GET: /TestCustomer/
        public ActionResult TestCustomer()
        {
            return View();
        }

        public ActionResult CalculateCustomerRate(List<String> dataWeight, List<String> dataClass, String orgZip, String destZip, String customerId, String carrierId)
        {
            decimal rwRate;
            decimal netRate = 0;
            decimal fsc = 0;
            decimal rate = 0;
            customerObj customerData;
            var tariff = "";
            var effectiveDate = "";
            string strFZone = "";
            string strTZone = "";
            DataTable dtCarriers = new DataTable();
            decimal amc = 0;
            decimal discount = 0;
            var customerCharges = "";
            bool customerPricing;
            bool returnedRow = true;
            DataTable dtRateExceptions = new DataTable();
            
            //get customer profile data
            customerData = GetCustomerData(Convert.ToInt16(customerId));
            tariff = customerData.TariffName;
            effectiveDate = customerData.PLIRateEffective;
            customerPricing = customerData.IsRateException;

            //get the list item count, only need to count one array since they both have the same count
            var listCount = dataWeight.Count;

            //determine if customer carrier FAK exceptions exist
            //create a datatable to store the FAK exceptions
            DataTable dtExceptions = CreateDataTable("SELECT a.fak, a.fak_min, a.fak_max FROM fak_exceptions a, customer_carrier_fak b WHERE b.CustomerId ='" + customerId + "' AND b.carrier_id ='" + carrierId + "' AND b.fak_id = a.fak_id");
            if (dtExceptions.Rows.Count > 0)  //if exceptions found
            {
                for (int x = 1; x <= listCount; x++)
                {
                    for (int y = 1; y <= dtExceptions.Rows.Count; y++)
                    {
                        if (Convert.ToDecimal(dataClass[x - 1]) >= Convert.ToDecimal(dtExceptions.Rows[y - 1]["fak_min"]) && Convert.ToDecimal(dataClass[x - 1]) <= Convert.ToDecimal(dtExceptions.Rows[y - 1]["fak_max"]))
                        {
                            dataClass[x - 1] = Convert.ToString(dtExceptions.Rows[y - 1]["fak"]);
                            break;
                        }
                    }
                }
            }

            //build zone zips datatable
            DataTable dtZoneZips = CreateDataTable("SELECT zone_id, zip_begin, zip_end FROM zone_zip ORDER BY zone_id");

            //build a list zones that matchs the origin/destination zip pair
            foreach (DataRow row in dtZoneZips.Rows)
            {
                decimal zipbegin = Convert.ToDecimal(row["zip_begin"]);
                decimal zipend = Convert.ToDecimal(row["zip_end"]);

                if (IsBetween(Convert.ToDecimal(orgZip), zipbegin, zipend))
                {
                    strFZone = strFZone + "'" + Convert.ToString(row["zone_id"]) + "'" + ",";
                }

                if (Convert.ToDecimal(orgZip) == zipbegin)
                {
                    strFZone = strFZone + "'" + Convert.ToString(row["zone_id"]) + "'" + ",";
                }

                if (IsBetween(Convert.ToDecimal(destZip), zipbegin, zipend))
                {
                    strTZone = strTZone + "'" + Convert.ToString(row["zone_id"]) + "'" + ",";
                }

                if (Convert.ToDecimal(destZip) == zipend)
                {
                    strTZone = strTZone + "'" + Convert.ToString(row["zone_id"]) + "'" + ",";
                }
            }

            //remove the last extra comma at the end of string
            strFZone = strFZone.TrimEnd(',');
            strTZone = strTZone.TrimEnd(',');

            //get pricing for customer carrier
            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ToString()))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("GetCustomerCarriersPricingByZones", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@strCustomerID", SqlDbType.NVarChar).Value = customerId;
                cmd.Parameters.Add("@strCarrierID", SqlDbType.NVarChar).Value = carrierId;
                cmd.Parameters.Add("@strFromZones", SqlDbType.NVarChar).Value = strFZone;
                cmd.Parameters.Add("@strToZones", SqlDbType.NVarChar).Value = strTZone;
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    dtRateExceptions.Load(reader);
                    discount = Convert.ToDecimal(dtRateExceptions.Rows[0]["discount"]) * 100;
                    amc = Convert.ToDecimal(dtRateExceptions.Rows[0]["absolute_minimum"]);
                }
                else
                {
                    returnedRow = false;
                }
                reader.Close();
                cmd.Dispose();
            };

            if (returnedRow == false)
            {
                //get pricing for customer
                using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ToString()))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetCustomerPricingByZones", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@strCustomerID", SqlDbType.NVarChar).Value = customerId;
                    cmd.Parameters.Add("@strFromZones", SqlDbType.NVarChar).Value = strFZone;
                    cmd.Parameters.Add("@strToZones", SqlDbType.NVarChar).Value = strTZone;
                    SqlDataReader reader = cmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        dtRateExceptions.Load(reader);
                        discount = Convert.ToDecimal(dtRateExceptions.Rows[0]["discount"]) * 100;
                        amc = Convert.ToDecimal(dtRateExceptions.Rows[0]["absolute_minimum"]);
                    }
                    else
                    {
                        returnedRow = false;
                    }
                    reader.Close();
                    cmd.Dispose();
                };
            }
                       
            /*
            //get customer default pricing
            DataTable dtCustomerRow = CreateDataTable("SELECT InterStateMinCharge, InterStateDiscount FROM Customer WHERE CustomerId = " + customerId);
            if (dtCustomerRow.Rows.Count > 0)
            {
                discount = Convert.ToDecimal(dtCustomerRow.Rows[0]["InterStateDiscount"]);
                amc = Convert.ToDecimal(dtCustomerRow.Rows[0]["InterStateMinCharge"]);
            }
            
            //if customer has zone pricing exceptions
            if (customerPricing)
            {
                {
                    using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ToString()))
                    {
                        conn.Open();
                        SqlCommand cmd = new SqlCommand("GetCustomerCarriersPricingByZones", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@strCustomerID", SqlDbType.NVarChar).Value = customerId;
                        cmd.Parameters.Add("@strCarrierID", SqlDbType.NVarChar).Value = carrierId;
                        cmd.Parameters.Add("@strFromZones", SqlDbType.NVarChar).Value = strFZone;
                        cmd.Parameters.Add("@strToZones", SqlDbType.NVarChar).Value = strTZone;
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.HasRows)
                        {
                            dtRateExceptions.Load(reader);
                            discount = Convert.ToDecimal(dtRateExceptions.Rows[0]["discount"]) * 100;
                            amc = Convert.ToDecimal(dtRateExceptions.Rows[0]["absolute_minimum"]);
                        }
                        reader.Close();
                        cmd.Dispose();
                    };
                }
            }
            */
           
            //call method to build the commodity array to use with RateWare
            LTLRequestDetail[] dtCommAry = CreateCommodityArray(dataWeight, dataClass, listCount);

            //rate shipment using RateWare
            var rwRsp = RateLTLShipment(orgZip, destZip, tariff, effectiveDate, dtCommAry);

            //get rate from result from RateWare
            rwRate = Convert.ToDecimal(rwRsp.totalCharge);

            //calculate netrate with discound applied
            netRate = CalculateNetRate(rwRate, (discount/100));

            //determine if AMC applies.  Returns the larger of the two values
            rate = Math.Max(amc, netRate);

            //calculate the fsc charge
            fsc = CalculateFSC(rate);

            customerCharges = Convert.ToString(rate);

            var jsonData = new
            {
                fsc = Convert.ToString(fsc),
                customerRate = Convert.ToString(customerCharges),
                //rateWareRate = Convert.ToString(rwRate),
                //customerPricing = Convert.ToBoolean(customerPricing),
                amc = amc,
                discount = discount
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        
        // GET: /TestCarrier/
        public ActionResult TestCarrier()
        {
            return View();
        }

        public static List<string> StatusList = new List<string>()
        {
            "BOOKED",
            "CANCELLED",
            "CLAIM FILED",
            "CLAIM PAID",
            "CLAIM PENDING",
            "COMPLETED",
            "DELIVERED",
            "IN TRANSIT",
            "ON HOLD",
            "OUT FOR DELIVERY",
            "PU REQUESTED",
            "PU SCHEDULED",
            "DELETE"
        };

        public static List<string> BillToList = new List<string>()
        {
            "CUSTOMER",
            "CONSIGNEE",
            "SHIPPER",
            "3RDPARTY"
        };

        /*CONSIGNEE GRID METHODS*/
        //soft delete a consignee row from the grid
        [HttpPost]
        public string DeleteConsigneeRow(string Booking, int Sequence)
        {
            string msg = "";
            shipment_consignee consigneeObj = db.shipment_consignee.Find(Booking, Sequence);
            if (consigneeObj != null)
            {
                consigneeObj.record_state = "0";
            }

            try
            {
                if (ModelState.IsValid)
                {
                    db.SaveChanges();
                    msg = "Delete successful";
                }
                else
                {
                    msg = "Validation data not successful";
                }
            }
            catch (Exception ex)
            {
                msg = "Error occured:" + ex.Message;
            }

            return msg;
        }
        
        //get the consignee location zip
        public string GetConsigneeLocationZip(int consigneeId, int locationId)
        {
            var list = db.GetConsigneeLocationZip(consigneeId, locationId).FirstOrDefault();
            return list;
        }

        //build the customer dropdown list for the partial view
        [HttpGet]
        public ActionResult GetCustomerList()
        {
            var CustomerListResult = customerList.GetAllCustomers();
            var CustomerSelectList = new SelectList(CustomerListResult, "CustomerId", "CustomerName");
            
            //Convert to Dictionary
            var ls = CustomerListResult.ToDictionary(q => q.CustomerId, q => q.CustomerName);
           
            return PartialView("_Select", ls);
        }
        
        //get shipment_consignee rows for consigneeGrid
        public JsonResult GetConsigneeRows(string sidx, string sord, int page, int rows)
        {
            string booking = Request.QueryString["booking"];
            //string booking = "1216840";
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            var itemsResults = from sc in db.shipment_consignee where sc.pli_book_number == booking && (sc.record_state == null || sc.record_state == "1") select sc;
            /*
            var itemsResults = (from ss in db.shipment_shipper
                               join s in db.shippers on ss.shipper_id equals s.shipper_id
                               where ss.pli_book_number == booking
                               select new
                               {
                                ss.pli_book_number,
                                ss.shipper_id,
                                ss.location_id,
                                ss.location_contact_id,
                                ss.reference_number,
                                ss.actual_pu_date,
                                ss.sequence,
                                s.name
                               }).ToList();
            */
            int totalRecords = itemsResults.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord.ToUpper() == "DESC")
            {
                itemsResults = itemsResults.OrderByDescending(sc => sc.sequence);
                itemsResults = itemsResults.Skip(pageIndex * pageSize).Take(pageSize);
            }
            else
            {
                itemsResults = itemsResults.OrderBy(sc => sc.sequence);
                itemsResults = itemsResults.Skip(pageIndex * pageSize).Take(pageSize);
            }
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = itemsResults
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        //get the consignee list
        public JsonResult GetConsigneeList()
        {
            //get the result from a method of the class
            //this method utilizes caching
            var ConsigneeListResult = consigneeList.GetAllConsignees();
            var ConsigneeSelectList = new SelectList(ConsigneeListResult, "consignee_id", "name");
            return Json(ConsigneeSelectList.ToList(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        //public string CreateShipmentConsignee([Bind(Exclude = "Id")] shipment_consignee objSC, FormCollection formCollection, List<String> Bookings, List<String> Consignees, List<String> ConsigneeLocs, List<String> ConsigneeLocCons, List<String> POs, List<DateTime> ReqstDelvs, List<String> ReqstTimes, List<DateTime> ETAs, List<DateTime> ActDelvs, List<String> BillAmts, List<String> BillTos, List<String> BillCustomers, List<String> Sequences, List<String> LocZips, List<String> LocSTs, List<String> Select, List<String> InvoiceNos, List<String> InvoiceSeqs, List<String> AdvanceFees, List<String> LinehaulFees, List<String> DirectFees, List<String> FeePercent)
        public string CreateShipmentConsignee([Bind(Exclude = "Id")] shipment_consignee objSC, FormCollection formCollection, List<String> Bookings, List<String> Consignees, List<String> ConsigneeLocs, List<String> ConsigneeLocCons, List<String> POs, List<DateTime> ReqstDelvs, List<String> ReqstTimes, List<DateTime> ETAs, List<DateTime> ActDelvs, List<String> BillAmts, List<String> BillCustomers, List<String> Sequences, List<String> LocZips, List<String> LocSTs, List<String> Select, List<String> FeePercent)
        {
            string msg;

            //get the count of the Bookings array.  Only need the count of one array since all arrays have the same count.
            var listCount = Bookings.Count;

            for (int x = 1; x <= listCount; x++)
            {
                shipment_consignee consigneeRow = new shipment_consignee();
                consigneeRow.pli_book_number = Bookings[x - 1];
                consigneeRow.consignee_id = Convert.ToInt64(Consignees[x - 1]);
                consigneeRow.location_id = Convert.ToInt16(ConsigneeLocs[x - 1]);
                consigneeRow.location_contact_id = Convert.ToInt16(ConsigneeLocCons[x - 1]);
                consigneeRow.po_number = POs[x - 1];

                if (ReqstDelvs[x - 1] != DateTime.MinValue)
                {
                    consigneeRow.requested_delivery = ReqstDelvs[x - 1];
                }
                
                consigneeRow.requested_delivery_time = ReqstTimes[x - 1];
              
                if (ETAs[x - 1] != DateTime.MinValue)
                {
                    consigneeRow.delivery_eta = ETAs[x - 1];
                }
                
                if (ActDelvs[x - 1] != DateTime.MinValue)
                {
                    consigneeRow.actual_delivery = ActDelvs[x - 1];
                }
                                            
                consigneeRow.bill_amount = Convert.ToDecimal(BillAmts[x - 1]);
                //consigneeRow.bill_to_id = BillTos[x - 1];
                consigneeRow.bill_to_customer_id = Convert.ToInt32(BillCustomers[x - 1]);
                consigneeRow.sequence = Convert.ToInt16(Sequences[x - 1]);
                consigneeRow.location_zip = LocZips[x - 1];
                consigneeRow.location_state = LocSTs[x - 1];
                consigneeRow.group_select = Convert.ToBoolean(Select[x - 1]);

                //consigneeRow.invoice_number = InvoiceNos[x - 1];
                //consigneeRow.invoice_sequence = Convert.ToInt16(InvoiceSeqs[x - 1]);
                //consigneeRow.advance_fee = Convert.ToDecimal(AdvanceFees[x - 1]);
                //consigneeRow.linehaul_fee = Convert.ToDecimal(LinehaulFees[x - 1]);
                //consigneeRow.direct_fee = Convert.ToDecimal(DirectFees[x - 1]);
                consigneeRow.fee_percent = Convert.ToDecimal(FeePercent[x - 1]);
                
                /*
                consigneeRow.pli_book_number = Convert.ToString(Bookings[x - 1]);
                consigneeRow.consignee_id = Convert.ToInt64(Consignees[x - 1]);
                consigneeRow.location_id = Convert.ToInt16(ConsigneeLocs[x - 1]);
                consigneeRow.location_contact_id = Convert.ToInt16(ConsigneeLocCons[x - 1]);
                consigneeRow.po_number = Convert.ToString(POs[x - 1]);
                consigneeRow.requested_delivery = Convert.ToDateTime(ReqstDelvs[x - 1]);
                consigneeRow.requested_delivery_time = Convert.ToString(ReqstTimes[x - 1]);
                consigneeRow.delivery_eta = Convert.ToDateTime(ETAs[x - 1]);
                consigneeRow.actual_delivery = Convert.ToDateTime(ActDelvs[x - 1]);
                consigneeRow.bill_amount = Convert.ToDecimal(BillAmts[x - 1]);
                consigneeRow.bill_to_id = Convert.ToString(BillTos[x - 1]);
                consigneeRow.bill_to_customer_id = Convert.ToInt16(BillCustomers[x - 1]);
                consigneeRow.sequence = Convert.ToInt16(Sequences[x - 1]);
                consigneeRow.location_zip = Convert.ToString(LocZips[x - 1]);
                consigneeRow.location_state = Convert.ToString(LocSTs[x - 1]);
                */

                try
                {
                    if (ModelState.IsValid)
                    {
                        db.shipment_consignee.Add(consigneeRow);
                    }
                    else
                    {
                        msg = "Validation data not successfull";
                    }
                }
                catch (Exception ex)
                {
                    msg = "Error occured:" + ex.Message;
                }
            } //eo for

            db.SaveChanges();
            msg = "Row saved successfully";
            return msg;
            
            /*
            try
            {
                if (ModelState.IsValid)
                {
                    db.shipment_consignee.Add(objSC);
                    db.SaveChanges();
                    msg = "Row saved successfully";
                }
                else
                {
                    msg = "Validation data not successfull";
                }
            }
            catch (Exception ex)
            {
                msg = "Error occured:" + ex.Message;
            }
            return msg;
            */
        }

        //method write log entry to file
        public static void Log(string bookingNumber, string seq, string locID, StreamWriter log)
        {
            log.WriteLine("Date:" + DateTime.Now);
            log.WriteLine("Booking #:" + bookingNumber);
            log.WriteLine("Seq:" + seq);
            log.WriteLine("Location:" + locID);
            log.WriteLine();
        }

        [HttpPost]
        public string UpdateShipmentConsignee([Bind(Exclude = "Id")] shipment_consignee objSC, FormCollection formCollection, List<String> Bookings, List<String> Consignees, List<String> ConsigneeLocs, List<String> ConsigneeLocCons, List<String> POs, List<DateTime> ReqstDelvs, List<String> ReqstTimes, List<DateTime> ETAs, List<DateTime> ActDelvs, List<String> BillAmts, List<String> BillTos, List<String> BillCustomers, List<String> LocZips, List<String> LocSTs, List<String> Sequences, List<String> Select, List<String> InvoiceNos, List<String> InvoiceSeqs, List<String> AdvanceFees, List<String> LinehaulFees, List<String> DirectFees, List<String> FeePercent)
        {
            string msg;
            //StreamWriter log;

            //get the count of the Bookings array.  Only need the count of one array since all arrays have the same count.
            var listCount = Bookings.Count;

            //log = new StreamWriter(@"C:\Temp\" + DateTime.Now.ToString("yyyyMMdd_") + "log.txt");

            for (int x = 1; x <= listCount; x++)
            {
                shipment_consignee consigneeObj = db.shipment_consignee.Find(Bookings[x - 1], Convert.ToInt16(Sequences[x - 1]));

                if (consigneeObj == null)
                //add not found record
                {
                    shipment_consignee consigneeRow = new shipment_consignee();
                    consigneeRow.pli_book_number = Bookings[x - 1];
                    consigneeRow.sequence = Convert.ToInt16(Sequences[x - 1]);
                    consigneeRow.consignee_id = Convert.ToInt64(Consignees[x - 1]);
                    consigneeRow.location_id = Convert.ToInt16(ConsigneeLocs[x - 1]);
                    consigneeRow.location_contact_id = Convert.ToInt16(ConsigneeLocCons[x - 1]);
                    consigneeRow.po_number = POs[x - 1];
                    consigneeRow.requested_delivery = ReqstDelvs[x - 1];
                    consigneeRow.requested_delivery_time = ReqstTimes[x - 1];
                    consigneeRow.delivery_eta = ETAs[x - 1];
                    consigneeRow.actual_delivery = ActDelvs[x - 1];
                    consigneeRow.bill_amount = Convert.ToDecimal(BillAmts[x - 1]);
                    consigneeRow.bill_to_id = BillTos[x - 1];
                    consigneeRow.bill_to_customer_id = Convert.ToInt32(BillCustomers[x - 1]);
                    consigneeRow.location_zip = LocZips[x - 1];
                    consigneeRow.location_state = LocSTs[x - 1];
                    consigneeRow.group_select = Convert.ToBoolean(Select[x - 1]);
                    consigneeRow.invoice_number = InvoiceNos[x - 1];
                    consigneeRow.invoice_sequence = Convert.ToInt16(InvoiceSeqs[x - 1]);
                    consigneeRow.advance_fee = Convert.ToDecimal(AdvanceFees[x - 1]);
                    consigneeRow.linehaul_fee = Convert.ToDecimal(LinehaulFees[x - 1]);
                    consigneeRow.direct_fee = Convert.ToDecimal(DirectFees[x - 1]);
                    consigneeRow.fee_percent = Convert.ToDecimal(FeePercent[x - 1]);

                    try
                    {
                        if (ModelState.IsValid)
                        {
                            db.shipment_consignee.Add(consigneeRow);
                            db.SaveChanges();
                        }
                        else
                        {
                            msg = "Validation data not successfull";
                        }
                    }
                    catch (Exception ex)
                    {
                        msg = "Error occured:" + ex.Message;
                    }
                }
                //update found record
                else
                {
                    consigneeObj.consignee_id = Convert.ToInt64(Consignees[x - 1]);
                    consigneeObj.location_id = Convert.ToInt16(ConsigneeLocs[x - 1]);
                    consigneeObj.location_contact_id = Convert.ToInt16(ConsigneeLocCons[x - 1]);
                    consigneeObj.po_number = POs[x - 1];
                    consigneeObj.requested_delivery = ReqstDelvs[x - 1];
                    consigneeObj.requested_delivery_time = ReqstTimes[x - 1];
                    consigneeObj.delivery_eta = ETAs[x - 1];
                    consigneeObj.actual_delivery = ActDelvs[x - 1];
                    consigneeObj.bill_amount = Convert.ToDecimal(BillAmts[x - 1]);
                    consigneeObj.bill_to_id = BillTos[x - 1];
                    consigneeObj.bill_to_customer_id = Convert.ToInt32(BillCustomers[x - 1]);
                    consigneeObj.location_zip = LocZips[x - 1];
                    consigneeObj.location_state = LocSTs[x - 1];
                    consigneeObj.group_select = Convert.ToBoolean(Select[x - 1]);
                    consigneeObj.invoice_number = InvoiceNos[x - 1];
                    consigneeObj.invoice_sequence = Convert.ToInt16(InvoiceSeqs[x - 1]);
                    consigneeObj.advance_fee = Convert.ToDecimal(AdvanceFees[x - 1]);
                    consigneeObj.linehaul_fee = Convert.ToDecimal(LinehaulFees[x - 1]);
                    consigneeObj.direct_fee = Convert.ToDecimal(DirectFees[x - 1]);
                    consigneeObj.fee_percent = Convert.ToDecimal(FeePercent[x - 1]);

                    try
                    {
                        if (ModelState.IsValid)
                        {
                            db.SaveChanges();
                        }
                        else
                        {
                            msg = "Validation data not successfull";
                        }
                    }
                    catch (Exception ex)
                    {
                        msg = "Error occured:" + ex.Message;
                    }
                }
                
                //log the update event
                //Log(Bookings[x - 1], Convert.ToString(Sequences[x - 1]), Convert.ToString(ConsigneeLocs[x - 1]), log);
            
            } //eo for

            //log.Close();

            msg = "Row saved successfully";
            return msg;
        }

        //get carrier ins expiration date
        public string GetCarrierInsExpDate(int carrierId)
        {
            var list = Convert.ToString(db.GetCarrierInsExpDate(carrierId).FirstOrDefault());
            return list;
        }

        //calculate days left between dates
        public string CalculateDaysLeft(string expDate)
        {
            var list = Convert.ToString(db.CalculateDaysLeft(Convert.ToDateTime (expDate)).FirstOrDefault());
            return list;
        }
        
        //get the consignee location notes
        public string GetConsigneeLocationNotes(int consigneeId, int locationId)
        {
            var list = db.GetConsigneeLocationNotes(consigneeId, locationId).FirstOrDefault();
            return list;
        }

        //get the shipper location notes
        public string GetShipperLocationNotes(int shipperId, int locationId)
        {
            var list = db.GetShipperLocationNotes(shipperId,locationId).FirstOrDefault();
            return list;
        }
        
        //get the customer notes
        public string GetCustomerNotes(int customerId)
        {
            var list = db.GetCustomerNotes(customerId).FirstOrDefault();
            return list;
        }
        
        //get the customer name
        public string GetCustomerName(int customerId)
        {
            var list = db.GetCustomerName(customerId).FirstOrDefault();
            return list;
        }

        //get the consignee location name
        public string GetConsigneeLocationName(int consigneeId, int locationId)
        {
            var list = db.GetConsigneeLocationName(consigneeId, locationId).FirstOrDefault();
            return list;
        }

        //get the consignee location contact name
        public string GetConsigneeLocationContactName(int consigneeId, int locationId, int contactId)
        {
            var list = db.GetConsigneeLocationContactName(consigneeId, locationId, contactId).FirstOrDefault();
            return list;
        }

        public string EditShipmentConsignee(shipment_consignee objSC)
        {
            string msg;
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(objSC).State = EntityState.Modified;
                    db.SaveChanges();
                    msg = "Row saved successfully";
                }
                else
                {
                    msg = "Validation data not successfull";
                }
            }
            catch (Exception ex)
            {
                msg = "Error occured:" + ex.Message;
            }
            return msg;
        }

        public string DeleteShipmentConsignee(string booking, int sequence)
        {
            //Id comes from the View.  Id is the default row number.
            //booking and sequence come from the delData parameters of the View.
            //return booking + "-" + Convert.ToString(sequence);

            shipment_consignee objSC = db.shipment_consignee.Find(booking, sequence);
            db.shipment_consignee.Remove(objSC);
            db.SaveChanges();
            return "Row deleted successfully";
        }       

        
        /*SHIPPER GIRD METHODS*/
        [HttpPost]
        public string DeleteShipperRow(string Booking, int Sequence)
        {
            string msg = "";
            shipment_shipper shipperObj = db.shipment_shipper.Find(Booking, Sequence);
            if (shipperObj != null)
            {
                shipperObj.record_state = "0";
            }

            try
            {
                if (ModelState.IsValid)
                {
                    db.SaveChanges();
                    msg = "Delete successful";
                }
                else
                {
                    msg = "Validation data not successful";
                }
            }
            catch (Exception ex)
            {
                msg = "Error occured:" + ex.Message;
            }

            return msg;
        }

        //build the shipper dropdown list for the partial view
        public JsonResult GetShipmentShipperLocation(int shipperId, int locationId)
        {
            var ShipperListResult = db.GetShipmentShipperLocation(shipperId,locationId).ToList();
            var ShipperSelectList = new SelectList(ShipperListResult, "seq_number", "LocationName");
            return Json(ShipperSelectList.ToList(), JsonRequestBehavior.AllowGet);
        }

        //get shipment_shipper rows for shipperGrid
        public JsonResult GetShipperRows(string sidx, string sord, int page, int rows)
        {
            string booking = Request.QueryString["booking"];
            //string booking = "1216840";
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            var itemsResults = from ss in db.shipment_shipper where ss.pli_book_number == booking && (ss.record_state == null || ss.record_state == "1") select ss;
            /*
            var itemsResults = (from ss in db.shipment_shipper
                               join s in db.shippers on ss.shipper_id equals s.shipper_id
                               where ss.pli_book_number == booking
                               select new
                               {
                                ss.pli_book_number,
                                ss.shipper_id,
                                ss.location_id,
                                ss.location_contact_id,
                                ss.reference_number,
                                ss.actual_pu_date,
                                ss.sequence,
                                s.name
                               }).ToList();
              */                            

            int totalRecords = itemsResults.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord.ToUpper() == "DESC")
            {
                itemsResults = itemsResults.OrderByDescending(ss => ss.sequence);
                itemsResults = itemsResults.Skip(pageIndex * pageSize).Take(pageSize);
            }
            else
            {
                itemsResults = itemsResults.OrderBy(s => s.sequence);
                itemsResults = itemsResults.Skip(pageIndex * pageSize).Take(pageSize);
            }
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = itemsResults
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public string EditShipmentShipper(shipment_shipper objSS)
        {
            string msg;
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(objSS).State = EntityState.Modified;
                    db.SaveChanges();
                    msg = "Row saved successfully";
                }
                else
                {
                    msg = "Validation data not successfull";
                }
            }
            catch (Exception ex)
            {
                msg = "Error occured:" + ex.Message;
            }
            return msg;
        }

        [HttpPost]
        public string CreateShipmentShipper([Bind(Exclude = "Id")] shipment_shipper objSS, FormCollection formCollection, List<String> Bookings, List<String> Shippers, List<String> ShipperLocs, List<String> ShipperLocCons, List<String> References, List<DateTime> PickUp, List<String> Sequences, List<String> LocZips, List<String> LocSTs )
        {
            string msg;

            //get the count of the Bookings array.  Only need the count of one array since all arrays have the same count.
            var listCount = Bookings.Count;

            for (int x = 1; x <= listCount; x++)
            {
                shipment_shipper shipperRow = new shipment_shipper();
                shipperRow.pli_book_number = Bookings[x - 1];
                shipperRow.shipper_id = Convert.ToInt64(Shippers[x - 1]);
                shipperRow.location_id = Convert.ToInt16(ShipperLocs[x - 1]);
                shipperRow.sequence = Convert.ToInt16(Sequences[x - 1]);
                shipperRow.location_contact_id = Convert.ToInt16(ShipperLocCons[x - 1]);
                shipperRow.reference_number = References[x - 1];
                if (PickUp[x - 1] != DateTime.MinValue)
                {
                    shipperRow.actual_pu_date = PickUp[x - 1];
                }
                shipperRow.location_zip = LocZips[x - 1];
                shipperRow.location_state = LocSTs[x - 1];

                try
                {
                    if (ModelState.IsValid)
                    {
                        db.shipment_shipper.Add(shipperRow);
                    }
                    else
                    {
                        msg = "Validation data not successfull";
                    }
                }
                catch (Exception ex)
                {
                    msg = "Error occured:" + ex.Message;
                }
            } //eo for

            db.SaveChanges();
            msg = "Row saved successfully";
            return msg;
        }

        [HttpPost]
        public string UpdateShipmentShipper([Bind(Exclude = "Id")] shipment_shipper objSS, FormCollection formCollection, List<String> Bookings, List<String> Shippers, List<String> ShipperLocs, List<String> ShipperLocCons, List<String> References, List<DateTime> PickUps, List<String> Sequences, List<String> LocZips, List<String> LocSTs)
        {
            string msg;

            //get the count of the Bookings array.  Only need the count of one array since all arrays have the same count.
            var listCount = Bookings.Count;

            for (int x = 1; x <= listCount; x++)
            {
                shipment_shipper shipperObj = db.shipment_shipper.Find(Bookings[x - 1], Convert.ToInt16(Sequences[x - 1]));
                
                if (shipperObj == null)
                //add not found record
                {
                    shipment_shipper shipperRow = new shipment_shipper();
                    shipperRow.pli_book_number = Bookings[x - 1];
                    shipperRow.shipper_id = Convert.ToDecimal(Shippers[x - 1]);
                    shipperRow.location_id = Convert.ToInt16(ShipperLocs[x - 1]);
                    shipperRow.sequence = Convert.ToInt16(Sequences[x - 1]);
                    shipperRow.location_contact_id = Convert.ToDecimal(ShipperLocCons[x - 1]);
                    shipperRow.reference_number = References[x - 1];
                    shipperRow.actual_pu_date = PickUps[x - 1];
                    shipperRow.location_zip = LocZips[x - 1];
                    shipperRow.location_state = LocSTs[x - 1];

                    try
                    {
                        if (ModelState.IsValid)
                        {
                            db.shipment_shipper.Add(shipperRow);
                            db.SaveChanges();
                        }
                        else
                        {
                            msg = "Validation data not successfull";
                        }
                    }
                    catch (Exception ex)
                    {
                        msg = "Error occured:" + ex.Message;
                    }
                }
                //update found record
                else
                {
                    shipperObj.shipper_id = Convert.ToDecimal(Shippers[x - 1]);
                    shipperObj.location_id = Convert.ToInt16(ShipperLocs[x - 1]);
                    shipperObj.location_contact_id = Convert.ToDecimal(ShipperLocCons[x - 1]);
                    shipperObj.reference_number = References[x - 1];
                    shipperObj.location_zip = LocZips[x - 1];
                    shipperObj.location_state = LocSTs[x - 1];
                    shipperObj.actual_pu_date = PickUps[x - 1];
                    
                    try
                    {
                        if (ModelState.IsValid)
                        {
                            db.SaveChanges();
                        }
                        else
                        {
                            msg = "Validation data not successfull";
                        }
                    }
                    catch (Exception ex)
                    {
                        msg = "Error occured:" + ex.Message;
                    }
                }
            } //eo for

            msg = "Row saved successfully";
            return msg;
        }

        public string DeleteShipmentShipper(string booking, int sequence)
        {
            //Id comes from the View.  Id is the default row number.
            //booking and sequence come from the delData parameters of the View.
            //return booking + "-" + Convert.ToString(sequence);

            shipment_shipper objSS = db.shipment_shipper.Find(booking, sequence);
            db.shipment_shipper.Remove(objSS);
            db.SaveChanges();
            return "Row deleted successfully";
        }        
        /*END*/

        /*COMMODITY GRID METHODS*/
        //soft delete a commodity row from the grid
        [HttpPost]
        public string DeleteCommodityRow(string Booking, int Sequence)
        {
            string msg = "";
            shipment_items itemObj = db.shipment_items.Find(Booking, Sequence);
            if (itemObj != null)
            {
                itemObj.record_state = "0";
            }

            try
            {
                if (ModelState.IsValid)
                {
                    db.SaveChanges();
                    msg = "Delete successful";
                }
                else
                {
                    msg = "Validation data not successful";
                }
            }
            catch (Exception ex)
            {
                msg = "Error occured:" + ex.Message;
            }

            return msg;
        }

        //get shipment_items rows for commodityGrid
        public JsonResult GetCommodityRows(string sidx, string sord, int page, int rows)
        {
            string booking = Request.QueryString["booking"];
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            var itemsResults = from si in db.shipment_items where si.pli_book_number == booking && (si.record_state == null || si.record_state == "1") select si;

            int totalRecords = itemsResults.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord.ToUpper() == "DESC")
            {
                itemsResults = itemsResults.OrderByDescending(s => s.item_seq_number);
                itemsResults = itemsResults.Skip(pageIndex * pageSize).Take(pageSize);
            }
            else
            {
                itemsResults = itemsResults.OrderBy(s => s.item_seq_number);
                itemsResults = itemsResults.Skip(pageIndex * pageSize).Take(pageSize);
            }
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = itemsResults
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        /*This method is no longer needed, consolidate insert commodity code to UpdateShipmentCommodity
        [HttpPost]
        public string CreateCommodity([Bind(Exclude = "Id")] shipment_items objSI, FormCollection formCollection, List<String> Bookings, List<String> Sequences, List<String> Descriptions, List<String> NMFCs, List<String> Subs, List<String> Classes, List<String> Pieces, List<String> Weight, List<String> Packaging, List<String> HazMat, List<String> Length, List<String> Width, List<String> Height, List<String> NMFCIDs)
        {
            string msg;

            //get the count of the Bookings array.  Only need the count of one array since all arrays have the same count.
            var listCount = Bookings.Count;

            for (int x = 1; x <= listCount; x++)
            {
                shipment_items itemRow = new shipment_items();
                itemRow.pli_book_number = Bookings[x - 1];
                itemRow.item_seq_number = Convert.ToInt16(Sequences[x - 1]);
                itemRow.description = Descriptions[x - 1];
                itemRow.nmfc = NMFCs[x - 1];
                itemRow.sub = Subs[x - 1];
                itemRow.@class = Classes[x - 1];

                int weightVal = Convert.ToInt32(Weight[x - 1]);
                itemRow.weight = (float)weightVal;

                itemRow.pieces = Convert.ToInt16(Pieces[x - 1]);
                itemRow.packaging = Packaging[x - 1];
                itemRow.hazmat_notes = HazMat[x - 1];
                itemRow.length = Length[x - 1];
                itemRow.width = Width[x - 1];
                itemRow.height = Height[x - 1];
                itemRow.nmfc_id = NMFCIDs[x - 1];
                
                
                //shipper_nmfc nmfcRow = new shipper_nmfc();
                //nmfcRow.shipper_id = 33605;
                //nmfcRow.nmfc_id = Convert.ToInt16(NMFCIDs[x - 1]);
                
                try
                {
                    if (ModelState.IsValid)
                    {
                        db.shipment_items.Add(itemRow);
                        //db.shipper_nmfc.Add(nmfcRow);
                    }
                    else
                    {
                        msg = "Validation data not successfull";
                    }
                }
                catch (Exception ex)
                {
                    msg = "Error occured:" + ex.Message;
                }
            } //eo for

            db.SaveChanges();
            msg = "Row saved successfully";
            return msg;
        }
        */
        
        public string EditCommodity(shipment_items objSI)
        {
            string msg;
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(objSI).State = EntityState.Modified;
                    db.SaveChanges();
                    
                    //execute the stored procedure to set the description of the selected nmfc item
                    db.SetNMFCItemDescription(Convert.ToString(objSI.pli_book_number), Convert.ToInt16(objSI.item_seq_number), Convert.ToString(objSI.nmfc_id));
                    
                    msg = "Row saved successfully";
                }
                else
                {
                    msg = "Validation data not successfull";
                }
            }
            catch (Exception ex)
            {
                msg = "Error occured:" + ex.Message;
            }
            return msg;
        }

        public string DeleteShipmentItem(string booking, decimal sequence)
        {
            //Id comes from the View.  Id is the default row number.
            //booking and sequence come from the delData parameters of the View.
            //return booking + "-" + Convert.ToString(sequence);
            
            shipment_items objSI = db.shipment_items.Find(booking, sequence);
            db.shipment_items.Remove(objSI);
            db.SaveChanges();
            return "Row deleted successfully";
        }

        //method write log entry to file
        public static void Log(string msg, StreamWriter log)
        {
            log.WriteLine("Error:" + msg);
            log.WriteLine();
        }

        [HttpPost]
        public string UpdateShipmentCommodity([Bind(Exclude = "Id")] shipment_items objSI, FormCollection formCollection, List<String> Bookings, List<String> Sequences, List<String> Descriptions, List<String> NMFCs, List<String> Subs, List<String> Classes, List<String> Pieces, List<String> Weight, List<String> Packaging, List<String> HazMat, List<String> Length, List<String> Width, List<String> Height, List<String> NMFCIDs, List<String> ShipperIDs, List<String> Density, List<String> Pallet)
        {
            string msg;

            //get the count of the Bookings array.  Only need the count of one array since all arrays have the same count.
            var listCount = Bookings.Count;
            
            //get the count of shipper ids
            var shipperCount = ShipperIDs.Count;
            
            for (int x = 1; x <= listCount; x++)
            {
                shipment_items itemObj = db.shipment_items.Find(Bookings[x - 1], Convert.ToInt16(Sequences[x - 1]));
                if (itemObj == null)
                //add record not found
                {
                    shipment_items itemRow = new shipment_items();
                    itemRow.pli_book_number = Bookings[x - 1];
                    itemRow.item_seq_number = Convert.ToInt16(Sequences[x - 1]);
                    itemRow.description = Descriptions[x - 1];
                    itemRow.nmfc = NMFCs[x - 1];
                    itemRow.sub = Subs[x - 1];
                    itemRow.@class = Classes[x - 1];
                    int weightVal = Convert.ToInt32(Weight[x - 1]);
                    itemRow.weight = (float)weightVal;
                    itemRow.pieces = Convert.ToInt16(Pieces[x - 1]);
                    itemRow.packaging = Packaging[x - 1];
                    itemRow.hazmat_notes = HazMat[x - 1];
                    itemRow.length = Length[x - 1];
                    itemRow.width = Width[x - 1];
                    itemRow.height = Height[x - 1];
                    itemRow.nmfc_id = NMFCIDs[x - 1];

                    if (Pallet[x - 1].Length > 0 || Pallet[x - 1] != "")
                    {
                        itemRow.pallet = Convert.ToInt16(Pallet[x - 1]);
                    }
                    else
                    {
                        itemRow.pallet = null;
                    }

                    if (Density[x - 1].Length > 0 || Density[x - 1] != "")
                    {
                        itemRow.density = Convert.ToDecimal(Density[x - 1]);
                    }
                    else
                    {
                        itemRow.density = null;
                    }

                    try
                    {
                        if (ModelState.IsValid)
                        {
                            db.shipment_items.Add(itemRow);
                            db.SaveChanges();
                        }
                        else
                        {
                            msg = "Validation data not successfull";
                        }
                    }
                    catch (Exception ex)
                    {
                        msg = "Error occured:" + ex.Message;
                        
                    }
                }
                //update found record
                else
                {
                    itemObj.description = Descriptions[x - 1];
                    itemObj.nmfc = NMFCs[x - 1];
                    itemObj.sub = Subs[x - 1];
                    itemObj.@class = Classes[x - 1];
                    int weightVal = Convert.ToInt32(Weight[x - 1]);
                    itemObj.weight = (float)weightVal;
                    itemObj.pieces = Convert.ToInt16(Pieces[x - 1]);
                    itemObj.packaging = Packaging[x - 1];
                    itemObj.hazmat_notes = HazMat[x - 1];
                    itemObj.length = Length[x - 1];
                    itemObj.width = Width[x - 1];
                    itemObj.height = Height[x - 1];
                    itemObj.nmfc_id = NMFCIDs[x - 1];
                    
                    if (Pallet[x - 1].Length > 0 || Pallet[x - 1] != "")
                    {
                        itemObj.pallet = Convert.ToInt16(Pallet[x - 1]);
                    }
                    else
                    {
                        itemObj.pallet = null;
                    }



                    if (Density[x - 1].Length > 0 || Density[x - 1] != "")
                    {
                        itemObj.density = Convert.ToDecimal(Density[x - 1]);
                    }
                    else
                    {
                        itemObj.density = null;
                    }
                     
                    
                    try
                    {
                        if (ModelState.IsValid)
                        {
                            db.SaveChanges();
                        }
                        else
                        {
                            msg = "Validation data not successfull";
                        }
                    }
                    catch (Exception ex)
                    {
                        msg = "Error occured:" + ex.Message;
                    }
                }
                //add nmfc to shipper nmfc list
                for (int y = 1; y <= shipperCount; y++) {                   
                    shipper_nmfc nmfcObj = db.shipper_nmfc.Find(Convert.ToInt64(ShipperIDs[y - 1]), Convert.ToInt16(NMFCIDs[x - 1]));            
                    if (nmfcObj == null)
                    {
                        shipper_nmfc nmfcRow = new shipper_nmfc();
                        nmfcRow.shipper_id = Convert.ToInt64(ShipperIDs[y - 1]);
                        nmfcRow.nmfc_id = Convert.ToInt16(NMFCIDs[x - 1]);
                        db.shipper_nmfc.Add(nmfcRow);
                        db.SaveChanges();
                    }
                }
            } //eo outer for

            msg = "Row saved successfully";
            return msg;
        }
        /*END*/

        /*CARRIER GRID METHODS*/
        //hard delete shipment carrier
        [HttpPost]
        public string DeleteShipmentCarrier(string booking)
        {
            string msg = "";

            try
            {
                if (ModelState.IsValid)
                {
                    //execute the stored procedure to hard delete the shipment carrier
                    db.DeleteShipmentCarrier(booking);

                    msg = "Row deleted successfully";
                }
                else
                {
                    msg = "Validation data not successfull";
                }
            }
            catch (Exception ex)
            {
                msg = "Error occured:" + ex.Message;
            }

            return msg;
        }

        //soft delete a carrier row from the grid
        [HttpPost]
        public string DeleteCarrierRow(string Booking, int Sequence)
        {
            string msg = "";
            shipment_carrier carrierObj = db.shipment_carrier.Find(Booking, Sequence);
            if (carrierObj != null)
            {
                //carrierObj.record_state = "0";
                db.shipment_carrier.Remove(carrierObj);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    db.SaveChanges();
                    msg = "Delete successful";
                }
                else
                {
                    msg = "Validation data not successful";
                }
            }
            catch (Exception ex)
            {
                msg = "Error occured:" + ex.Message;
            }

            return msg;
        }

        //method to get profile data of the carrier
        public carrierObj GetCarrierData(int carrierId, string toCountry)
        {
            //create a datatable to store the carrier data
            DataTable dtCarrier = CreateDataTable("select b.carrier_id, c.ModuleName, REPLACE(CONVERT(varchar, c.PLIRateEffDate, 102),'.','') as PLIRateEffDate, c.IsActive from carrier_tariff b, SMCDataModule c where b.carrier_id ='" + carrierId + "' and b.tariff_code_id = c.CodeId and c.IsActive=1 and b.to_country ='" + toCountry + "'");
            if (dtCarrier.Rows.Count > 0)
            {
                carrierObj carrier = new carrierObj();
                carrier.CarrierId = Convert.ToInt16(dtCarrier.Rows[0]["carrier_id"]);
                carrier.TariffName = Convert.ToString(dtCarrier.Rows[0]["ModuleName"]);
                carrier.PLIRateEffective = Convert.ToString(dtCarrier.Rows[0]["PLIRateEffDate"]);
                return carrier;
            }
            else
            {
                carrierObj carrier = new carrierObj();
                carrier.CarrierId = Convert.ToInt16(carrierId);
                carrier.TariffName = "KANECZ02";  //set as default
                carrier.PLIRateEffective = "19950101";  //set as default
                return carrier;
            }
        }
        
        //method to get the current FSC rate
        public decimal CalculateFSC(decimal rate)
        {
            decimal weekFSC;
            decimal fsc;
            DataTable dtFsc1 = CreateDataTable("SELECT TOP 1 FscPercent FROM FuelSurchargeWeekly ORDER BY WeekBeginningDate DESC");
            if (dtFsc1.Rows.Count > 0)
            {
                weekFSC = Convert.ToDecimal(dtFsc1.Rows[0]["FscPercent"]);
            }
            else
            {
                weekFSC = 0;
            }
            fsc = Math.Round(rate * (weekFSC / 100), 2);
            
            return fsc;
        }

        //method to calculate discount
        public decimal CalculateNetRate(decimal rwRate, decimal discountRate)
        {
            decimal discountValue;
            discountValue = Math.Round(discountRate * rwRate, 2);
            var netRate = rwRate - discountValue;
            return netRate;
        }

        //method to calculate carrier pricing
        public decimal CalculateCarrierRate(List<String> dataWeight, List<String> dataClass, String orgZip, String destZip, String customerId, String carrierId)
        {
            carrierObj carrierData;
            decimal rwRate;
            var tariff = "";
            var effectiveDate = "";

            //get carrier tariff profile data
            carrierData = GetCarrierData(Convert.ToInt16(carrierId),"USA");
            tariff = carrierData.TariffName;
            //tariff = "KANECZ02";
            effectiveDate = carrierData.PLIRateEffective;
            //effectiveDate = "19950101";

            //get the dataweight count, only need to count one array since all arrays the same counts
            var listCount = dataWeight.Count;
       
            //determine if carrier customer FAK exceptions exist
            //create a datatable to store the FAK exceptions
            DataTable dtExceptions = CreateDataTable("SELECT a.fak, a.fak_min, a.fak_max FROM fak_exceptions a, carrier_customer_fak b WHERE b.customer_id ='" + customerId + "' AND b.carrier_id ='" + carrierId + "' AND b.fak_id = a.fak_id");
            if (dtExceptions.Rows.Count > 0)  //if exceptions found
            {
                for (int x = 1; x <= listCount; x++)
                {
                    for (int y = 1; y <= dtExceptions.Rows.Count; y++)
                    {
                        if (Convert.ToDecimal(dataClass[x - 1]) >= Convert.ToDecimal(dtExceptions.Rows[y - 1]["fak_min"]) && Convert.ToDecimal(dataClass[x - 1]) <= Convert.ToDecimal(dtExceptions.Rows[y - 1]["fak_max"]))
                        {
                            dataClass[x - 1] = Convert.ToString(dtExceptions.Rows[y - 1]["fak"]);
                            break;
                        }
                    }
                }
            }
             
            //call method to build the commodity array to use with RateWare
            LTLRequestDetail[] dtCommAry = CreateCommodityArray(dataWeight, dataClass, listCount);

            //rate shipment using RateWare
            var rwRsp = RateLTLShipment(orgZip, destZip, tariff, effectiveDate, dtCommAry);

            //get rate from result from RateWare
            rwRate = Convert.ToDecimal(rwRsp.totalCharge);

            return rwRate;
        }

        //method to get the transit days of the selected carrier
        public string GetCarrierTransitDays(Int16 carrierId, string orgZip, string destZip)
        {
            var scac = GetSCAC("SELECT ISNULL(scac,'N/A') AS carrierSCAC FROM carrier WHERE carrier_id=" + carrierId);

            //begin CarrierConnect Code
            CarrierConnectWSPortTypeClient ccws = new CarrierConnectWSPortTypeClient();
            //point the client at the web service URL
            ccws.Endpoint.Address = new System.ServiceModel.EndpointAddress(@"http://applications.smc3.com/AdminManager/services/CarrierConnectWS");

            IntraX2.CarrierConnectService.AuthenticationToken authCCToken = new IntraX2.CarrierConnectService.AuthenticationToken();
            //get the authentication information out of the authentication.settings property file
            authCCToken.licenseKey = Properties.authentication.Default.licenseKeyCC;
            authCCToken.password = Properties.authentication.Default.password;
            authCCToken.username = Properties.authentication.Default.username;

            //is the web service ready?
            bool bCCReady = ccws.isReady(authCCToken);
            if (bCCReady != true)
            {
                //ViewData["Errormsg"] = "Connect Carrier serivce is not accepting requests";
                //return View("ErrorView1");
            }

            //build a transit request
            TransitRequest transitRequest = new TransitRequest();
            TransitResponse transitResponse = new TransitResponse();
            IntraX2.CarrierConnectService.ShipmentLocale originCC = new IntraX2.CarrierConnectService.ShipmentLocale();
            IntraX2.CarrierConnectService.ShipmentLocale destinationCC = new IntraX2.CarrierConnectService.ShipmentLocale();

            //get transit days
            originCC.postalCode = orgZip;
            originCC.countryCode = "USA";
            destinationCC.postalCode = destZip;
            destinationCC.countryCode = "USA";
            transitRequest.origin = originCC;
            transitRequest.destination = destinationCC;
            ScacRequest[] dtScacAry = new ScacRequest[1];
            ScacRequest rdScac = new ScacRequest();

            //assign the selected carrier scac to the request scac parameter
            rdScac.scac = scac;
            dtScacAry[0] = rdScac;
            transitRequest.scacs = dtScacAry;

            //call Transit web service method
            try
            {
                transitResponse = ccws.Transit(authCCToken, transitRequest);
                ScacResponse[] scacRespones = transitResponse.scacResponses;
                var transDays = Convert.ToString(scacRespones[0].days);
                //ViewData["serviceType"] = scacRespones[0].originServiceType;

                //transitDays ccDays = new transitDays();
                //ccDays.days = transDays;
                //return Json(new { days = ccDays.days }, JsonRequestBehavior.AllowGet);
                return transDays;
            }
            catch
            {
                var transDays = "N/A";
                //ViewData["serviceType"] = "I";

                //transitDays ccDays = new transitDays();
                //ccDays.days = transDays;
                //return Json(new { days = ccDays.days }, JsonRequestBehavior.AllowGet);
                return transDays;
            }
        }

        //get shipment_carrier rows for carrierGrid
        public JsonResult GetCarrierRows(string sidx, string sord, int page, int rows)
        {
            string booking = Request.QueryString["booking"];
            //string booking = "1216840";
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            var itemsResults = from scr in db.shipment_carrier where scr.pli_book_number == booking && (scr.record_state == null || scr.record_state == "1") select scr;
            /*
            var itemsResults = (from ss in db.shipment_shipper
                               join s in db.shippers on ss.shipper_id equals s.shipper_id
                               where ss.pli_book_number == booking
                               select new
                               {
                                ss.pli_book_number,
                                ss.shipper_id,
                                ss.location_id,
                                ss.location_contact_id,
                                ss.reference_number,
                                ss.actual_pu_date,
                                ss.sequence,
                                s.name
                               }).ToList();
              */

            int totalRecords = itemsResults.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord.ToUpper() == "DESC")
            {
                itemsResults = itemsResults.OrderByDescending(scr => scr.sequence);
                itemsResults = itemsResults.Skip(pageIndex * pageSize).Take(pageSize);
            }
            else
            {
                itemsResults = itemsResults.OrderBy(scr => scr.sequence);
                itemsResults = itemsResults.Skip(pageIndex * pageSize).Take(pageSize);
            }
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = itemsResults
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        //public ActionResult GetCarrierRows2(string sidx, string sord, int page, int rows, List<String> dataWeight, List<String> dataClass, String orgZip, String destZip, int customerId)
        public ActionResult GetCarrierRows2(string sidx, string sord, int page, int rows, List<String> dataWeight, List<String> dataClass)
        {
            //get the dataweight count, only need to count one array since all arrays the same counts
            //var listCount = dataWeight.Count;
            Console.Write(dataWeight.Count);
            
            //THIS WORKS
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
           
            carrierRow car1 = new carrierRow()
            {
                pli_book_number = "1234567",
                carrier_id = "4662",
                carrier_charges = "150.75",
                transit_days = "3"
            };

            List<carrierRow> carriers = new List<carrierRow>();
            carriers.Add(car1);

            string json = JsonConvert.SerializeObject(carriers, Formatting.Indented);

            int totalRecords = carriers.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = carriers
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
            
        }

        [HttpPost]
        public string CreateShipmentCarrier([Bind(Exclude = "Id")] shipment_carrier objSSCR, FormCollection formCollection, List<String> Bookings, List<Int32> Carriers, List<Int16> CarrierLocs, List<Int16> CarrierLocCons, List<Int16> Sequences, List<String> Roles, List<String> Pros, List<Decimal> Charges, List<String> Days, List<String> Dispatched, List<String> Notes, List<String> ServicePoints, List<Boolean> Received, List<DateTime> InvoicedDates)
        {
            string msg;
            //StreamWriter log;

            //get the count of the Bookings array.  Only need the count of one array since all arrays have the same count.
            var listCount = Bookings.Count;

            
            for (int x = 1; x <= listCount; x++)
            {
                shipment_carrier carrierObj = db.shipment_carrier.Find(Bookings[x - 1], Sequences[x - 1]);
                if (carrierObj == null)
                //row not found, add row
                {
                    shipment_carrier carrierRow = new shipment_carrier();
                    carrierRow.pli_book_number = Bookings[x - 1];
                    carrierRow.carrier_id = Carriers[x - 1];
                    carrierRow.location_id = CarrierLocs[x - 1];
                    carrierRow.location_contact_id = CarrierLocCons[x - 1];
                    carrierRow.sequence = Sequences[x - 1];
                    carrierRow.role_id = Roles[x - 1];
                    carrierRow.pro_number = Pros[x - 1];
                    
                    carrierRow.charges = Charges[x - 1];
                    carrierRow.transit_days = Days[x - 1];
                    carrierRow.dispatched_date_time = Dispatched[x - 1];
                    carrierRow.notes = Notes[x - 1];
                    carrierRow.service_point = ServicePoints[x - 1];
                    carrierRow.invoice_received = Received[x - 1];
                    carrierRow.invoiced_date = InvoicedDates[x - 1];

                    try
                    {
                        if (ModelState.IsValid)
                        {
                            db.shipment_carrier.Add(carrierRow);
                            db.SaveChanges();
                        }
                        else
                        {
                            msg = "Validation data not successfull";
                        }
                    }
                    catch (Exception ex)
                    {
                        msg = "Error occured:" + ex.Message;
                    }
                    
                } //eo if
                else
                //row found, update row data
                {
                    carrierObj.carrier_id = Carriers[x - 1];
                    carrierObj.location_id = CarrierLocs[x - 1];
                    carrierObj.location_contact_id = CarrierLocCons[x - 1];
                    carrierObj.role_id = Roles[x - 1];
                    carrierObj.pro_number = Pros[x - 1];
                    carrierObj.charges = Charges[x - 1];
                    carrierObj.transit_days = Days[x - 1];
                    carrierObj.dispatched_date_time = Dispatched[x - 1];
                    carrierObj.notes = Notes[x - 1];
                    carrierObj.service_point = ServicePoints[x - 1];
                    carrierObj.invoice_received = Received[x - 1];
                    carrierObj.invoiced_date = InvoicedDates[x - 1];
                    try
                    {
                        if (ModelState.IsValid)
                        {
                            db.SaveChanges();
                        }
                        else
                        {
                            msg = "Validation data not successfull";
                        }
                    }
                    catch (Exception ex)
                    {
                        msg = "Error occured:" + ex.Message;
                    }
                } //eo else
            }
            
            msg = "Row saved successfully";
            return msg;
        }
        
        public string EditShipmentCarrier(shipment_carrier objSSCR)
        {
            string msg;
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(objSSCR).State = EntityState.Modified;
                    db.SaveChanges();
                    msg = "Row saved successfully";
                }
                else
                {
                    msg = "Validation data not successfull";
                }
            }
            catch (Exception ex)
            {
                msg = "Error occured:" + ex.Message;
            }
            return msg;
        }

        public string DeleteShipmentCarrier(string booking, int sequence)
        {
            //Id comes from the View.  Id is the default row number.
            //booking and sequence come from the delData parameters of the View.
            //return booking + "-" + Convert.ToString(sequence);

            shipment_carrier objSSCR = db.shipment_carrier.Find(booking, sequence);
            db.shipment_carrier.Remove(objSSCR);
            db.SaveChanges();
            return "Row deleted successfully";
        }
        /*END*/

        [HttpPost]
        public string UpdateCarrierBills([Bind(Exclude = "Id")] shipment_carrier objShipmentCarrier, FormCollection formCollection, List<String> Bookings, List<DateTime> InvoiceDate, List<Int16> Sequences, List<String> Select)
        {
            string msg = "";

            //open database connection
            connection.Open();

            //get the count of the Bookings array.  Only need the count of one array since all arrays have the same count.
            var listCount = Bookings.Count;

            for (int x = 1; x <= listCount; x++)
            {
                if (Convert.ToBoolean(Select[x - 1]))  //only update selected bookings
                {
                    shipment_carrier carrierObj = db.shipment_carrier.Find(Bookings[x - 1], Sequences[x - 1]);
                    if (carrierObj != null)
                    {
                        try
                        {
                            SqlCommand command = new SqlCommand("UPDATE shipment_carrier SET invoiced_date='" + InvoiceDate[x - 1] + "' ,invoice_received='" + Select[x - 1] + "' WHERE pli_book_number='" + Bookings[x - 1] + "' AND sequence=" + Sequences[x - 1], connection);
                            command.ExecuteNonQuery();
                            msg = "Row updated successfully";
                        }
                        catch (Exception exp)
                        {
                            return exp.Message;
                        }

                        /*save this code for future debugging from controller*/
                        /*
                        try
                        {
                            db.SaveChanges();
                        }
                        catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                        {
                            Exception raise = dbEx;
                            foreach (var validationErrors in dbEx.EntityValidationErrors)
                            {
                                foreach (var validationError in validationErrors.ValidationErrors)
                                {
                                    string message = string.Format("{0}:{1}",
                                    validationErrors.Entry.Entity.ToString(),
                                    validationError.ErrorMessage);
                                    // raise a new exception nesting
                                    // the current instance as InnerException
                                    raise = new InvalidOperationException(message, raise);
                                }
                            }
                                throw raise;
                        }
                        */
                    } //eo if

                } //eo if select = 1    
            } //eo for
            connection.Close();
            return msg;
        }

        [HttpPost]
        public string UpdateShipmentInvoices([Bind(Exclude = "Id")] shipment objShipment, FormCollection formCollection, List<String> Bookings, List<DateTime> InvoiceDate, List<DateTime> DueDate, List<String> Select)
        {
            string msg = "";
            
            //open database connection
            connection.Open();
            
            //get the count of the Bookings array.  Only need the count of one array since all arrays have the same count.
            var listCount = Bookings.Count;

            for (int x = 1; x <= listCount; x++)
            {
                if (Convert.ToBoolean(Select[x - 1]))  //only update selected bookings
                {
                    shipment shipmentObj = db.shipments.Find(Bookings[x - 1]);
                    if (shipmentObj != null)
                    {                        
                        try
                        {
                            SqlCommand command = new SqlCommand("UPDATE shipment SET customer_invoice_date='" + InvoiceDate[x - 1] + "' ,customer_due_date='" + DueDate[x - 1] + "' WHERE pli_book_number='" + Bookings[x - 1] + "'", connection);
                            command.ExecuteNonQuery();
                            msg = "Row updated successfully";
                        }
                        catch (Exception exp)
                        {
                            return exp.Message;
                        }
                       
                        /*save this code for future debugging from controller*/
                        /*
                        try
                        {
                            db.SaveChanges();
                        }
                        catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                        {
                            Exception raise = dbEx;
                            foreach (var validationErrors in dbEx.EntityValidationErrors)
                            {
                                foreach (var validationError in validationErrors.ValidationErrors)
                                {
                                    string message = string.Format("{0}:{1}",
                                    validationErrors.Entry.Entity.ToString(),
                                    validationError.ErrorMessage);
                                    // raise a new exception nesting
                                    // the current instance as InnerException
                                    raise = new InvalidOperationException(message, raise);
                                }
                            }
                                throw raise;
                        }
                        */
                    } //eo if
                
                } //eo if select = 1    
            } //eo for
            connection.Close();
            return msg;
        }
       
        public decimal GetInsuranceFees(decimal shipmentValue)
        {
            decimal insFees = 0;
            //create a datatable to store the insurance fees table
            DataTable dtInsurance = CreateDataTable("SELECT minimum, maximum, fees FROM insurance_fees");
            for (int y = 1; y <= dtInsurance.Rows.Count; y++)
            {
                if (shipmentValue >= Convert.ToDecimal(dtInsurance.Rows[y - 1]["minimum"]) && shipmentValue <= Convert.ToDecimal(dtInsurance.Rows[y - 1]["maximum"]))
                {
                    insFees = Convert.ToDecimal(dtInsurance.Rows[y - 1]["fees"]);
                    break;
                }
            }
            return insFees;
        }

        //soft delete a shipment notes row from the grid
        [HttpPost]
        public string DeleteShipmentNotesRow(string Booking, int Sequence)
        {
            string msg = "";
            
            //open database connection
            connection.Open();

            try
            {
                SqlCommand command = new SqlCommand("UPDATE ShipmentNotes SET record_state='1' WHERE pli_book_number='" + Booking + "' AND ShipmentNoteID=" + Sequence, connection);
                command.ExecuteNonQuery();
                msg = "Row updated successfully";
            }
            catch (Exception exp)
            {
                connection.Close();
                return exp.Message;
            }

            connection.Close();
            return msg;
        }

        //get the function role code
        public ActionResult GetFunctionRoleCode(string RoleID, string FunctionDesc)
        {
            string code;
            DataTable dtCode = CreateDataTable("SELECT (CHARINDEX('" + RoleID + "', RoleAllowed)) AS result FROM PflinFunctionRole WHERE Description = '" + FunctionDesc + "'");
            code = Convert.ToString(dtCode.Rows[0]["result"]);
            return Json(new { code = code}, JsonRequestBehavior.AllowGet);
        }

        //update shipment status and status date
        [HttpPost]
        public string UpdateShipmentStatus(string Booking, string Status, string StatusDate)
        {
            string msg = "";
            shipment shipmentObj = db.shipments.Find(Booking);
            if (shipmentObj != null)
            {
                shipmentObj.status = Status;
                shipmentObj.status_date = Convert.ToDateTime(StatusDate);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    db.SaveChanges();
                    msg = "Update successful";
                }
                else
                {
                    msg = "Update not successful";
                }
            }
            catch (Exception ex)
            {
                msg = "Error occured:" + ex.Message;
            }

            return msg;
        }

        //soft delete a accessorial row from the grid
        [HttpPost]
        public string DeleteAccessorialRow(string Booking, int Sequence)
        {
            string msg = "";
            shipment_accessorial accessorialObj = db.shipment_accessorial.Find(Booking, Sequence);
            if (accessorialObj != null)
            {
                accessorialObj.record_state = "0";
            }

            try
            {
                if (ModelState.IsValid)
                {
                    db.SaveChanges();
                    msg = "Delete successful";
                }
                else
                {
                    msg = "Validation data not successful";
                }
            }
            catch (Exception ex)
            {
                msg = "Error occured:" + ex.Message;
            }

            return msg;
        }

        [HttpPost]
        public string CreateShipmentNotes(List<String> Bookings, List<String> Sequences, List<String> Notes, List<String> CreatedBy, List<String> CreatedDate)
        {
            string msg = "";
            Int32 maxval = 0;

            //get the count of the Bookings array.  Only need the count of one array since all arrays have the same count.
            var listCount = Bookings.Count;

            for (int x = 1; x <= listCount; x++)
            {
                ShipmentNote shipmentNoteObj = db.ShipmentNotes.Find(Convert.ToInt32(Sequences[x - 1]), Bookings[x - 1]);

                if (shipmentNoteObj == null)
                //add record not found
                {
                    /*get the max shipment note id*/
                    using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ToString()))
                    {
                        conn.Open();
                        SqlCommand cmd = new SqlCommand("GetMaxShipmentNoteID", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            if (!reader.IsDBNull(reader.GetOrdinal("maxval")))
                            {
                                maxval = reader.GetInt32(0);
                            }
                            else
                            {
                                maxval = 0;
                            }
                        }
                        reader.Close();
                        cmd.Dispose();
                    };

                    maxval = maxval + 1;
                    ShipmentNote itemRow = new ShipmentNote();
                    itemRow.pli_book_number = Bookings[x - 1];
                    itemRow.ShipmentNoteID = maxval;
                    itemRow.InternalNote = Notes[x - 1];
                    itemRow.CreatedBy = Convert.ToInt16(CreatedBy[x - 1]);
                    itemRow.CreatedDate = Convert.ToDateTime(CreatedDate[x - 1]);
                    
                    /*insert row to database*/
                    try
                    {
                        if (ModelState.IsValid)
                        {
                            db.ShipmentNotes.Add(itemRow);
                            db.SaveChanges();                          
                            msg = "Row added successfully";
                        }
                        else
                        {
                            msg = "Validation data not successfull";
                        }
                    }
                    catch (Exception ex)
                    {
                        msg = "Error occured:" + ex.Message;
                    }
                }
                //update found record
                else
                {
                    try
                    {
                        shipmentNoteObj.InternalNote = Notes[x - 1];
                        shipmentNoteObj.CreatedBy = Convert.ToInt16(CreatedBy[x - 1]);
                        shipmentNoteObj.CreatedDate = Convert.ToDateTime(CreatedDate[x - 1]);
                        db.SaveChanges();
                        msg = "Row updated successfully";
                    }
                    catch (Exception ex)
                    {
                        msg = "Error occured:" + ex.Message;
                    }
                }
            } //eo for
            //msg = "Row saved successfully";
            return msg;
        }

        [HttpPost]
        public string CreateAccessorial([Bind(Exclude = "Id")] shipment_accessorial objSA, FormCollection formCollection, List<String> Bookings, List<String> Sequences, List<String> Accessorial, List<String> Fees)
        {
            string msg;

            //get the count of the Bookings array.  Only need the count of one array since all arrays have the same count.
            var listCount = Bookings.Count;

            for (int x = 1; x <= listCount; x++)
            {
                shipment_accessorial accessorialObj = db.shipment_accessorial.Find(Bookings[x - 1], Convert.ToInt16(Sequences[x - 1]));

                if (accessorialObj == null)
                //add record not found
                {
                    shipment_accessorial itemRow = new shipment_accessorial();
                    itemRow.pli_book_number = Bookings[x - 1];
                    itemRow.sequence = Convert.ToInt16(Sequences[x - 1]);
                    itemRow.accessorial_id = Convert.ToInt16(Accessorial[x - 1]);
                    itemRow.fee = Convert.ToDecimal(Fees[x - 1]);

                    try
                    {
                        if (ModelState.IsValid)
                        {
                            db.shipment_accessorial.Add(itemRow);
                            db.SaveChanges();
                        }
                        else
                        {
                            msg = "Validation data not successfull";
                        }
                    }
                    catch (Exception ex)
                    {
                        msg = "Error occured:" + ex.Message;
                    }
                }
                //update found record
                else
                {
                    accessorialObj.accessorial_id = Convert.ToInt16(Accessorial[x - 1]);
                    accessorialObj.fee = Convert.ToDecimal(Fees[x - 1]);

                    try
                    {
                        if (ModelState.IsValid)
                        {
                            db.SaveChanges();
                        }
                        else
                        {
                            msg = "Validation data not successfull";
                        }
                    }
                    catch (Exception ex)
                    {
                        msg = "Error occured:" + ex.Message;
                    }
                }
            } //eo for
            msg = "Row saved successfully";
            return msg;
        }

        //get ShipmentNotes rows for shipmentNotesGrid
        public JsonResult GetShipmentNotesRows(string booking)
        {
            var list = db.pflin_Shpmt_GetShipmentNote_IX2(booking).ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        
        //get shipment_accessorial rows for accessorialGrid
        public JsonResult GetAccessorialRows(string sidx, string sord, int page, int rows)
        {
            string booking = Request.QueryString["booking"];
            //string booking = "1216840";
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            var itemsResults = from sa in db.shipment_accessorial where sa.pli_book_number == booking && (sa.record_state == null || sa.record_state == "1") select sa;
            /*
            var itemsResults = (from ss in db.shipment_shipper
                               join s in db.shippers on ss.shipper_id equals s.shipper_id
                               where ss.pli_book_number == booking
                               select new
                               {
                                ss.pli_book_number,
                                ss.shipper_id,
                                ss.location_id,
                                ss.location_contact_id,
                                ss.reference_number,
                                ss.actual_pu_date,
                                ss.sequence,
                                s.name
                               }).ToList();
              */

            int totalRecords = itemsResults.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord.ToUpper() == "DESC")
            {
                itemsResults = itemsResults.OrderByDescending(sa => sa.sequence);
                itemsResults = itemsResults.Skip(pageIndex * pageSize).Take(pageSize);
            }
            else
            {
                itemsResults = itemsResults.OrderBy(s => s.sequence);
                itemsResults = itemsResults.Skip(pageIndex * pageSize).Take(pageSize);
            }
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = itemsResults
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        
        //get the current week FSC rate
        public ActionResult GetWeekFSCRate()
        {
            decimal weekFSC;
            DataTable dtFsc = CreateDataTable("SELECT TOP 1 FscPercent FROM FuelSurchargeWeekly ORDER BY WeekBeginningDate DESC");
            if (dtFsc.Rows.Count > 0)
            {
                weekFSC = Convert.ToDecimal(dtFsc.Rows[0]["FscPercent"]);
            }
            else
            {
                weekFSC = 0;
            }
            return Json(new { weekFSC = Convert.ToString(weekFSC) }, JsonRequestBehavior.AllowGet);
        }

        //method to get rate from SMC rateware webservice
        public static LTLRateShipmentResponse RateLTLShipment(string originZip, string destinationZip, string customerTariff, string effectiveDate, LTLRequestDetail[] dtAry)
        {
            RateWareXLPortTypeClient rw = new RateWareXLPortTypeClient();
            IntraX2.RateWareServices.AuthenticationToken authToken = new IntraX2.RateWareServices.AuthenticationToken();

            //get the authentication information out of the authentication.settings property file
            authToken.licenseKey = Properties.authentication.Default.licensekey;
            authToken.password = Properties.authentication.Default.password;
            authToken.username = Properties.authentication.Default.username;

            //is the web service ready?
            bool bReady = rw.isReady(authToken);
            //Console.Out.WriteLine("Is the web service available? {0}", bReady);
            if (bReady != true)
            {
                //ViewData["Errormsg"] = "Rating serivce is not accepting requests";
                //return View("ErrorView");
            }

            LTLRateShipmentRequest rs = new LTLRateShipmentRequest();
            rs.originCountry = "USA";
            rs.originPostalCode = originZip;
            rs.destinationCountry = "USA";
            rs.destinationPostalCode = destinationZip;
            //rs.tariffName = "PATHCZ02";
            rs.tariffName = customerTariff;
            //rs.shipmentDateCCYYMMDD = "20130915";
            rs.shipmentDateCCYYMMDD = effectiveDate;

            //assign the commodities array to the detail of the request
            rs.details = dtAry;

            //rate shipment
            LTLRateShipmentResponse rsp = rw.LTLRateShipment(authToken, rs);
            return rsp;
        }

        //method to build array of commodities to use with RateWare
        public static LTLRequestDetail[] CreateCommodityArray(List<string> dataWeight, List<string> dataClass, int listCount)
        {
            LTLRequestDetail[] dtAry = new LTLRequestDetail[listCount];  //declare array variable to store commodities

            for (int x = 1; x <= listCount; x++)
            {
                LTLRequestDetail rd = new LTLRequestDetail();
                rd.nmfcClass = Convert.ToString(dataClass[x - 1]);
                rd.weight = Convert.ToString(dataWeight[x - 1]);

                dtAry[x - 1] = rd;
            }

            return dtAry;
        }

        public ActionResult GetZipZones(String orgZip, String destZip)
        {
            string strFZone = "";
            string strTZone = "";
            
            //build zone zips datatable
            DataTable dtZoneZips = CreateDataTable("SELECT zone_id, zip_begin, zip_end FROM zone_zip ORDER BY zone_id");

            //build a list zones that matchs the origin/destination zip pair
            foreach (DataRow row in dtZoneZips.Rows)
            {
                decimal zipbegin = Convert.ToDecimal(row["zip_begin"]);
                decimal zipend = Convert.ToDecimal(row["zip_end"]);

                if (IsBetween(Convert.ToDecimal(orgZip), zipbegin, zipend))
                {
                    strFZone = strFZone + "'" + Convert.ToString(row["zone_id"]) + "'" + ",";
                }

                if (Convert.ToDecimal(orgZip) == zipbegin)
                {
                    strFZone = strFZone + "'" + Convert.ToString(row["zone_id"]) + "'" + ",";
                }

                if (IsBetween(Convert.ToDecimal(destZip), zipbegin, zipend))
                {
                    strTZone = strTZone + "'" + Convert.ToString(row["zone_id"]) + "'" + ",";
                }

                if (Convert.ToDecimal(destZip) == zipend)
                {
                    strTZone = strTZone + "'" + Convert.ToString(row["zone_id"]) + "'" + ",";
                }
            }

            //remove the last extra comma at the end of string
            strFZone = strFZone.TrimEnd(',');
            strTZone = strTZone.TrimEnd(',');

            var jsonData = new
            {
                fzone = strFZone,
                tzone = strTZone
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        /*
        public ActionResult CalculateCustomerRate(List<String> dataWeight, List<String> dataClass, String orgZip, String destZip, String customerId, String carrierId)
        {
            decimal rwRate;
            decimal netRate = 0;
            decimal fsc = 0;
            decimal rate = 0;
            customerObj customerData;
            var tariff = "";
            var effectiveDate = "";
            string strFZone = "";
            string strTZone = "";
            DataTable dtCarriers = new DataTable();
            var dtCount = 0;
            var localCarrierId = "";
            decimal amc = 0;
            decimal discount = 0;
            var customerCharges = "";
            bool customerPricing;

            //get customer profile data
            customerData = GetCustomerData(Convert.ToInt16(customerId));
            tariff = customerData.TariffName;
            effectiveDate = customerData.PLIRateEffective;
            customerPricing = customerData.IsRateException;
            
            //get the list item count, only need to count one array since they both have the same count
            var listCount = dataWeight.Count;
          
            //determine if customer carrier FAK exceptions exist
            //create a datatable to store the FAK exceptions
            DataTable dtExceptions = CreateDataTable("SELECT a.fak, a.fak_min, a.fak_max FROM fak_exceptions a, customer_carrier_fak b WHERE b.CustomerId ='" + customerId + "' AND b.carrier_id ='" + carrierId + "' AND b.fak_id = a.fak_id");
            if (dtExceptions.Rows.Count > 0)  //if exceptions found
            {
                for (int x = 1; x <= listCount; x++)
                {
                    for (int y = 1; y <= dtExceptions.Rows.Count; y++)
                    {
                        if (Convert.ToDecimal(dataClass[x - 1]) >= Convert.ToDecimal(dtExceptions.Rows[y - 1]["fak_min"]) && Convert.ToDecimal(dataClass[x - 1]) <= Convert.ToDecimal(dtExceptions.Rows[y - 1]["fak_max"]))
                        {
                            dataClass[x - 1] = Convert.ToString(dtExceptions.Rows[y - 1]["fak"]);
                            break;
                        }
                    }
                }
            }

            //build zone zips datatable
            DataTable dtZoneZips = CreateDataTable("SELECT zone_id, zip_begin, zip_end FROM zone_zip ORDER BY zone_id");

            //build a list zones that matchs the origin/destination zip pair
            foreach (DataRow row in dtZoneZips.Rows)
            {
                decimal zipbegin = Convert.ToDecimal(row["zip_begin"]);
                decimal zipend = Convert.ToDecimal(row["zip_end"]);

                if (IsBetween(Convert.ToDecimal(orgZip), zipbegin, zipend))
                {
                    strFZone = strFZone + "'" + Convert.ToString(row["zone_id"]) + "'" + ",";
                }

                if (Convert.ToDecimal(orgZip) == zipbegin)
                {
                    strFZone = strFZone + "'" + Convert.ToString(row["zone_id"]) + "'" + ",";
                }

                if (IsBetween(Convert.ToDecimal(destZip), zipbegin, zipend))
                {
                    strTZone = strTZone + "'" + Convert.ToString(row["zone_id"]) + "'" + ",";
                }
                
                if (Convert.ToDecimal(destZip) == zipend)
                {
                    strTZone = strTZone + "'" + Convert.ToString(row["zone_id"]) + "'" + ",";
                }
            }

            //remove the last extra comma at the end of string
            strFZone = strFZone.TrimEnd(',');
            strTZone = strTZone.TrimEnd(',');

            //if customer does not have zone pricing exception
            if (!customerPricing)
            {
                DataTable dtCustomerRow = CreateDataTable("SELECT discount,absolute_minimum FROM customer_pricing WHERE customer_id = " + customerId);
                if (dtCustomerRow.Rows.Count > 0)
                {
                    amc = Convert.ToDecimal(dtCustomerRow.Rows[0]["absolute_minimum"]);
                    discount = Convert.ToDecimal(dtCustomerRow.Rows[0]["discount"]);
                }
            }
            else {
                DataTable dtCustomerRow = CreateDataTable("SELECT distinct(customer_id) FROM customer_pricing WHERE customer_id = " + customerId);
                if (dtCustomerRow.Rows.Count > 0)  //use customer carriers pricing
                {
                    using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ToString()))
                    {
                        conn.Open();
                        SqlCommand cmd = new SqlCommand("GetCustomerCarriersPricingByZones", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@strCustomerID", SqlDbType.NVarChar).Value = customerId;
                        cmd.Parameters.Add("@strFromZones", SqlDbType.NVarChar).Value = strFZone;
                        cmd.Parameters.Add("@strToZones", SqlDbType.NVarChar).Value = strTZone;
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.HasRows)
                        {
                            dtCarriers.Load(reader);
                            dtCount = dtCarriers.Rows.Count;
                        }
                        reader.Close();
                        cmd.Dispose();
                    };
                }
                
                List<carrierRow> carriers = new List<carrierRow>();

                for (int x = 0; x <= dtCount - 1; x++)
                {
                    localCarrierId = Convert.ToString(dtCarriers.Rows[x]["carrier_id"]);
                    if (localCarrierId.Trim() == carrierId.Trim())
                    {
                        amc = Convert.ToDecimal(dtCarriers.Rows[x]["absolute_minimum"]);
                        discount = Convert.ToDecimal(dtCarriers.Rows[x]["discount"]);
                        break;
                    }
                }
            }

            //call method to build the commodity array to use with RateWare
            LTLRequestDetail[] dtCommAry = CreateCommodityArray(dataWeight, dataClass, listCount);

            //rate shipment using RateWare
            var rwRsp = RateLTLShipment(orgZip, destZip, tariff, effectiveDate, dtCommAry);

            //get rate from result from RateWare
            rwRate = Convert.ToDecimal(rwRsp.totalCharge);

            //calculate netrate with discound applied
            netRate = CalculateNetRate(rwRate, discount);

            //determine if AMC applies.  Returns the larger of the two values
            rate = Math.Max(amc, netRate);

            //calculate the fsc charge
            fsc = CalculateFSC(rate);

            customerCharges = Convert.ToString(rate);

            var jsonData = new
            {
                fsc = Convert.ToString(fsc),
                customerRate = Convert.ToString(customerCharges)
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        */

        public bool IsBetween(decimal valToCheck, decimal begin, decimal end)
        {
            return (valToCheck >= begin && valToCheck < end);
        }

        public ActionResult CalculateSingleCarrierRate(List<String> dataWeight, List<String> dataClass, String orgZip, String destZip, String bookingNumber, String carrierID)
        {
            var transitDays = "";
            var rwRate = "";
            decimal netRate = 0;
            decimal rate = 0;
            decimal fsc = 0;
            var carrierCharges = "";
            
            transitDays = GetCarrierTransitDays(Convert.ToInt16(carrierID), orgZip, destZip);

            rwRate = Convert.ToString(CalculateCarrierRate(dataWeight, dataClass, orgZip, destZip, "", carrierID));
            //netRate = CalculateNetRate(Convert.ToDecimal(rwRate), Convert.ToDecimal(dtCarriers.Rows[x]["discount"]));
            netRate = Convert.ToDecimal(rwRate);

            //determine if AMC applies.  Returns the larger of the two values
            //rate = Math.Max(Convert.ToDecimal(dtCarriers.Rows[x]["absolute_minimum"]), netRate);
            rate = netRate;

            //calculate the fsc charge
            fsc = CalculateFSC(rate);

            carrierCharges = Convert.ToString(rate + fsc);

            var jsonData = new
            {
                pli_book_number = bookingNumber,
                role_id = "NA",
                carrier_id = carrierID,
                service_point = "",
                pro_number = "",
                carrier_charges = carrierCharges,
                transit_days = transitDays,
                invoiced_date = "",
                dispatched_date_time = "",
                notes = "",
                carrier_sequence = Convert.ToString(1)
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        //method to calculate carrier pricing
        public ActionResult CalculateCarriersRate(List<String> dataWeight, List<String> dataClass, String orgZip, String destZip, String bookingNumber, String customerID)
        {
            /*
            List<String> dataWeight = new List<String>()
            {
                "500"
            };
            List<String> dataClass = new List<String>()
            {
                "150"
            };
            */
           
            decimal fsc = 0;
            var carrierCharges = "";
            decimal rate = 0;
            decimal netRate = 0;
            var rwRate = "";
            var carrierID = "";
            var transitDays = "";
            var dtCount = 0;
            string strFZone = "";
            string strTZone = "";
            DataTable dtCarriers = new DataTable();
            DataTable dtZoneZips = CreateDataTable("SELECT zone_id, zip_begin, zip_end FROM zone_zip ORDER BY zone_id");
            
            //build a list zones that matchs the origin/destination zip pair
            foreach (DataRow row in dtZoneZips.Rows)
            {
                decimal zipbegin = Convert.ToDecimal(row["zip_begin"]);
                decimal zipend = Convert.ToDecimal(row["zip_end"]);

                if (IsBetween(Convert.ToDecimal(orgZip), zipbegin, zipend))
                {
                    strFZone = strFZone + "'" + Convert.ToString(row["zone_id"]) + "'" + ",";
                }

                if (IsBetween(Convert.ToDecimal(destZip), zipbegin, zipend))
                {
                    strTZone = strTZone + "'" + Convert.ToString(row["zone_id"]) + "'" + ",";
                }
            }

            //remove the last extra comma at the end of string
            strFZone = strFZone.TrimEnd(',');
            strTZone = strTZone.TrimEnd(',');

            /*use this method to pass back any values you want to view using firebug.  make sure to not comment out the return Json at the end
            //begin test
            List<carrierRow> carriers = new List<carrierRow>();
            carrierRow carrier = new carrierRow()
            {
                pli_book_number = bookingNumber,
                role_id = "NA",
                carrier_id = "ABF",
                service_point = "",
                pro_number = "from:" + strFZone,
                carrier_charges = "",
                transit_days = "",
                invoiced_date = "",
                dispatched_date_time = "",
                notes = "to:" + strTZone,
                carrier_sequence = ""
            };
            carriers.Add(carrier);
            //end test
            */
            
            //determine if customer has carrier pricing exceptions
            DataTable dtCustomerRow = CreateDataTable("SELECT customer_id FROM carrier_pricing WHERE customer_id = " + customerID);
            if (dtCustomerRow.Rows.Count > 0)  //use customer carriers pricing
            {
                using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ToString()))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetCustomerCarriersByZones", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@strCustomerID", SqlDbType.NVarChar).Value = customerID;
                    cmd.Parameters.Add("@strFromZones", SqlDbType.NVarChar).Value = strFZone;
                    cmd.Parameters.Add("@strToZones", SqlDbType.NVarChar).Value = strTZone;
                    SqlDataReader reader = cmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        dtCarriers.Load(reader);
                        dtCount = dtCarriers.Rows.Count;
                    }

                    reader.Close();
                    cmd.Dispose();
                };
            }
            else  //use blanket carriers pricing
            {
                using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ToString()))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetCarriersByZones", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@strFromZones", SqlDbType.NVarChar).Value = strFZone;
                    cmd.Parameters.Add("@strToZones", SqlDbType.NVarChar).Value = strTZone;
                    SqlDataReader reader = cmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        dtCarriers.Load(reader);
                        dtCount = dtCarriers.Rows.Count;
                    }

                    reader.Close();
                    cmd.Dispose();
                };
            }
            
            //Build strZone in this format to pass into the GetCarriersByZones and GetCustomerCarriersByZones SPs
            //'zone00010','zone00140' (FromZone)
            //'zone00010','zone00050' (ToZone)
           
            List<carrierRow> carriers = new List<carrierRow>();

            for (int x = 0; x <= dtCount-1; x++)
            {
                carrierID = Convert.ToString(dtCarriers.Rows[x]["carrier_id"]);
                //get carrier transit days
                if (carrierID.Length > 0)
                {
                    transitDays = GetCarrierTransitDays(Convert.ToInt16(carrierID), orgZip, destZip);
                    if (transitDays != "N/A")
                    {
                        rwRate = Convert.ToString(CalculateCarrierRate(dataWeight, dataClass, orgZip, destZip, customerID, carrierID));
                        netRate = CalculateNetRate(Convert.ToDecimal(rwRate), Convert.ToDecimal(dtCarriers.Rows[x]["discount"]));

                        //determine if AMC applies.  Returns the larger of the two values
                        rate = Math.Max(Convert.ToDecimal(dtCarriers.Rows[x]["absolute_minimum"]), netRate);

                        //calculate the fsc charge
                        fsc = CalculateFSC(rate);

                        carrierCharges = Convert.ToString(rate + fsc);
                    }
                }
                else
                {
                    transitDays = "N/A";
                }

                //only add carrier with transit days to json object
                if (transitDays != "N/A")
                {
                    carrierRow carrier = new carrierRow()
                    {
                        pli_book_number = bookingNumber,
                        role_id = "NA",
                        carrier_id = Convert.ToString(dtCarriers.Rows[x]["carrier_id"]),
                        service_point = Convert.ToString(dtCarriers.Rows[x]["service_point"]),
                        pro_number = "",
                        carrier_charges = carrierCharges,
                        transit_days = transitDays,
                        invoiced_date = "",
                        dispatched_date_time = "",
                        notes = "",
                        carrier_sequence = Convert.ToString(x + 1)
                    };
                    carriers.Add(carrier);
                } 
            }
            
            var jsonData = new
            {
                rows = carriers
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        //method to run SQL statement and populate the datatable with the result
        public static DataTable CreateDataTable(string queryString)
        {
            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ConnectionString))
            {
                SqlDataAdapter dap = new SqlDataAdapter(queryString, conn);
                DataTable dt = new DataTable();
                dap.Fill(dt);
                return dt;
            }
        }

        //method to get profile data of the selected customer
        public customerObj GetCustomerData(int customerId)
        {
            //create a datatable to store the customer data
            DataTable dtCustomer = CreateDataTable("select b.CustomerId, b.CustomerName, c.ModuleName, REPLACE(CONVERT(varchar, c.PLIRateEffDate, 102),'.','') as PLIRateEffDate, c.IsActive, b.IsRateException from Customer b, SMCDataModule c where b.CustomerId ='" + customerId + "' and b.TariffModCodeId = c.CodeId and c.IsActive=1");
            if (dtCustomer.Rows.Count > 0)
            {
                customerObj customer = new customerObj();
                customer.CustomerName = Convert.ToString(dtCustomer.Rows[0]["CustomerName"]);
                customer.TariffName = Convert.ToString(dtCustomer.Rows[0]["ModuleName"]);
                customer.PLIRateEffective = Convert.ToString(dtCustomer.Rows[0]["PLIRateEffDate"]);
                customer.IsRateException = Convert.ToBoolean(dtCustomer.Rows[0]["IsRateException"]);
                return customer;
            }
            else
            {
                customerObj customer = new customerObj();
                return customer;
            }
        }

        //method to get the carrier scac
        public static string GetSCAC(string queryString)
        {
            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ConnectionString))
            {
                conn.Open();
                SqlCommand command = new SqlCommand(queryString, conn);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    return reader.GetString(0);
                }
            }
            return "";
        }

        //method to get the transit days of the selected carrier
        public ActionResult GetTransitDays(Int16 carrierId, string orgZip, string destZip)
        {
            var scac = GetSCAC("SELECT ISNULL(scac,'N/A') AS carrierSCAC FROM carrier WHERE carrier_id=" + carrierId);
            
            //begin CarrierConnect Code
            CarrierConnectWSPortTypeClient ccws = new CarrierConnectWSPortTypeClient();
            //point the client at the web service URL
            ccws.Endpoint.Address = new System.ServiceModel.EndpointAddress(@"http://applications.smc3.com/AdminManager/services/CarrierConnectWS");

            IntraX2.CarrierConnectService.AuthenticationToken authCCToken = new IntraX2.CarrierConnectService.AuthenticationToken();
            //get the authentication information out of the authentication.settings property file
            authCCToken.licenseKey = Properties.authentication.Default.licenseKeyCC;
            authCCToken.password = Properties.authentication.Default.password;
            authCCToken.username = Properties.authentication.Default.username;

            //is the web service ready?
            bool bCCReady = ccws.isReady(authCCToken);
            if (bCCReady != true)
            {
                ViewData["Errormsg"] = "Connect Carrier serivce is not accepting requests";
                return View("ErrorView1");
            }

            //build a transit request
            TransitRequest transitRequest = new TransitRequest();
            TransitResponse transitResponse = new TransitResponse();
            IntraX2.CarrierConnectService.ShipmentLocale originCC = new IntraX2.CarrierConnectService.ShipmentLocale();
            IntraX2.CarrierConnectService.ShipmentLocale destinationCC = new IntraX2.CarrierConnectService.ShipmentLocale();

            //get transit days
            originCC.postalCode = orgZip;
            originCC.countryCode = "USA";
            destinationCC.postalCode = destZip;
            destinationCC.countryCode = "USA";
            transitRequest.origin = originCC;
            transitRequest.destination = destinationCC;
            ScacRequest[] dtScacAry = new ScacRequest[1];
            ScacRequest rdScac = new ScacRequest();

            //assign the selected carrier scac to the request scac parameter
            rdScac.scac = scac;
            dtScacAry[0] = rdScac;
            transitRequest.scacs = dtScacAry;

            //call Transit web service method
            try
            {
                transitResponse = ccws.Transit(authCCToken, transitRequest);
                ScacResponse[] scacRespones = transitResponse.scacResponses;
                var transDays = Convert.ToString(scacRespones[0].days);
                //ViewData["serviceType"] = scacRespones[0].originServiceType;
                
                transitDays ccDays = new transitDays();
                ccDays.days = transDays;
                return Json(new { days = ccDays.days }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                var transDays = "N/A";
                //ViewData["serviceType"] = "I";
                
                transitDays ccDays = new transitDays();
                ccDays.days = transDays;
                return Json(new { days = ccDays.days }, JsonRequestBehavior.AllowGet);
            }
        }

        //get the nmfc commodity list
        public JsonResult GetCommodity()
        {
            //this works - call the SP directly
            //var list = db.pflin_GetNMFCMasterLookupData2(null, null).ToList();
            //return Json(list, JsonRequestBehavior.AllowGet);
            
            //get the result from a method of the class
            //this method utilizes caching
            var CommodityListResult = commodityList.GetAllCommodities();
            var CommoditySelectList = new SelectList(CommodityListResult, "Value", "Description");
            return Json(CommoditySelectList.ToList(), JsonRequestBehavior.AllowGet);
        }

        //get the nmfc commodity list for popup dialog
        public JsonResult GetDialogCommodity(int shipperId)
        {
            //call the SP directly
            var list = db.GetNMFCLookupData(shipperId).ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        //get the package types list
        public JsonResult GetPackageTypes()
        {
            var PackageListResult = packageList.GetPackageTypes();
            var PackageSelectList = new SelectList(PackageListResult, "package_id", "package_description");
            return Json(PackageSelectList.ToList(), JsonRequestBehavior.AllowGet);
        }

        //get the accessorial list
        public JsonResult GetAccessorialList()
        {
            //get the result from a method of the class
            //this method utilizes caching
            var AccessorialListResult = accessorialList.GetAccessorialServices();
            var AccessorialSelectList = new SelectList(AccessorialListResult, "AccChargeId", "AccType");
            return Json(AccessorialSelectList.ToList(), JsonRequestBehavior.AllowGet);
        }

        //get the shipper list
        public JsonResult GetShipperList()
        {
            //get the result from a method of the class
            //this method utilizes caching
            var ShipperListResult = shipperList.GetAllShippers();
            var ShipperSelectList = new SelectList(ShipperListResult, "shipper_id", "name");
            return Json(ShipperSelectList.ToList(), JsonRequestBehavior.AllowGet);
        }

        //get the carrier list
        public JsonResult GetCarrierList()
        {
            //get the result from a method of the class
            //this method utilizes caching
            var CarrierListResult = carrierList.GetAllCarriers();
            var CarrierSelectList = new SelectList(CarrierListResult, "CarrierID", "CarrierName");
            return Json(CarrierSelectList.ToList(), JsonRequestBehavior.AllowGet);
        }

        // GET: /Shipment/TestCommodity
        public ActionResult TestCommodity()
        {
            //var CommodityListResult = commodityList.GetAllCommodities();
            //var CommoditySelectList = new SelectList(CommodityListResult, "Value", "Description");
            //ViewBag.CommodityList = CommoditySelectList;

            var PackageListResult = packageList.GetPackageTypes();
            var PackageSelectList = new SelectList(PackageListResult, "package_id", "package_description");
            ViewBag.PackageList = PackageSelectList;
            
            return View();
        }

        // GET: /Shipment/TestSearch
        public ActionResult TestSearch()
        {
            return View();
        }
        
        // GET: /Shipment/SearchBooking
        public ActionResult SearchBooking()
        {
            return View();
        }

        // POST: /Shipment/SearchBooking
        [HttpPost]
        public ActionResult SearchBooking(string booking, string btnSearch, string btnCancel)
        {
            ViewData["booking"] = booking;
            var button = btnSearch ?? btnCancel;
            if (button == "search")
            {
                using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ToString()))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("SearchShipment", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@BookingValue ", SqlDbType.NVarChar).Value = booking;
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        ViewData["result"] = "Edit booking number: " + booking;
                    }
                    else
                    {
                        ViewData["result"] = "Booking number not found.";
                    }
                    reader.Close();
                    cmd.Dispose();
                }
            }
            if (button == "cancel")
            {
                return RedirectToAction("Index","Home");
            }
            return View("Edit");
        }

        /*
        // GET: /Shipment/CreateBooking
        public ActionResult CreateBooking()
        {
            var CommodityListResult = commodityList.GetAllCommodities();
            var CommoditySelectList = new SelectList(CommodityListResult, "Value", "Description");
            ViewBag.CommodityList = CommoditySelectList;

            var PackageListResult = packageList.GetPackageTypes();
            var PackageSelectList = new SelectList(PackageListResult, "package_id", "package_description");
            ViewBag.PackageList = PackageSelectList;

            return View(new IntraX2.Models.shipmentModel());
        }

        [HttpPost]
        public ActionResult CreateBooking(shipmentModel sm, FormCollection form)
        {
            //int itemCount = 2;
            string itemPieces = Convert.ToString(sm.m_shipment_item.pieces);
            ViewData["itemPieces"] = itemPieces;
            //ViewData["itemPieces"] = "1";

            //string[] arrayPieces = form["Pieces"].Split(',');

            return View("SavedView");
        }
        */
      
        // GET: /Shipment/Create
        public ActionResult Create()
        { 
            var ModeListResult = modeList.GetShipmentModes();
            var ModeSelectList = new SelectList(ModeListResult, "mode_id", "mode_description");
            ViewBag.ModeList = ModeSelectList;

            var CustomerListResult = customerList.GetAllCustomers();
            var CustomerSelectList = new SelectList(CustomerListResult, "CustomerId", "CustomerName");
            ViewBag.CustomerList = CustomerSelectList;

            //ViewData["CustList"] = new SelectList(CustomerSelectList.Select(x => x.Value == "21813"));

            var EquipmentListResult = equipmentList.GetEquipmentTypes();
            var EquipmentSelectList = new SelectList(EquipmentListResult, "equipment_id", "equipment_description");
            ViewBag.EquipmentList = EquipmentSelectList;

            var CustomerContactRoleListResult = customerContactRoleList.GetCustomerContactRoleList();
            var CustomerContactRoleSelectList = new SelectList(CustomerContactRoleListResult, "CodeId", "Description");
            ViewBag.CustomerContactRoleList = CustomerContactRoleSelectList;

            var StateListResult = statesList.GetStates();
            var StateSelectList = new SelectList(StateListResult, "StateCode", "StateName");
            ViewBag.StatesList = StateSelectList;

            var CountryListResult = countryCodeList.GetCountries();
            var CountrySelectList = new SelectList(CountryListResult, "CountryCode", "CountryName");
            ViewBag.CountryList = CountrySelectList;

            /*
            var AccessorialListResult = accessorialList.GetAccessorialServices();
            var AccessorialSelectList = new SelectList(AccessorialListResult, "AccChargeId", "AccType");
            ViewBag.AccessorialList = AccessorialSelectList;
            */ 

            //default select list value to BOOKED
            ViewBag.StatusList = new SelectList(StatusList, "BOOKED");

            //default select list value to Customer
            ViewBag.BillToList = new SelectList(BillToList, "CUSTOMER");   
                          
            /*
            var PackageListResult = packageList.GetPackageTypes();
            var PackageSelectList = new SelectList(PackageListResult, "package_id", "package_description");
            ViewBag.PackageList = PackageSelectList;

            var CustomerListResult = customerList.GetAllCustomers();
            var CustomerSelectList = new SelectList(CustomerListResult, "CustomerId", "CustomerName");
            ViewBag.CustomerList = CustomerSelectList;

            var ShipperListResult = shipperList.GetAllShippers();
            var ShipperSelectList = new SelectList(ShipperListResult, "shipper_id", "name");
            ViewBag.ShipperList = ShipperSelectList;

            var ConsigneeListResult = consigneeList.GetAllConsignees();
            var ConsigneeSelectList = new SelectList(ConsigneeListResult, "consignee_id", "name");
            ViewBag.ConsigneeList = ConsigneeSelectList;

            var BillToListResult = billtoList.GetBillToParty();
            var BillToSelectList = new SelectList(BillToListResult, "id", "description");
            ViewBag.BillToList = BillToSelectList;

            var CommodityListResult = commodityList.GetAllCommodities();
            var CommoditySelectList = new SelectList(CommodityListResult, "Value", "Description");
            ViewBag.CommodityList = CommoditySelectList;
           
            var EquipmentListResult = equipmentList.GetEquipmentTypes();
            var EquipmentSelectList = new SelectList(EquipmentListResult, "equipment_id", "equipment_description");
            ViewBag.EquipmentList = EquipmentSelectList;

            var AccessorialListResult = accessorialList.GetAccessorialServices();
            var AccessorialSelectList = new SelectList(AccessorialListResult, "AccChargeId", "AccType");
            ViewBag.AccessorialList = AccessorialSelectList;

            var RoleListResult = roleList.GetRoles();
            var RoleSelectList = new SelectList(RoleListResult, "carrier_role_id", "carrier_role_description");
            ViewBag.RoleList = RoleSelectList;

            var CarrierListResult = carrierList.GetAllCarriers();
            var CarrierSelectList = new SelectList(CarrierListResult, "CarrierID", "CarrierName");
            ViewBag.CarrierList = CarrierSelectList;
            */

            //Get the next booking number
            //System.Data.Entity.Core.Objects.ObjectParameter RetSeqNum = new System.Data.Entity.Core.Objects.ObjectParameter("intSeqNum", typeof(int));  //variable intSeqNum is defined in SP as OUTPUT
            System.Data.Objects.ObjectParameter RetSeqNum = new System.Data.Objects.ObjectParameter("intSeqNum", typeof(int)); //variable intSeqNum is defined in SP as OUTPUT
            db.GetSequence(RetSeqNum);
            ViewData["newSeq"] = Convert.ToString(RetSeqNum.Value);

            string format = "M/d/yyyy";
            ViewData["bookedDate"] = DateTime.Now.ToString(format);

            ViewData["statusDate"] = DateTime.Now.ToString("G");

            var userCookie = Request.Cookies["userIDCookie"];
            if (userCookie != null)
                ViewData["bookedBy"] = userCookie.Value;
                
            else
                ViewData["bookedBy"] = "PLI";
            
            //ViewData["pliRated"] = false;

            //create and assign an action to the cookie
            HttpCookie actionCookie = new HttpCookie("actionCookie");
            actionCookie.Value = "create_booking";
            Response.Cookies.Add(actionCookie);

            //return View(new IntraX2.Models.shipmentModel());
            return View(new IntraX2.Models.shipment());
        }

        [HttpPost]
        public ActionResult Create(shipment objShipment)
        {
            if (ModelState.IsValid)
            {
                db.shipments.Add(objShipment);
                db.SaveChanges();
                //return RedirectToAction("Shipments", "Shipment");
                return RedirectToAction("Edit", new { booking = objShipment.pli_book_number, rowAction = "create"});  //this will keep the booking in display and edit mode
            }
            else
            {
                ViewData["ErrorMsg"] = "Alert: Data model error encountered";
                return View("ErrorView1");
            }
            //return RedirectToAction("Edit", new { booking = objShipment.pli_book_number, rowAction = "create" });
        }

        // GET: /Shipment/Edit
        public ActionResult Edit(string booking, string rowAction)
        {
            shipment shipment = db.shipments.Find(booking);
            if (shipment == null)
            {
                ViewData["MessageText"] = "The booking number you entered was not found.";
                return View("MessageView");
            }
            
            var ModeListResult = modeList.GetShipmentModes();
            var ModeSelectList = new SelectList(ModeListResult, "mode_id", "mode_description");
            ViewBag.ModeList = ModeSelectList;

            var EquipmentListResult = equipmentList.GetEquipmentTypes();
            var EquipmentSelectList = new SelectList(EquipmentListResult, "equipment_id", "equipment_description");
            ViewBag.EquipmentList = EquipmentSelectList;

            var CustomerListResult = customerList.GetAllCustomers();
            var CustomerSelectList = new SelectList(CustomerListResult, "CustomerId", "CustomerName");
            ViewBag.CustomerList = CustomerSelectList;

            //default select list value to PU SCHEDULED
            ViewBag.StatusList = new SelectList(StatusList);
            ViewData["Booking"] = booking;

            //create and assign an action to the cookie
            HttpCookie actionCookie = new HttpCookie("actionCookie");
            actionCookie.Value = "edit_booking";
            Response.Cookies.Add(actionCookie);

            //create a cookie to store the view type
            /*
            HttpCookie viewTypeCookie = new HttpCookie("viewTypeCookie");
            viewTypeCookie.Value = "editshipment";
            viewTypeCookie.Expires = DateTime.Now.AddDays(1);
            Response.Cookies.Add(viewTypeCookie);
            */

            ViewData["RowAction"] = rowAction;
            return View(shipment);
        }

        // POST: /Shipment/Edit
        [HttpPost]
        public ActionResult Edit(shipment objShipment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(objShipment).State = EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Shipments", "Shipment");
            //return RedirectToAction("Edit", new { booking = objShipment.pli_book_number, rowAction = "edit" });  //this will keep the booking in display and edit mode
        }

        //soft delete a shipment row
        [HttpPost]
        public string DeleteShipmentRow(string Booking)
        {
            string msg = "";
            shipment shipmentObj = db.shipments.Find(Booking);
            if (shipmentObj != null)
            {
                shipmentObj.status = "DELETE";
            }

            try
            {
                if (ModelState.IsValid)
                {
                    db.SaveChanges();
                    msg = "Delete successful";
                }
                else
                {
                    msg = "Validation data not successful";
                }
            }
            catch (Exception ex)
            {
                msg = "Error occured:" + ex.Message;
            }

            return msg;
        }

        //get the invoice id of the booking number
        public string GetInvoiceID(string booking)
        {
            var list = db.pflin_Shpmt_GetInvoiceID(booking).FirstOrDefault();
            return Convert.ToString(list);
        }

        // GET: /Shipment/
        public ActionResult Index()
        {
            return View();
        }

        // GET: /ShipmentCarrierBills/
        public ActionResult ShipmentCarrierBills()
        {
            return View();
        }

        // POST: /ShipmentCarrierBills/
        [HttpPost]
        public ActionResult ShipmentCarrierBills(FormCollection fc)
        {
            return View();
        }

        // GET: /ShipmentInvoices/
        public ActionResult ShipmentInvoices()
        {
            return View();
        }

        // POST: /ShipmentsInvoice/
        [HttpPost]
        public ActionResult ShipmentsInvoice(FormCollection fc)
        {
            return View();
        }
        
        // GET: /Shipments/
        public ActionResult Shipments()
        {
            ViewBag.StatusList = new SelectList(StatusList);

            var CustomerListResult = customerList.GetAllCustomers();
            var CustomerSelectList = new SelectList(CustomerListResult, "CustomerId", "CustomerName");
            ViewBag.CustomerList = CustomerSelectList;

            var ShipperListResult = shipperList.GetAllShippers();
            var ShipperSelectList = new SelectList(ShipperListResult, "shipper_id", "name");
            ViewBag.ShipperList = ShipperSelectList;

            var ConsigneeListResult = consigneeList.GetAllConsignees();
            var ConsigneeSelectList = new SelectList(ConsigneeListResult, "consignee_id", "name");
            ViewBag.ConsigneeList = ConsigneeSelectList;

            var ModeListResult = modeList.GetShipmentModes();
            var ModeSelectList = new SelectList(ModeListResult, "mode_id", "mode_description");
            ViewBag.ModeList = ModeSelectList;

            var CarrierListResult = carrierList.GetAllCarriers();
            var CarrierSelectList = new SelectList(CarrierListResult, "CarrierID", "CarrierName");
            ViewBag.CarrierList = CarrierSelectList;

            var SalesRepListResult = salesRepList.GetAllSalesReps();
            var SalesRepSelectList = new SelectList(SalesRepListResult, "SalesRepID", "SalesRepAlias");
            ViewBag.SalesRepList = SalesRepSelectList;

            return View();
        }

        //get the nmfc detail
        public JsonResult GetNMFCItemDetail(int value)
        {
            var list = db.GetNMFCItemDetail(value);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        //get the shipper nmfc
        public JsonResult GetShipperNMFCData(int shipperId)
        {
            var list = db.GetShipperNMFCData(shipperId).ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        
        //get the shipper location zip
        public string GetShipperLocationZip(int shipperId, int locationId)
        {
            var list = db.GetShipperLocationZip(shipperId, locationId).FirstOrDefault();
            return list;
        }
        
        //get the shipper location name
        public string GetShipperLocationName(int shipperId, int locationId)
        {
            var list = db.GetShipperLocationName(shipperId, locationId).FirstOrDefault();
            return list;
        }

        //get the shipper location contact name
        public string GetShipperLocationContactName(int shipperId, int locationId, int contactId)
        {
            var list = db.GetShipperLocationContactName(shipperId, locationId, contactId).FirstOrDefault();
            return list;
        }

        //get selected customer's sales rep
        public string GetCustomerSalesRep(int customerId)
        {
            var list = db.GetCustomerSalesRep(customerId).FirstOrDefault();
            return Convert.ToString(list);
        }

        //get single carrier of the shipment
        public string GetShipmentCarrier(string booking)
        {
            var list = db.GetShipmentCarrier(booking).FirstOrDefault();
            return Convert.ToString(list);
        }

        //get multiple carriers of the shipment
        public JsonResult GetShipmentCarriers(string booking)
        {
            var list = db.GetShipmentCarrier(booking).FirstOrDefault();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        //get customer contacts of the selected customer
        public JsonResult GetCustomerContacts(int customerId)
        {
            var list = db.GetCustomerContacts(customerId).ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        //get locations of the selected shipper
        public JsonResult GetShipperLocations(int shipperId)
        {
            var list = db.GetShipperLocations(shipperId).ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        //get state and zip of the selected shipper location
        public JsonResult GetShipperLocationStateZip(int shipperId, int locationId)
        {
            var list = db.GetShipperStateZip(shipperId, locationId).ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        //get contacts of the select shipper location
        public JsonResult GetShipperLocationContactList(int shipperId, int locationId)
        {
            var list = db.GetShipperLocationContacts(shipperId, locationId).ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        //get locations of the selected consignee
        public JsonResult GetConsigneeLocations(int consigneeId)
        {
            var list = db.GetConsigneeLocations(consigneeId).ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        //get state and zip of the selected consignee location
        public JsonResult GetConsigneeLocationStateZip(int consigneeId, int locationId)
        {
            var list = db.GetConsigneeStateZip(consigneeId, locationId).ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        //get contacts of the select consignee location
        public JsonResult GetConsigneeLocationContactList(int consigneeId, int locationId)
        {
            var list = db.GetConsigneeLocationContacts(consigneeId, locationId).ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        //get tariff exception of the selected customer
        public JsonResult GetCustomerTariffException(int customerId)
        {
            var list = db.GetCustomerTariffException(customerId).ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        //get locations of the selected carrier
        public JsonResult GetCarrierLocations(int carrierId)
        {
            var list = db.GetCarrierLocations(carrierId).ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        //get contacts of the select carrier location
        public JsonResult GetCarrierLocationContactList(int carrierId, int locationId)
        {
            var list = db.GetCarrierLocationContacts(carrierId, locationId).ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        //get the shipment multiple shippers
        public JsonResult GetShipmentMultipleShippers(string bookingNbr)
        {
            var list = db.GetShipmentMultipleShippers(bookingNbr).ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        //get the shipment multiple carriers
        public JsonResult GetShipmentMultipleCarriers(string bookingNbr)
        {
            var list = db.GetShipmentMultipleCarriers(bookingNbr).ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        //get the shipment multiple consignees
        public JsonResult GetShipmentMultipleConsignees(string bookingNbr)
        {
            var list = db.GetShipmentMultipleConsignees(bookingNbr).ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        //get the shipment status details
        public JsonResult GetShipmentStatusDetails(string booking)
        {
            var list = db.GetShipmentStatusDetail(booking).ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }
    }
}
