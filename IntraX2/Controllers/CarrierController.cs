﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Data;
using System.Data.Entity;
using IntraX2.Models;

namespace IntraX2.Controllers
{
    public class CarrierController : BaseController
    {
        //instantiate pathfinderEntities data model entity container, defined in the Models.
        private pathfinderEntities db = new pathfinderEntities();

        //set database connection
        SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ConnectionString);
        
        //
        // GET: /Carrier/
        public ActionResult Index()
        {
            var StateListResult = statesList.GetStates();
            var StateSelectList = new SelectList(StateListResult, "StateCode", "StateName");
            ViewBag.StatesList = StateSelectList;

            var TariffModeListResult = tariffModeList.GetModeList();
            var TariffModeSelectList = new SelectList(TariffModeListResult, "CodeId", "Description");
            ViewBag.TariffModeList = TariffModeSelectList;

            return View(new IntraX2.Models.carrierModel());
        }

        // POST: /Carrier/Index
        [HttpPost]
        public ActionResult Index(carrierModel cm, FormCollection fc)
        { 
            var w9onfile = Convert.ToString(fc["w9onfile"]);
            var cargoonfile = Convert.ToString(fc["cargo_onfile"]);
            var cargoshowspli = Convert.ToString(fc["cargo_showspli"]);

            var msg = "";
            try
            {
                if (Convert.ToString(fc["carrierOprFlag"]) == "add")
                {
                    db.Entry(cm.m_carrier).State = EntityState.Added;
                    db.SaveChanges();
                    //update non-binding fields
                    UpdatePartialCarrier(Convert.ToInt32(cm.m_carrier.carrier_id), w9onfile, cargoonfile, cargoshowspli);
                }
                else
                {
                    db.Entry(cm.m_carrier).State = EntityState.Modified;
                    db.SaveChanges();
                    //update non-binding fields
                    UpdatePartialCarrier(Convert.ToInt32(cm.m_carrier.carrier_id), w9onfile, cargoonfile, cargoshowspli);
                }
                return RedirectToAction("Index", "Carrier");
            }
            catch (Exception ex)
            {
                msg = "Error occured:" + ex.Message;
                ViewData["ErrorMsg"] = msg;
                return View("ErrorView1");
            }       
            
            /*
            if (ModelState.IsValid)
            {
                if (Convert.ToString(fc["newCarrierFlag"]) == "Y")
                {
                    db.Entry(cm.m_carrier).State = EntityState.Added;
                    db.SaveChanges();
                    //update non-binding fields
                    UpdatePartialCarrier(Convert.ToInt32(cm.m_carrier.carrier_id), w9onfile, cargoonfile, cargoshowspli);
                }
                else
                {
                    db.Entry(cm.m_carrier).State = EntityState.Modified;
                    db.SaveChanges();
                    //update non-binding fields
                    UpdatePartialCarrier(Convert.ToInt32(cm.m_carrier.carrier_id), w9onfile, cargoonfile, cargoshowspli);
                }
            }
            */ 
            
            //ViewData["ErrorMsg"] = fc["locationId"];
            //return View("ErrorView1");       
            
            //return RedirectToAction("Index", "Carrier");
            //return RedirectToAction("Edit", new { booking = objShipment.pli_book_number, rowAction = "edit" });  //this will keep the booking in display and edit mode
        }

        [HttpPost]
        public string UpdatePartialCarrier(Int32 carrierId, string w9onfile, string cargoonfile, string cargoshowspli)
        {
            string msg;
            carrier carrierObj = db.carriers.Find(carrierId);
            if (carrierObj != null) //record found
            {
                if (w9onfile == "false")
                {
                    carrierObj.w9_onfile = null;
                }
                else
                {
                    carrierObj.w9_onfile = "Y";
                }

                if (cargoonfile == "false")
                {
                    carrierObj.cargo_ins_onfile = null;
                }
                else
                {
                    carrierObj.cargo_ins_onfile = "Y";
                }

                if (cargoshowspli == "false")
                {
                    carrierObj.cargo_ins_showspli = null;
                }
                else
                {
                    carrierObj.cargo_ins_showspli = "Y";
                }

                db.SaveChanges();
            }
            msg = "Row saved successfully";
            return msg;
        }

        [HttpPost]
        public string UpdateLocation(decimal carrierId, int locationId, string location, string address1, string address2, string city, string state, string zip)
        {
            string msg;
            carrier_locations clObj = db.carrier_locations.Find(carrierId, locationId);
            if (clObj != null) //record found
            {
                clObj.name = location;
                clObj.address_1 = address1;
                clObj.address_2 = address2;
                clObj.city = city;
                clObj.state = state;
                clObj.zip = zip;

                db.SaveChanges();
                msg = "Row saved successfully";
            }
            else //add new record
            {
                carrier_locations newRow = new carrier_locations();
                newRow.carrier_id = carrierId;
                newRow.seq_number = locationId;
                newRow.name = location;
                newRow.address_1 = address1;
                newRow.address_2 = address2;
                newRow.city = city;
                newRow.state = state;
                newRow.zip = zip;

                /*insert row to database*/
                try
                {
                    if (ModelState.IsValid)
                    {
                        db.carrier_locations.Add(newRow);
                        db.SaveChanges();
                        msg = "Row added successfully";
                    }
                    else
                    {
                        msg = "Validation data not successfull";
                    }
                }
                catch (Exception ex)
                {
                    msg = "Error occured:" + ex.Message;
                }
            }
            return msg;
        }

        //soft delete a carrier
        [HttpPost]
        public string DeleteCarrier(decimal carrierId)
        {
            string msg = "";

            try
            {
                if (ModelState.IsValid)
                {
                    //execute the stored procedure to soft delete the carrier and it's associated loctions and contacts
                    db.DeleteCarrier(carrierId);

                    msg = "Row deleted successfully";
                }
                else
                {
                    msg = "Validation data not successfull";
                }
            }
            catch (Exception ex)
            {
                msg = "Error occured:" + ex.Message;
            }
            
            return msg;
        }

        //soft delete a location
        [HttpPost]
        public string DeleteLocation(int carrierId, int locationId)
        {
            string msg = "";
            carrier_locations clObj = db.carrier_locations.Find(carrierId, locationId);
            if (clObj != null)
            {
                clObj.record_status = "0";
            }

            try
            {
                if (ModelState.IsValid)
                {
                    db.SaveChanges();
                    msg = "Delete successful";
                }
                else
                {
                    msg = "Validation data not successful";
                }
            }
            catch (Exception ex)
            {
                msg = "Error occured:" + ex.Message;
            }

            return msg;
        }

        //soft delete a contact
        [HttpPost]
        public string DeleteContact(int carrierId, int locationId, int contactId)
        {
            string msg = "";
            carrier_location_contacts clcObj = db.carrier_location_contacts.Find(carrierId, locationId, contactId);
            if (clcObj != null)
            {
                clcObj.record_status = "0";
            }

            try
            {
                if (ModelState.IsValid)
                {
                    db.SaveChanges();
                    msg = "Delete successful";
                }
                else
                {
                    msg = "Validation data not successful";
                }
            }
            catch (Exception ex)
            {
                msg = "Error occured:" + ex.Message;
            }

            return msg;
        }

        [HttpPost]
        public string UpdateContact(decimal carrierId, int locationId, int contactId, string phone, string ext, string fax, string email, string name)
        {
            string msg;
            carrier_location_contacts clcObj = db.carrier_location_contacts.Find(carrierId, locationId, contactId);
            if (clcObj != null) //record found
            {
                clcObj.phone = phone;
                clcObj.extension = ext;
                clcObj.fax = fax;
                clcObj.email = email;
                clcObj.name = name;
                msg = "Row saved successfully";
            }
            else //add new record
            {
                carrier_location_contacts newRow = new carrier_location_contacts();
                newRow.carrier_id = carrierId;
                newRow.seq_number = locationId;
                newRow.contact_id = contactId;
                newRow.name = name;
                newRow.phone = phone;
                newRow.extension = ext;
                newRow.fax = fax;
                newRow.email = email;

                /*insert row to database*/
                try
                {
                    if (ModelState.IsValid)
                    {
                        db.carrier_location_contacts.Add(newRow);
                        msg = "Row added successfully";
                    }
                    else
                    {
                        msg = "Validation data not successfull";
                    }
                }
                catch (Exception ex)
                {
                    msg = "Error occured:" + ex.Message;
                }
            }

            db.SaveChanges();          
            return msg;
        }      

        public ActionResult GetCarrierDetail(Int32 carrierId)
        {
            DataTable dtCarrier = new DataTable();
            var scac = "";
            var setupBy = "";
            var setupDate = "";
            var name = "";
            var mcNumber = "";
            var usDOT = "";
            var taxID = "";
            var w9onFile = "";
            var agentName = "";
            var agentPhone = "";
            var cargoAmount = "";
            var cargoExp = "";
            var cargoInsOnFile = "";
            var cargoInsShowsPLI = "";
            var liabilityAmount = "";
            var liabilityExp = "";
            Boolean IsRateException = false;
            var strIsRateException = "";
            var TariffModule = "";
            var Discount = "";
            var MinCharge = "";
            Boolean IsAutoRateSuppressed = false;
            var strIsAutoRateSuppressed = "";
            var ExceptionNotes = "";
            var EffectiveDate = "";
           
            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ToString()))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("GetCarrierDetail", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@intCarrierID", SqlDbType.Int).Value = carrierId;
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    dtCarrier.Load(reader);
                    scac = Convert.ToString(dtCarrier.Rows[0]["scac"]);
                    setupDate = Convert.ToString(dtCarrier.Rows[0]["setupDate"]);
                    setupBy = Convert.ToString(dtCarrier.Rows[0]["setup_by"]);     
                    name = Convert.ToString(dtCarrier.Rows[0]["name"]);
                    mcNumber = Convert.ToString(dtCarrier.Rows[0]["mc_number"]);
                    usDOT = Convert.ToString(dtCarrier.Rows[0]["us_dot_number"]);
                    taxID = Convert.ToString(dtCarrier.Rows[0]["tax_id"]);               
                    w9onFile = Convert.ToString(dtCarrier.Rows[0]["w9_onfile"]);         
                    agentName = Convert.ToString(dtCarrier.Rows[0]["agent_name"]);
                    agentPhone = Convert.ToString(dtCarrier.Rows[0]["agent_phone"]);
                    cargoAmount = Convert.ToString(dtCarrier.Rows[0]["cargo_ins_amount"]);  
                    cargoExp = Convert.ToString(dtCarrier.Rows[0]["cargoInsExpDate"]);
                    cargoInsOnFile = Convert.ToString(dtCarrier.Rows[0]["cargo_ins_onfile"]);
                    cargoInsShowsPLI = Convert.ToString(dtCarrier.Rows[0]["cargo_ins_showspli"]);    
                    liabilityAmount = Convert.ToString(dtCarrier.Rows[0]["liability_ins_amount"]);
                    liabilityExp = Convert.ToString(dtCarrier.Rows[0]["liabilityInsExpDate"]);

                    strIsRateException = Convert.ToString(dtCarrier.Rows[0]["IsRateException"]);
                    if (strIsRateException == "")
                    {
                        IsRateException = false;
                    }
                    else
                    {
                        IsRateException = Convert.ToBoolean(dtCarrier.Rows[0]["IsRateException"]);
                    }
                              
                    TariffModule = Convert.ToString(dtCarrier.Rows[0]["TariffModCodeId"]);
                    Discount = Convert.ToString(dtCarrier.Rows[0]["Discount"]);
                    MinCharge = Convert.ToString(dtCarrier.Rows[0]["MinCharge"]);

                    strIsAutoRateSuppressed = Convert.ToString(dtCarrier.Rows[0]["IsAutoRateSuppressed"]);
                    if (strIsAutoRateSuppressed == "")
                    {
                        IsAutoRateSuppressed = false;
                    }
                    else
                    {
                        IsAutoRateSuppressed = Convert.ToBoolean(dtCarrier.Rows[0]["IsAutoRateSuppressed"]);
                    }
                 
                    ExceptionNotes = Convert.ToString(dtCarrier.Rows[0]["RateExceptionNotes"]);
                    EffectiveDate = Convert.ToString(dtCarrier.Rows[0]["EffectiveDate"]);                   
                }
                reader.Close();
                cmd.Dispose();
            }; //eo using

            var jsonData = new
            {
                carrierID = Convert.ToString(carrierId),
                scac = scac,
                setupDate = setupDate,
                setupBy = setupBy,
                name = name,
                mcNumber = mcNumber,
                usDOT = usDOT,
                taxID = taxID,
                w9onFile = w9onFile,
                agentName = agentName,
                agentPhone = agentPhone,
                cargoAmount = cargoAmount,
                cargoExp = cargoExp,
                cargoInsOnFile = cargoInsOnFile,
                cargoInsShowsPLI = cargoInsShowsPLI,
                liabilityAmount = liabilityAmount,
                liabilityExp = liabilityExp,
                IsRateException = IsRateException,
                TariffModule = TariffModule,
                Discount = Discount,
                MinCharge = MinCharge,
                IsAutoRateSuppressed = IsAutoRateSuppressed,
                ExceptionNotes = ExceptionNotes,
                EffectiveDate = EffectiveDate
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        } //eo GetCarrierDetail

        public ActionResult GetCarrierLocationDetail(Int32 carrierId, Int16 locationId)
        {
            DataTable dtLocation = new DataTable();
            var name = "";
            var address1 = "";
            var address2 = "";
            var city = "";
            var state = "";
            var zip = "";
            
            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ToString()))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("GetCarrierLocationDetail", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@intCarrierID", SqlDbType.Int).Value = carrierId;
                cmd.Parameters.Add("@intLocationID", SqlDbType.Int).Value = locationId;
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    dtLocation.Load(reader);
                    name = Convert.ToString(dtLocation.Rows[0]["name"]);
                    address1 = Convert.ToString(dtLocation.Rows[0]["address_1"]);
                    address2 = Convert.ToString(dtLocation.Rows[0]["address_2"]);
                    city = Convert.ToString(dtLocation.Rows[0]["city"]);
                    state = Convert.ToString(dtLocation.Rows[0]["state"]);
                    zip = Convert.ToString(dtLocation.Rows[0]["zip"]);
                }
                reader.Close();
                cmd.Dispose();
            }; //eo using

            var jsonData = new
            {
                name = name,
                address1 = address1,
                address2 = address2,
                city = city,
                state = state,
                zip = zip
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        } //eo GetCarrierLocationDetail

        public ActionResult GetCarrierLocationContactDetail(Int32 carrierId, Int16 locationId, Int16 contactId)
        {
            DataTable dtContact = new DataTable();
            var phone = "";
            var extension = "";
            var fax = "";
            var email = "";

            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ToString()))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("GetCarrierLocationContactDetail", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@intCarrierID", SqlDbType.Int).Value = carrierId;
                cmd.Parameters.Add("@intLocationID", SqlDbType.Int).Value = locationId;
                cmd.Parameters.Add("@intContactID", SqlDbType.Int).Value = contactId;
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    dtContact.Load(reader);
                    phone = Convert.ToString(dtContact.Rows[0]["phone"]);
                    extension = Convert.ToString(dtContact.Rows[0]["extension"]);
                    fax = Convert.ToString(dtContact.Rows[0]["fax"]);
                    email = Convert.ToString(dtContact.Rows[0]["email"]);
                }
                reader.Close();
                cmd.Dispose();
            }; //eo using

            var jsonData = new
            {
                phone = phone,
                extension = extension,
                fax = fax,
                email = email
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        } //eo GetCarrierLocationContactDetail

        public ActionResult GetMaxCarrierLocationSeq(int carrierId)
        {
            DataTable dtSeq = new DataTable();
            var maxval = "";
            
            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ToString()))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("GetMaxCarrierLocationSeq", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@intCarrierID", SqlDbType.Int).Value = carrierId;
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    dtSeq.Load(reader);
                    maxval = Convert.ToString(dtSeq.Rows[0]["sequence"]);
                }
                
                reader.Close();
                cmd.Dispose();            
            }; //eo using

            var jsonData = new
            {
                maxval = Convert.ToInt16(maxval) + 1
            };   
            
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        } //eo GetMaxCarrierLocationSeq

        public ActionResult GetMaxCarrierContactSeq(int carrierId, int locationId)
        {
            var nextSeqNum = "";

            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ToString()))
            {
                System.Data.Objects.ObjectParameter RetNextSeqNum = new System.Data.Objects.ObjectParameter("intNextVal", typeof(int)); //variable intNextVal is defined in SP as OUTPUT
                db.GetMaxCarrierContactSeq(carrierId, locationId, RetNextSeqNum);
                nextSeqNum = Convert.ToString(RetNextSeqNum.Value);
            }; //eo using

            var jsonData = new
            {
                nextSeqNum = nextSeqNum
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
                      
            /*
            DataTable dtSeq = new DataTable();
            var maxval = "";

            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ToString()))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("GetMaxCarrierContactSeq", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@intCarrierID", SqlDbType.Int).Value = carrierId;
                cmd.Parameters.Add("@intLocationID", SqlDbType.Int).Value = locationId;
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    dtSeq.Load(reader);
                    maxval = Convert.ToString(dtSeq.Rows[0]["sequence"]);
                }

                reader.Close();
                cmd.Dispose();
            }; //eo using

            var jsonData = new
            {
                maxval = Convert.ToInt16(maxval) + 1
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
            */
        } //eo GetMaxCarrierLocationSeq

        public ActionResult GetMaxCarrierSeq()
        {
            //Get the next carrier id sequence number
            System.Data.Objects.ObjectParameter RetSeqNum = new System.Data.Objects.ObjectParameter("intSeqNum", typeof(int)); //variable intSeqNum is defined in SP as OUTPUT
            db.GetCarrierID(RetSeqNum);

            var jsonData = new
            {
                maxval = Convert.ToInt32(RetSeqNum.Value) + 1
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        } //eo GetMaxCarrierSeq

        //get locations of the selected carrier
        public JsonResult GetCarrierAllLocations(int carrierId)
        {
            var list = db.GetCarrierAllLocations(carrierId).ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        //get contacts of the select carrier location
        public JsonResult GetCarrierLocationContacts(int carrierId, int locationId)
        {
            var list = db.GetCarrierLocationContacts(carrierId, locationId).ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        //get carrier list
        public JsonResult GetCarrierList()
        {
            var list = db.basp_GetCarrierListData().ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Exceptions()
        {
            return View();
        }

        //get the carrier exceptions
        public JsonResult GetCarrierExceptions(int carrierId)
        {
            var list = db.GetCarrierExceptions(carrierId).ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public string UpdateCarrierException([Bind(Exclude = "Id")] FormCollection formCollection, string carrierId)
        {
            string msg;
            Int16 intID;
            Int16 intCarrierID;
            string strOper = Convert.ToString(formCollection["oper"]);

            intID = Convert.ToInt16(formCollection["ExceptionId"]);
            intCarrierID = Convert.ToInt16(carrierId);
            carrier_pricing cpObj = db.carrier_pricing.Find(intID);
            if (cpObj != null)
            {
                cpObj.from_zone = Convert.ToString(formCollection["from_zone"]);
                cpObj.to_zone = Convert.ToString(formCollection["to_zone"]);
                cpObj.service_point = Convert.ToString(formCollection["service_point"]);
                cpObj.discount = Convert.ToDouble(formCollection["discount_rate"]) / 100;
                cpObj.absolute_minimum = Convert.ToDecimal(formCollection["absolute_minimum"]);
            }
            else //add new row
            {
                if (strOper == "add")
                {
                    //Get the next ID
                    //System.Data.Objects.ObjectParameter RetID = new System.Data.Objects.ObjectParameter("intID", typeof(int)); //variable intID is defined in SP as OUTPUT
                    //db.GetMaxCustomerPricingID(RetID);                   
                    //no need to set ID because customer_pricing table is set as identity auto-increment
                    //newRow.ID = Convert.ToInt16(RetID.Value) + 1;

                    carrier_pricing newRow = new carrier_pricing();
                    newRow.carrier_id = intCarrierID;
                    newRow.customer_id = Convert.ToInt16(formCollection["customer_id"]);
                    newRow.from_zone = Convert.ToString(formCollection["from_zone"]);
                    newRow.to_zone = Convert.ToString(formCollection["to_zone"]);
                    newRow.service_point = Convert.ToString(formCollection["service_point"]);
                    newRow.discount = Convert.ToDouble(formCollection["discount_rate"]) / 100;
                    newRow.absolute_minimum = Convert.ToDecimal(formCollection["absolute_minimum"]);
                    db.carrier_pricing.Add(newRow);
                }
            }
            try
            {
                if (ModelState.IsValid)
                {
                    db.SaveChanges();
                    if (strOper == "add")
                    {
                        msg = "Add successful";
                    }
                    else if (strOper == "edit")
                    {
                        msg = "Edit successful";
                    }
                    else
                    {
                        msg = "";
                    }
                }
                else
                {
                    msg = "Validation data not successful";
                }
            }
            catch (Exception ex)
            {
                msg = "Error occured:" + ex.Message;
            }
            return msg;
        } //eo UpdateCarrierExceptions

        //get the zone detail
        public ActionResult GetZoneDetail(string zoneId)
        {
            DataTable dtData = new DataTable();
            var zoneBegin = "";
            var zoneEnd = "";

            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ToString()))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("GetZoneDetail", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@strZoneID", SqlDbType.NVarChar).Value = zoneId;
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    dtData.Load(reader);
                    zoneBegin = Convert.ToString(dtData.Rows[0]["zip_begin"]);
                    zoneEnd = Convert.ToString(dtData.Rows[0]["zip_end"]);
                }
                reader.Close();
                cmd.Dispose();
            }; //eo using
            var jsonData = new
            {
                begin = Convert.ToString(zoneBegin),
                end = Convert.ToString(zoneEnd)
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
    }
}
