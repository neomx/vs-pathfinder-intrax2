﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Data;
using System.Data.Entity;
using IntraX2.Models;

namespace IntraX2.Controllers
{
    public class ShipperController : BaseController
    {
        //instantiate pathfinderEntities data model entity container, defined in the Models.
        private pathfinderEntities db = new pathfinderEntities();

        //set database connection
        SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ConnectionString);
        
        //
        // GET: /Shipper/

        public ActionResult Index()
        {
            var StateListResult = statesList.GetStates();
            var StateSelectList = new SelectList(StateListResult, "StateCode", "StateName");
            ViewBag.StatesList = StateSelectList;

            var CountryListResult = countryList.GetCountryList();
            var CountrySelectList = new SelectList(CountryListResult, "CountryId", "CountryName");
            ViewBag.CountryList = CountrySelectList;
            
            return View(new IntraX2.Models.shipperModel());
        }

        // POST: /Shipper/Index
        [HttpPost]
        public ActionResult Index(shipperModel sm, FormCollection fc)
        {
            if (Convert.ToString(fc["newShipperFlag"]) == "Y")
            {
                db.Entry(sm.m_shipper).State = EntityState.Added;
                db.SaveChanges();
            }
            else
            {
                db.Entry(sm.m_shipper).State = EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Index", "Shipper");
        }

        [HttpPost]
        public string UpdateLogin(Int32 uid, string id, string password, Int16 updatedBy, DateTime updatedDate)
        {
            string msg = "";
            User userObj = db.Users.Find(uid);
            if (userObj != null) //record found
            {
                userObj.user_id = id;
                userObj.password = password;
                userObj.UpdatedBy = updatedBy;
                userObj.UpdatedDate = updatedDate;
              
                db.SaveChanges();
                msg = "Row saved successfully";
            }
            /*
            else //add new record
            {
                User newRow = new User();
                newRow.UID = uid;
                newRow.user_id = id;
                newRow.password = password;
               
                //insert row to database
                try
                {
                    if (ModelState.IsValid)
                    {
                        db.Users.Add(newRow);
                        db.SaveChanges();
                        msg = "Row added successfully";
                    }
                    else
                    {
                        msg = "Validation data not successfull";
                    }
                }
                catch (Exception ex)
                {
                    msg = "Error occured:" + ex.Message;
                }
            }
            */
            return msg;
        }

        [HttpPost]
        public string UpdateLocation(decimal shipperId, int locationId, string location, string address1, string address2, string city, string state, string zip)
        {
            string msg;
            shipper_locations slObj = db.shipper_locations.Find(shipperId, locationId);
            if (slObj != null) //record found
            {
                slObj.name = location;
                slObj.address_1 = address1;
                slObj.address_2 = address2;
                slObj.city = city;
                slObj.state = state;
                slObj.zip = zip;

                db.SaveChanges();
                msg = "Row saved successfully";
            }
            else //add new record
            {
                shipper_locations newRow = new shipper_locations();
                newRow.shipper_id = shipperId;
                newRow.seq_number = locationId;
                newRow.name = location;
                newRow.address_1 = address1;
                newRow.address_2 = address2;
                newRow.city = city;
                newRow.state = state;
                newRow.zip = zip;

                /*insert row to database*/
                try
                {
                    if (ModelState.IsValid)
                    {
                        db.shipper_locations.Add(newRow);
                        db.SaveChanges();
                        msg = "Row added successfully";
                    }
                    else
                    {
                        msg = "Validation data not successfull";
                    }
                }
                catch (Exception ex)
                {
                    msg = "Error occured:" + ex.Message;
                }
            }
            return msg;
        }

        //soft delete a location
        [HttpPost]
        public string DeleteLocation(int shipperId, int locationId)
        {
            string msg = "";
            shipper_locations slObj = db.shipper_locations.Find(shipperId, locationId);
            if (slObj != null)
            {
                slObj.record_status = "0";
            }

            try
            {
                if (ModelState.IsValid)
                {
                    db.SaveChanges();
                    msg = "Delete successful";
                }
                else
                {
                    msg = "Validation data not successful";
                }
            }
            catch (Exception ex)
            {
                msg = "Error occured:" + ex.Message;
            }

            return msg;
        }

        [HttpPost]
        public string UpdateContact(decimal shipperId, int locationId, int contactId, string phone, string ext, string fax, string email, string name)
        {
            string msg;
            shipper_location_contacts slcObj = db.shipper_location_contacts.Find(shipperId, locationId, contactId);
            if (slcObj != null) //record found
            {
                slcObj.phone = phone;
                slcObj.extension = ext;
                slcObj.fax = fax;
                slcObj.email = email;
                slcObj.name = name;
                msg = "Row saved successfully";
            }
            else //add new record
            {
                shipper_location_contacts newRow = new shipper_location_contacts();
                newRow.shipper_id = shipperId;
                newRow.seq_number = locationId;
                newRow.contact_id = contactId;
                newRow.name = name;
                newRow.phone = phone;
                newRow.extension = ext;
                newRow.fax = fax;
                newRow.email = email;

                /*insert row to database*/
                try
                {
                    if (ModelState.IsValid)
                    {
                        db.shipper_location_contacts.Add(newRow);
                        msg = "Row added successfully";
                    }
                    else
                    {
                        msg = "Validation data not successfull";
                    }
                }
                catch (Exception ex)
                {
                    msg = "Error occured:" + ex.Message;
                }
            }

            db.SaveChanges();
            return msg;
        }

        //soft delete a contact
        [HttpPost]
        public string DeleteContact(int shipperId, int locationId, int contactId)
        {
            string msg = "";
            shipper_location_contacts slcObj = db.shipper_location_contacts.Find(shipperId, locationId, contactId);
            if (slcObj != null)
            {
                slcObj.record_status = "0";
            }

            try
            {
                if (ModelState.IsValid)
                {
                    db.SaveChanges();
                    msg = "Delete successful";
                }
                else
                {
                    msg = "Validation data not successful";
                }
            }
            catch (Exception ex)
            {
                msg = "Error occured:" + ex.Message;
            }

            return msg;
        }

        public ActionResult GetShipperDetail(Int32 shipperId)
        {
            DataTable dtShipper = new DataTable();
            var setupBy = "";
            var setupDate = "";
            var loginID = "";
            var loginPassword = "";
            var name = "";
            var uid = "";
            var role = "";
          
            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ToString()))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("GetShipperDetail", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@intShipperID", SqlDbType.Int).Value = shipperId;
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    dtShipper.Load(reader);
                    setupBy = Convert.ToString(dtShipper.Rows[0]["setup_by"]);
                    setupDate = Convert.ToString(dtShipper.Rows[0]["setupDate"]);
                    loginID = Convert.ToString(dtShipper.Rows[0]["user_id"]);
                    loginPassword = Convert.ToString(dtShipper.Rows[0]["password"]);
                    name = Convert.ToString(dtShipper.Rows[0]["name"]);
                    uid = Convert.ToString(dtShipper.Rows[0]["UID"]);
                    role = Convert.ToString(dtShipper.Rows[0]["u_role"]);
                }
                reader.Close();
                cmd.Dispose();
            }; //eo using

            var jsonData = new
            {
                shipperID = Convert.ToString(shipperId),
                setupDate = setupDate,
                setupBy = setupBy,
                loginID = loginID,
                loginPassword = loginPassword,
                name = name,
                uid = uid,
                role = role
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        } //eo GetShipperDetail

        public ActionResult GetShipperLocationDetail(Int32 shipperId, Int16 locationId)
        {
            DataTable dtLocation = new DataTable();
            var name = "";
            var address1 = "";
            var address2 = "";
            var city = "";
            var state = "";
            var zip = "";
            var country = "";

            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ToString()))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("GetShipperLocationDetail", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@intShipperID", SqlDbType.Int).Value = shipperId;
                cmd.Parameters.Add("@intLocationID", SqlDbType.Int).Value = locationId;
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    dtLocation.Load(reader);
                    name = Convert.ToString(dtLocation.Rows[0]["name"]);
                    address1 = Convert.ToString(dtLocation.Rows[0]["address_1"]);
                    address2 = Convert.ToString(dtLocation.Rows[0]["address_2"]);
                    city = Convert.ToString(dtLocation.Rows[0]["city"]);
                    state = Convert.ToString(dtLocation.Rows[0]["state"]);
                    zip = Convert.ToString(dtLocation.Rows[0]["zip"]);
                    country = Convert.ToString(dtLocation.Rows[0]["country"]);
                }
                reader.Close();
                cmd.Dispose();
            }; //eo using

            var jsonData = new
            {
                name = name,
                address1 = address1,
                address2 = address2,
                city = city,
                state = state,
                zip = zip,
                country = country
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        } //eo GetShipperLocationDetail

        public ActionResult GetShipperLocationContactDetail(Int32 shipperId, Int16 locationId, Int16 contactId)
        {
            DataTable dtContact = new DataTable();
            var phone = "";
            var extension = "";
            var fax = "";
            var email = "";

            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ToString()))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("GetShipperLocationContactDetail", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@intShipperID", SqlDbType.Int).Value = shipperId;
                cmd.Parameters.Add("@intLocationID", SqlDbType.Int).Value = locationId;
                cmd.Parameters.Add("@intContactID", SqlDbType.Int).Value = contactId;
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    dtContact.Load(reader);
                    phone = Convert.ToString(dtContact.Rows[0]["phone"]);
                    extension = Convert.ToString(dtContact.Rows[0]["extension"]);
                    fax = Convert.ToString(dtContact.Rows[0]["fax"]);
                    email = Convert.ToString(dtContact.Rows[0]["email"]);
                }
                reader.Close();
                cmd.Dispose();
            }; //eo using

            var jsonData = new
            {
                phone = phone,
                extension = extension,
                fax = fax,
                email = email
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        } //eo GetShipperLocationContactDetail

        public ActionResult GetMaxUserIDSeq()
        {
            DataTable dtSeq = new DataTable();
            var maxval = "";

            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ToString()))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("GetMaxUserIDSeq", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    dtSeq.Load(reader);
                    maxval = Convert.ToString(dtSeq.Rows[0]["maxseq"]);
                }

                reader.Close();
                cmd.Dispose();
            }; //eo using

            var jsonData = new
            {
                maxval = Convert.ToInt16(maxval) + 1
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        } //eo GetMaxUserIDSeq

        public ActionResult GetMaxShipperContactSeq(int shipperId, int locationId)
        {
            var nextSeqNum = "";

            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ToString()))
            {
                System.Data.Objects.ObjectParameter RetNextSeqNum = new System.Data.Objects.ObjectParameter("intNextVal", typeof(int)); //variable intNextVal is defined in SP as OUTPUT
                db.GetMaxShipperContactSeq(shipperId, locationId, RetNextSeqNum);
                nextSeqNum = Convert.ToString(RetNextSeqNum.Value);
            }; //eo using

            var jsonData = new
            {
                nextSeqNum = nextSeqNum
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
            
            /*
            DataTable dtSeq = new DataTable();
            var maxval = "";

            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ToString()))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("GetMaxShipperContactSeq", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@intShipperID", SqlDbType.Int).Value = shipperId;
                cmd.Parameters.Add("@intLocationID", SqlDbType.Int).Value = locationId;
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    dtSeq.Load(reader);
                    maxval = Convert.ToString(dtSeq.Rows[0]["sequence"]);
                }

                reader.Close();
                cmd.Dispose();
            }; //eo using

            var jsonData = new
            {
                maxval = Convert.ToInt16(maxval) + 1
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
             */
        } //eo GetMaxShipperLocationSeq

        //get shipper list
        public JsonResult GetShipperList()
        {
            var list = db.basp_GetShipperListData().ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        //get locations of the selected shipper
        public JsonResult GetShipperAllLocations(int shipperId)
        {
            var list = db.GetShipperAllLocations(shipperId).ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        //get contacts of the select shipper location
        public JsonResult GetShipperLocationContacts(int shipperId, int locationId)
        {
            var list = db.GetShipperLocationContacts(shipperId, locationId).ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetSequenceNumber()
        {
            //Get the next carrier id sequence number
            System.Data.Objects.ObjectParameter RetSeqNum = new System.Data.Objects.ObjectParameter("intSeqNum", typeof(int)); //variable intSeqNum is defined in SP as OUTPUT
            db.GetSeqNumber(RetSeqNum);
            var jsonData = new
            {
                maxval = Convert.ToInt32(RetSeqNum.Value) + 1
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
    }
}
