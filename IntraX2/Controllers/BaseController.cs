﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IntraX2.Models;

namespace IntraX2.Controllers
{
    public class BaseController : Controller
    {
        public BaseController()
        {
            ViewBag.Menu = BuildHomeMenu();
            ViewBag.AdminMenu = BuildAdminMenu();
            ViewBag.CreateBookingMenu = BuildCreateBookingMenu();
            ViewBag.TrackingMenu = BuildTrackingMenu();
            ViewBag.EditBookingMenu = BuildEditBookingMenu();
            ViewBag.InvoiceMenu = BuildInvoiceMenu();
        }

        private IList<MenuModel> BuildHomeMenu()
        {
            IList<MenuModel> mmHomeList = new List<MenuModel>(){

                // Parent
                new MenuModel(){ Id = 1, Name = "Home", ParentId = 0, SortOrder = 1, LinkText = "Home", ActionName = "Index", ControllerName = "Home"} ,
                new MenuModel(){ Id = 2, Name = "Create Booking", ParentId = 0, SortOrder = 1, LinkText = "Create Booking", ActionName = "Create", ControllerName = "Shipment"} ,
                new MenuModel(){ Id = 3, Name = "Tracking", ParentId = 0, SortOrder = 1, LinkText = "Tracking", ActionName = "Shipments", ControllerName = "Shipment"} ,
                new MenuModel(){ Id = 4, Name = "Administration", ParentId = 0, SortOrder = 1, LinkText = "Administration", ActionName = "", ControllerName = ""} ,
                //new MenuModel(){ Id = 5, Name = "Accounting", ParentId = 0, SortOrder = 1, LinkText = "Accounting", ActionName = "", ControllerName = ""} ,
                new MenuModel(){ Id = 5, Name = "Exit", ParentId = 0, SortOrder = 1, LinkText = "Exit", ActionName = "", ControllerName = ""} ,

                // Children
                //new MenuModel() { Id=21, Name = "Create User", ParentId = 2, SortOrder=1, LinkText = "Home", ActionName = "Index", ControllerName = "Home" },
                //new MenuModel() { Id=22, Name = "Create Group", ParentId = 2, SortOrder=2, LinkText = "Home", ActionName = "Index", ControllerName = "Home" },
                //new MenuModel() { Id=23, Name = "Create Account", ParentId = 2, SortOrder=3, LinkText = "Home", ActionName = "Index", ControllerName = "Home"},

                new MenuModel() { Id=41, Name = "Carrier", ParentId = 4, SortOrder=1, LinkText = "Carrier", ActionName = "Index", ControllerName = "Carrier" },
                new MenuModel() { Id=42, Name = "Consignee", ParentId = 4, SortOrder=2, LinkText = "Consignee", ActionName = "Index", ControllerName = "Consignee" },
                new MenuModel() { Id=43, Name = "Customer", ParentId = 4, SortOrder=3, LinkText = "Customer", ActionName = "Index", ControllerName = "Customer" },
                new MenuModel() { Id=44, Name = "NMFC", ParentId = 4, SortOrder=4, LinkText = "NMFC", ActionName = "Index", ControllerName = "NMFC" },
                new MenuModel() { Id=45, Name = "Shipper", ParentId = 4, SortOrder=5, LinkText = "Shipper", ActionName = "Index", ControllerName = "Shipper" },

                //new MenuModel() { Id=45, Name = "Batch Invoicing", ParentId = 5, SortOrder=1, LinkText = "Batch Invoicing", ActionName = "ShipmentInvoices", ControllerName = "Shipment" },
                //new MenuModel() { Id=46, Name = "Carrier Billing", ParentId = 5, SortOrder=2, LinkText = "Carrier Billing", ActionName = "ShipmentCarrierBills", ControllerName = "Shipment" },
                
                //Sub Children
                //new MenuModel() { Id=321, Name = "Salary Accounts", ParentId = 32, SortOrder=1, LinkText = "Home", ActionName = "Index", ControllerName = "Home" },
                //new MenuModel() { Id=322,Name = "Savings Accounts", ParentId = 32, SortOrder=2, LinkText = "Savings", ActionName = "Savings", ControllerName = "Home" },

            };

            return mmHomeList;
        }

        private IList<MenuModel> BuildCreateBookingMenu()
        {
            IList<MenuModel> mmList = new List<MenuModel>(){

                // Parent
                new MenuModel(){ Id = 1, Name = "Home", ParentId = 0, SortOrder = 1, LinkText = "Home", ActionName = "Index", ControllerName = "Home"} ,
                new MenuModel(){ Id = 2, Name = "Tracking", ParentId = 0, SortOrder = 2, LinkText = "Tracking", ActionName = "Shipments", ControllerName = "Shipment"} ,
                new MenuModel(){ Id = 3, Name = "Administration", ParentId = 0, SortOrder = 3, LinkText = "Administration", ActionName = "", ControllerName = ""} ,
                new MenuModel(){ Id = 4, Name = "Exit", ParentId = 0, SortOrder = 4, LinkText = "Exit", ActionName = "", ControllerName = ""} ,
                //new MenuModel(){ Id = 3, Name = "Accounting", ParentId = 0, SortOrder = 1, LinkText = "Accounting", ActionName = "Index", ControllerName = "Home"} ,

                // Children
                //new MenuModel() { Id=21, Name = "Create User", ParentId = 2, SortOrder=1, LinkText = "Home", ActionName = "Index", ControllerName = "Home" },
                //new MenuModel() { Id=22, Name = "Create Group", ParentId = 2, SortOrder=2, LinkText = "Home", ActionName = "Index", ControllerName = "Home" },
                //new MenuModel() { Id=23, Name = "Create Account", ParentId = 2, SortOrder=3, LinkText = "Home", ActionName = "Index", ControllerName = "Home"},
                new MenuModel() { Id=21, Name = "Carrier", ParentId = 3, SortOrder=1, LinkText = "Carrier", ActionName = "Index", ControllerName = "Carrier" },
                new MenuModel() { Id=22, Name = "Consignee", ParentId = 3, SortOrder=2, LinkText = "Consignee", ActionName = "Index", ControllerName = "Consignee" },
                new MenuModel() { Id=23, Name = "Customer", ParentId = 3, SortOrder=3, LinkText = "Customer", ActionName = "Index", ControllerName = "Customer" },
                new MenuModel() { Id=24, Name = "Shipper", ParentId = 3, SortOrder=4, LinkText = "Shipper", ActionName = "Index", ControllerName = "Shipper" },
                
                // SubChildren
                //new MenuModel() { Id=321, Name = "Salary Accounts", ParentId = 32, SortOrder=1, LinkText = "Home", ActionName = "Index", ControllerName = "Home" },
                //new MenuModel() { Id=322,Name = "Savings Accounts", ParentId = 32, SortOrder=2, LinkText = "Savings", ActionName = "Savings", ControllerName = "Home" },

            };

            return mmList;
        }

        private IList<MenuModel> BuildAdminMenu()
        {
            IList<MenuModel> mmList = new List<MenuModel>(){

                // Parent
                new MenuModel(){ Id = 1, Name = "Home", ParentId = 0, SortOrder = 1, LinkText = "Home", ActionName = "Index", ControllerName = "Home"} ,
                new MenuModel(){ Id = 2, Name = "Administration", ParentId = 0, SortOrder = 2, LinkText = "Administration", ActionName = "", ControllerName = ""} ,
                new MenuModel(){ Id = 3, Name = "Exit", ParentId = 0, SortOrder = 3, LinkText = "Exit", ActionName = "", ControllerName = ""} ,
                //new MenuModel(){ Id = 2, Name = "Admin", ParentId = 0, SortOrder = 1, LinkText = "Home", ActionName = "Index", ControllerName = "Home"} ,
                //new MenuModel(){ Id = 3, Name = "Accounting", ParentId = 0, SortOrder = 1, LinkText = "Accounting", ActionName = "Index", ControllerName = "Home"} ,

                // Children
                new MenuModel() { Id=21, Name = "Carrier", ParentId = 2, SortOrder=1, LinkText = "Carrier", ActionName = "Index", ControllerName = "Carrier" },
                new MenuModel() { Id=22, Name = "Consignee", ParentId = 2, SortOrder=2, LinkText = "Consignee", ActionName = "Index", ControllerName = "Consignee" },
                new MenuModel() { Id=23, Name = "Customer", ParentId = 2, SortOrder=3, LinkText = "Customer", ActionName = "Index", ControllerName = "Customer" },
                new MenuModel() { Id=24, Name = "NMFC", ParentId = 2, SortOrder=4, LinkText = "NMFC", ActionName = "Index", ControllerName = "NMFC" },
                new MenuModel() { Id=25, Name = "Shipper", ParentId = 2, SortOrder=5, LinkText = "Shipper", ActionName = "Index", ControllerName = "Shipper" },
                //new MenuModel() { Id=21, Name = "Create User", ParentId = 2, SortOrder=1, LinkText = "Home", ActionName = "Index", ControllerName = "Home" },
                //new MenuModel() { Id=22, Name = "Create Group", ParentId = 2, SortOrder=2, LinkText = "Home", ActionName = "Index", ControllerName = "Home" },
                //new MenuModel() { Id=23, Name = "Create Account", ParentId = 2, SortOrder=3, LinkText = "Home", ActionName = "Index", ControllerName = "Home"},

                //new MenuModel() { Id=31, Name = "Manage Account", ParentId = 3, SortOrder=1, LinkText = "Manage Account", ActionName = "Index", ControllerName = "Home" },
                //new MenuModel() { Id=32, Name = "GL Accounts", ParentId = 3, SortOrder=2, LinkText = "GL Accounts", ActionName = "Index", ControllerName = "Home" },
                
                //new MenuModel() { Id=321, Name = "Salary Accounts", ParentId = 32, SortOrder=1, LinkText = "Home", ActionName = "Index", ControllerName = "Home" },
                //new MenuModel() { Id=322,Name = "Savings Accounts", ParentId = 32, SortOrder=2, LinkText = "Savings", ActionName = "Savings", ControllerName = "Home" },

            };

            return mmList;
        }

        private IList<MenuModel> BuildTrackingMenu()
        {
            IList<MenuModel> mmList = new List<MenuModel>(){

                // Parent
                new MenuModel(){ Id = 1, Name = "Home", ParentId = 0, SortOrder = 1, LinkText = "Home", ActionName = "Index", ControllerName = "Home"} ,
                new MenuModel(){ Id = 2, Name = "Create Booking", ParentId = 0, SortOrder = 1, LinkText = "Create Booking", ActionName = "Create", ControllerName = "Shipment"} ,
                new MenuModel(){ Id = 3, Name = "Exit", ParentId = 0, SortOrder = 3, LinkText = "Exit", ActionName = "", ControllerName = ""} ,
            };

            return mmList;
        }

        private IList<MenuModel> BuildEditBookingMenu()
        {
            IList<MenuModel> mmList = new List<MenuModel>(){

                // Parent
                new MenuModel(){ Id = 1, Name = "Home", ParentId = 0, SortOrder = 1, LinkText = "Home", ActionName = "Index", ControllerName = "Home"} ,
                new MenuModel(){ Id = 2, Name = "Create Booking", ParentId = 0, SortOrder = 2, LinkText = "Create Booking", ActionName = "Create", ControllerName = "Shipment"} ,
                new MenuModel(){ Id = 3, Name = "Tracking", ParentId = 0, SortOrder = 3, LinkText = "Tracking", ActionName = "Shipments", ControllerName = "Shipment"} ,
                new MenuModel(){ Id = 4, Name = "Exit", ParentId = 0, SortOrder = 4, LinkText = "Exit", ActionName = "", ControllerName = ""} ,
            };

            return mmList;
        }

        private IList<MenuModel> BuildInvoiceMenu()
        {
            IList<MenuModel> mmList = new List<MenuModel>(){

                // Parent
                new MenuModel(){ Id = 1, Name = "Home", ParentId = 0, SortOrder = 1, LinkText = "Home", ActionName = "Index", ControllerName = "Home"} ,
                new MenuModel(){ Id = 2, Name = "Exit", ParentId = 0, SortOrder = 2, LinkText = "Exit", ActionName = "", ControllerName = ""} ,
            };

            return mmList;
        }
    }
}
