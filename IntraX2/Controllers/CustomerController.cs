﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Data;
using System.Data.Entity;
using IntraX2.Models;

namespace IntraX2.Controllers
{
    public class CustomerController : BaseController
    {
        //instantiate pathfinderEntities data model entity container, defined in the Models.
        private pathfinderEntities db = new pathfinderEntities();

        //set database connection
        SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ConnectionString);
 
        //
        // GET: /Customer/

        public ActionResult Index()
        {
            var StateListResult = statesList.GetStates();
            var StateSelectList = new SelectList(StateListResult, "StateCode", "StateName");
            ViewBag.StatesList = StateSelectList;

            var StateCodeListResult = stateCodeList.GetStateCode();
            var StateCodeSelectList = new SelectList(StateCodeListResult, "StateCodeId", "StateCodeName");
            ViewBag.StateCodeList = StateCodeSelectList;

            var CountryListResult = countryCodeList.GetCountries();
            var CountrySelectList = new SelectList(CountryListResult, "CountryCode", "CountryName");
            ViewBag.CountryList = CountrySelectList;
            
            var SalesRepListResult = salesRepList.GetAllSalesReps();
            var SalesRepSelectList = new SelectList(SalesRepListResult, "SalesRepID", "SalesRepAlias");
            ViewBag.SalesRepList = SalesRepSelectList;

            var InvoiceTypeListResult = invoiceTypeList.GetInvoiceTypes();
            var InvoiceTypeSelectList = new SelectList(InvoiceTypeListResult, "CodeId", "Description");
            ViewBag.InvoiceTypeList = InvoiceTypeSelectList;

            var InvoiceCycleListResult = invoiceCycleList.GetInvoiceCycle();
            var InvoiceCycleSelectList = new SelectList(InvoiceCycleListResult, "CodeId", "Description");
            ViewBag.InvoiceCycleList = InvoiceCycleSelectList;

            var PaymentTermsListResult = paymentTermList.GetPaymentTerms();
            var PaymentTermsSelectList = new SelectList(PaymentTermsListResult, "CodeId", "Description");
            ViewBag.PaymentTermsList = PaymentTermsSelectList;

            var TariffModeListResult = tariffModeList.GetModeList();
            var TariffModeSelectList = new SelectList(TariffModeListResult, "CodeId", "Description");
            ViewBag.TariffModeList = TariffModeSelectList;

            return View(new IntraX2.Models.customerModel());
        }

        // POST: /Customer/Index
        [HttpPost]
        public ActionResult Index(customerModel cm, FormCollection fc)
        {
            var msg = "";
            try
            {
                if (Convert.ToString(fc["customerOprFlag"]) == "add")
                {
                    db.Entry(cm.m_customer).State = EntityState.Added;
                    db.SaveChanges();
                }
                else
                {
                    db.Entry(cm.m_customer).State = EntityState.Modified;
                    db.SaveChanges();
                }
                return RedirectToAction("Index", "Customer");               
            }
            catch (Exception ex)
            {
                msg = "Error occured:" + ex.Message;
                ViewData["ErrorMsg"] = msg;
                return View("ErrorView1");
            }       
        }

        //get customer list
        public JsonResult GetCustomerList()
        {
            var list = db.GetCustomerListData().ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCustomerDetail(Int32 customerId)
        {
            DataTable dtCustomer = new DataTable();
            var CustomerName = "";
            int SalesRepId = 0;
            Boolean IsActive = true;
            var Notes = "";
            var CreatedDate = "";
            var CreatedBy = "";
            var SetupBy = "";
            var PriAddress1 = "";
            var PriAddress2 = "";
            var PriCity = "";
            var PriState = "";
            var PriZip = "";
            var PriCountry = "";
            var BillAddress1 = "";
            var BillAddress2 = "";
            var BillCity = "";
            var BillState = "";
            var BillZip = "";
            var BillCountry = "";
            var InvoicePref = "";
            Boolean IsCreditOnFile = true;
            var ApprovedDate = "";
            var InvoiceCycle = "";
            var CreditLimit = "";
            var PaymentTerm = "";
            Boolean IsInvoiceOverride = false;
            Boolean IsPODRequired = false;
            Boolean IsBOLRequired = false;
            var TariffModule = "";
            var InterStateDisc = "";
            var InterStateAMC = "";
            var IntraStateDisc = "";
            var IntraStateAMC = "";
            var RateEffectiveDate = "";
            Boolean IsAutoRateSuppressed = false;
            var ExceptionNotes = "";
            Boolean IsRateException = true;
            var UpdatedDate = "";
            var UpdatedBy = "";
            var InactiveDate = "";
            var UpdatedByID = "";
            var CreatedByID = "";
            var InvoiceNotes = "";

            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ToString()))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("GetCustomerDetail", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@intCustomerID", SqlDbType.Int).Value = customerId;
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    dtCustomer.Load(reader);
                    CustomerName = Convert.ToString(dtCustomer.Rows[0]["CustomerName"]);
                    SalesRepId = Convert.ToInt16(dtCustomer.Rows[0]["SalesRepId"]);
                    IsActive = Convert.ToBoolean(dtCustomer.Rows[0]["IsActive"]);
                    Notes = Convert.ToString(dtCustomer.Rows[0]["Notes"]);
                    CreatedDate = Convert.ToString(dtCustomer.Rows[0]["created_date"]);
                    CreatedBy = Convert.ToString(dtCustomer.Rows[0]["UserName"]);
                    CreatedByID = Convert.ToString(dtCustomer.Rows[0]["CreatedBy"]);
                    SetupBy = Convert.ToString(dtCustomer.Rows[0]["Setup_By"]);
                    PriAddress1 = Convert.ToString(dtCustomer.Rows[0]["Address1"]);
                    PriAddress2 = Convert.ToString(dtCustomer.Rows[0]["Address2"]);
                    PriCity = Convert.ToString(dtCustomer.Rows[0]["CityName"]);
                    PriState = Convert.ToString(dtCustomer.Rows[0]["StateId"]);
                    PriZip = Convert.ToString(dtCustomer.Rows[0]["ZipCode"]);
                    PriCountry = Convert.ToString(dtCustomer.Rows[0]["CountryId"]);
                    BillAddress1 = Convert.ToString(dtCustomer.Rows[0]["BillingAddress1"]);
                    BillAddress2 = Convert.ToString(dtCustomer.Rows[0]["BillingAddress2"]);
                    BillCity = Convert.ToString(dtCustomer.Rows[0]["BillingCity"]);
                    BillState = Convert.ToString(dtCustomer.Rows[0]["BillingStateId"]);
                    BillZip = Convert.ToString(dtCustomer.Rows[0]["BillingZipCode"]);
                    BillCountry = Convert.ToString(dtCustomer.Rows[0]["BillingCountryId"]);
                    InvoicePref = Convert.ToString(dtCustomer.Rows[0]["InvoicePrefCodeId"]);
                    IsCreditOnFile = Convert.ToBoolean(dtCustomer.Rows[0]["IsCreditOnFile"]);
                    ApprovedDate = Convert.ToString(dtCustomer.Rows[0]["approve_date"]);
                    InvoiceCycle = Convert.ToString(dtCustomer.Rows[0]["InvoiceCycleCodeId"]);
                    CreditLimit = Convert.ToString(dtCustomer.Rows[0]["CreditLimit"]);
                    PaymentTerm = Convert.ToString(dtCustomer.Rows[0]["PaymentTermCodeId"]);
                    IsInvoiceOverride = Convert.ToBoolean(dtCustomer.Rows[0]["IsInvoiceStatusOverride"]);
                    IsPODRequired = Convert.ToBoolean(dtCustomer.Rows[0]["IsPodRequired"]);
                    IsBOLRequired = Convert.ToBoolean(dtCustomer.Rows[0]["IsOrigBOLRequired"]);
                    TariffModule = Convert.ToString(dtCustomer.Rows[0]["TariffModCodeId"]);
                    InterStateDisc = Convert.ToString(dtCustomer.Rows[0]["InterStateDiscount"]);
                    InterStateAMC = Convert.ToString(dtCustomer.Rows[0]["InterStateMinCharge"]);
                    IntraStateDisc = Convert.ToString(dtCustomer.Rows[0]["IntraStateDiscount"]);
                    IntraStateAMC = Convert.ToString(dtCustomer.Rows[0]["IntraStateMinCharge"]);
                    RateEffectiveDate = Convert.ToString(dtCustomer.Rows[0]["rate_effective_date"]);
                    IsAutoRateSuppressed = Convert.ToBoolean(dtCustomer.Rows[0]["IsAutoRateSuppressed"]);
                    ExceptionNotes = Convert.ToString(dtCustomer.Rows[0]["RateExceptionNotes"]);
                    IsRateException = Convert.ToBoolean(dtCustomer.Rows[0]["IsRateException"]);
                    UpdatedDate = Convert.ToString(dtCustomer.Rows[0]["updated_date"]);
                    UpdatedBy = Convert.ToString(dtCustomer.Rows[0]["updated_by"]);
                    UpdatedByID = Convert.ToString(dtCustomer.Rows[0]["UpdatedBy"]);
                    InactiveDate = Convert.ToString(dtCustomer.Rows[0]["inactive_date"]);
                    InvoiceNotes = Convert.ToString(dtCustomer.Rows[0]["InvoiceNotes"]);
                }
                reader.Close();
                cmd.Dispose();
            }; //eo using

            var jsonData = new
            {
                customerID = Convert.ToString(customerId),
                CustomerName = CustomerName,
                SalesRepId = SalesRepId,
                IsActive = IsActive,
                Notes = Notes,
                CreatedDate = CreatedDate,
                CreatedBy = CreatedBy,
                SetupBy = SetupBy,
                PriAddress1 = PriAddress1,
                PriAddress2 = PriAddress2,
                PriCity = PriCity,
                PriState = PriState,
                PriZip = PriZip,
                PriCountry = PriCountry,
                BillAddress1 = BillAddress1,
                BillAddress2 = BillAddress2,
                BillCity = BillCity,
                BillState = BillState,
                BillZip = BillZip,
                BillCountry = BillCountry,
                InvoicePref = InvoicePref,
                IsCreditOnFile = IsCreditOnFile,
                ApprovedDate = ApprovedDate,
                InvoiceCycle = InvoiceCycle,
                CreditLimit = CreditLimit,
                PaymentTerm = PaymentTerm,
                IsInvoiceOverride = IsInvoiceOverride,
                IsPODRequired = IsPODRequired,
                IsBOLRequired = IsBOLRequired,
                TariffModule = TariffModule,
                InterStateDisc = InterStateDisc,
                InterStateAMC = InterStateAMC,
                IntraStateDisc = IntraStateDisc,
                IntraStateAMC = IntraStateAMC,
                RateEffectiveDate = RateEffectiveDate,
                IsAutoRateSuppressed = IsAutoRateSuppressed,
                ExceptionNotes = ExceptionNotes,
                IsRateException = IsRateException,
                UpdatedDate = UpdatedDate,
                UpdatedBy = UpdatedBy,
                InactiveDate = InactiveDate,
                CreatedByID = CreatedByID,
                UpdatedByID = UpdatedByID,
                InvoiceNotes = InvoiceNotes
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        } //eo GetCustomerrDetail

        //get customer contacts of the selected customer
        public JsonResult GetCustomerContacts(int customerId)
        {
            var list = db.GetCustomerContacts(customerId).ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCustomerContactDetail(Int32 customerId, Int16 contactId)
        {
            DataTable dtContact = new DataTable();
            var phone = "";
            var extension = "";
            var fax = "";
            var email = "";
            var title = "";
            var notes = "";
            Boolean isActive = true;
            var type = "";

            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ToString()))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("GetCustomerContactDetail", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@intCustomerID", SqlDbType.Int).Value = customerId;
                cmd.Parameters.Add("@intContactID", SqlDbType.Int).Value = contactId;
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    dtContact.Load(reader);
                    phone = Convert.ToString(dtContact.Rows[0]["Phone"]);
                    extension = Convert.ToString(dtContact.Rows[0]["Ext"]);
                    fax = Convert.ToString(dtContact.Rows[0]["Fax"]);
                    email = Convert.ToString(dtContact.Rows[0]["Email"]);
                    notes = Convert.ToString(dtContact.Rows[0]["Notes"]);
                    title = Convert.ToString(dtContact.Rows[0]["JobTitle"]);
                    isActive = Convert.ToBoolean(dtContact.Rows[0]["IsActive"]);
                    type = Convert.ToString(dtContact.Rows[0]["CustContactCodeTypeId"]);
                }
                reader.Close();
                cmd.Dispose();
            }; //eo using

            var jsonData = new
            {
                phone = phone,
                extension = extension,
                fax = fax,
                email = email,
                notes = notes,
                title = title,
                isActive = isActive,
                type = type
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        } //eo GetCustomerContactDetail

        public ActionResult Exceptions()
        {
            return View();
        }

        //get the customer exceptions
        public JsonResult GetCustomerExceptions(int customerId)
        {
            var list = db.GetCustomerExceptions(customerId).ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public string UpdateCustomerException([Bind(Exclude = "Id")] shipment_shipper objSS, FormCollection formCollection, string customerId)
        {
            string msg;
            Int16 intID;
            Int16 intCustomerID;
            string strOper = Convert.ToString(formCollection["oper"]);

            intID = Convert.ToInt16(formCollection["ExceptionId"]);
            intCustomerID = Convert.ToInt16(customerId);
            customer_pricing cpObj = db.customer_pricing.Find(intID);
            if (cpObj != null)
            {
                cpObj.from_zone = Convert.ToString(formCollection["from_zone"]);
                cpObj.to_zone = Convert.ToString(formCollection["to_zone"]);
                cpObj.discount = Convert.ToDouble(formCollection["discount_rate"]) / 100;
                cpObj.absolute_minimum = Convert.ToDecimal(formCollection["absolute_minimum"]);
            }
            else //add new row
            {
                if (strOper == "add")
                {
                    //Get the next ID
                    //System.Data.Objects.ObjectParameter RetID = new System.Data.Objects.ObjectParameter("intID", typeof(int)); //variable intID is defined in SP as OUTPUT
                    //db.GetMaxCustomerPricingID(RetID);                   
                    //no need to set ID because customer_pricing table is set as identity auto-increment
                    //newRow.ID = Convert.ToInt16(RetID.Value) + 1;

                    customer_pricing newRow = new customer_pricing();
                    newRow.customer_id = intCustomerID;
                    newRow.carrier_id = Convert.ToDecimal(formCollection["carrier_id"]);
                    newRow.from_zone = Convert.ToString(formCollection["from_zone"]);
                    newRow.to_zone = Convert.ToString(formCollection["to_zone"]);
                    newRow.discount = Convert.ToDouble(formCollection["discount_rate"]) / 100;
                    newRow.absolute_minimum = Convert.ToDecimal(formCollection["absolute_minimum"]);
                    db.customer_pricing.Add(newRow);
                }
            }
            try
            {
                if (ModelState.IsValid)
                {
                    db.SaveChanges();
                    if (strOper == "add")
                    {
                        msg = "Add successful";
                    }
                    else if (strOper == "edit")
                    {
                        msg = "Edit successful";
                    }
                    else
                    {
                        msg = "";
                    }
                }
                else
                {
                    msg = "Validation data not successful";
                }
            }
            catch (Exception ex)
            {
                msg = "Error occured:" + ex.Message;
            }
            return msg;
        }

        [HttpPost]
        public string UpdateContact(int customerId, int contactId, string title, string phone, string ext, string fax, string email, string notes, string name)
        {
            string msg = "";
            CustomerContact ccObj = db.CustomerContacts.Find(contactId, customerId);
            if (ccObj != null) //record found
            {
                ccObj.Name = name;
                ccObj.Phone = phone;
                ccObj.Ext = ext;
                ccObj.Fax = fax;
                ccObj.Email = email;
                ccObj.Notes = notes;
                ccObj.JobTitle = title;
                msg = "Row saved successfully";
            }
            //add new record
            else
            {
                CustomerContact newRow = new CustomerContact();
                newRow.CustomerId = customerId;
                newRow.CustomerContactId = contactId;
                newRow.JobTitle = title;
                newRow.Name = name;
                newRow.Phone = phone;
                newRow.Ext = ext;
                newRow.Fax = fax;
                newRow.Email = email;
                newRow.Notes = notes;
                newRow.IsActive = true;
                newRow.CreatedBy = Convert.ToInt16(Request.Cookies["userLoginIDCookie"].Value);
                newRow.CreatedDate = DateTime.Now;
                
                //insert row to database
                try
                {
                    db.CustomerContacts.Add(newRow);
                    msg = "Row added successfully";   
                }
                catch (Exception ex)
                {
                    msg = "Error occured:" + ex.Message;
                }
            } //eo else
            
            db.SaveChanges();
            return msg;
        } //eo UpdateContact

        public ActionResult GetMaxCustomerContactSeq(int customerId)
        {
            var nextSeqNum = "";

            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ToString()))
            {
                System.Data.Objects.ObjectParameter RetNextSeqNum = new System.Data.Objects.ObjectParameter("intNextVal", typeof(int)); //variable intNextVal is defined in SP as OUTPUT
                db.GetMaxCustomerContactSeq(customerId, RetNextSeqNum);
                nextSeqNum = Convert.ToString(RetNextSeqNum.Value);
            }; //eo using

            var jsonData = new
            {
                nextSeqNum = nextSeqNum
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        } //eo GetMaxCustomerContactSeq

        //soft delete a contact
        [HttpPost]
        public string DeleteContact(int customerId, int contactId)
        {
            string msg = "";
            CustomerContact ccObj = db.CustomerContacts.Find(contactId, customerId);
            if (ccObj != null)
            {
                ccObj.record_status = "0";
            }

            try
            {
                if (ModelState.IsValid)
                {
                    db.SaveChanges();
                    msg = "Delete successful";
                }
                else
                {
                    msg = "Validation data not successful";
                }
            }
            catch (Exception ex)
            {
                msg = "Error occured:" + ex.Message;
            }

            return msg;
        } //eo DeleteContact
    }
}
