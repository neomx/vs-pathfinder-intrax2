﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Data;
using System.Data.Entity;
using IntraX2.Models;

namespace IntraX2.Controllers
{
    public class CatalogController : Controller
    {
        //instantiate pathfinderEntities data model entity container, defined in the Models.
        private pathfinderEntities db = new pathfinderEntities();

        //set database connection
        SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ConnectionString);
        
        //
        // GET: /Catalog/

        public ActionResult Index()
        {
            return View(new IntraX2.Models.catalogModel());
        }

        //get category list
        public JsonResult GetCategoryList()
        {
            var list = db.GetCategoryList().ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        //get products of the selected category
        public JsonResult GetCategoryProductList(int categoryId)
        {
            var list = db.GetCategoryProduct(categoryId).ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

    }
}
