﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Data;
using System.Data.Entity;
using IntraX2.Models;


namespace IntraX2.Controllers
{
    public class NMFCController : BaseController
    {
        //instantiate pathfinderEntities data model entity container, defined in the Models.
        private pathfinderEntities db = new pathfinderEntities();

        //set database connection
        SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ConnectionString);

        [HttpPost]
        public string UpdateNMFC(Int32 nmfcItemID, string nmfcCategory, string nmfcItem)
        {
            string msg = "";
            
            nmfc_item NMFCItemObj = db.nmfc_item.Find(nmfcItemID);
            if (NMFCItemObj != null) //record found
            {
                NMFCItemObj.nmfc_category = nmfcCategory;
                msg = "Row saved successfully";
            }
            else
            {
                nmfc_item newRow = new nmfc_item();
                newRow.nmfc_item_id = nmfcItemID;
                newRow.nmfc_category = nmfcCategory;
                newRow.nmfc_item1 = nmfcItem;

                try
                {
                    db.nmfc_item.Add(newRow);
                    msg = "Row added successfully";
                }
                catch (Exception ex)
                {
                    msg = "Error occured:" + ex.Message;
                }
            }
            
            db.SaveChanges();
            return msg;
        }
        
        //get nfmc list
        public JsonResult GetNMFCList()
        {
            var list = db.GetNMFC_ITX2_sp().ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        //get nfmc sub list
        public JsonResult GetNMFCSubList(string nmfcId)
        {
            var list = db.GetNMFCSub(nmfcId).ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        //get nfmc category list
        public JsonResult GetNMFCCategoryList()
        {
            var list = db.GetNMFCCategory().ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        //get nfmc class list
        public JsonResult GetNMFCClassList()
        {
            var list = db.GetNMFCClass().ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        //get nfmc sub detail
        public ActionResult GetNMFCSubDetail(string nmfcId, int nmfcSubId)
        {
            DataTable dtSubDetail = new DataTable();
            var setupDate = "";
            var setupBy = "";
            var category = "";
            var classCode = "";
            var description = "";

            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ToString()))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("GetNMFCSubDetail", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@strItem", SqlDbType.NVarChar).Value = nmfcId;
                cmd.Parameters.Add("@intSub", SqlDbType.Int).Value = nmfcSubId;
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    dtSubDetail.Load(reader);
                    setupDate = Convert.ToString(dtSubDetail.Rows[0]["sub_setup_date"]);
                    setupBy = Convert.ToString(dtSubDetail.Rows[0]["setup_by"]);
                    category = Convert.ToString(dtSubDetail.Rows[0]["nmfc_category"]);
                    classCode = Convert.ToString(dtSubDetail.Rows[0]["nmfc_class"]);
                    description = Convert.ToString(dtSubDetail.Rows[0]["nmfc_description"]);
                }
                reader.Close();
                cmd.Dispose();
            }; //eo using

            var jsonData = new
            {
                setupDate = setupDate,
                setupBy = setupBy,
                category = category,
                classCode = classCode,
                description = description
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        } //eo GetNMFCSubDetail

        //get nfmc data detail
        public ActionResult GetNMFCItemData(decimal nmfcItemId)
        {
            DataTable dtNMFCData = new DataTable();
            var category = "";
            
            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ToString()))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("GetNMFCItemData", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@SelectedValue", SqlDbType.Decimal).Value = nmfcItemId;
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    dtNMFCData.Load(reader);
                    category = Convert.ToString(dtNMFCData.Rows[0]["nmfc_category"]);
                }
                reader.Close();
                cmd.Dispose();
            }; //eo using

            var jsonData = new
            {
                category = category
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        } //eo GetNMFCItemData

        // GET: /NMFC/
        public ActionResult Index()
        {
            return View();
        }
    }
}
