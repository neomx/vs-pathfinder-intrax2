﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Configuration;
using IntraX2.Models;

namespace IntraX2.DAL
{
    public class pathfinderContext : DbContext
    {
        public DbSet<shipment> Shipments { get; set; }
        public DbSet<shipment_items> Shipment_Items { get; set; }
    }
}