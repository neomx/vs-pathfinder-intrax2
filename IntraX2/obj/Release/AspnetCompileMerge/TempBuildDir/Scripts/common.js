﻿function getCookie(name) {
    var re = new RegExp(name + "=([^;]+)");
    var value = re.exec(document.cookie);
    return (value != null) ? unescape(value[1]) : null;
}

function calculateTotalPieces(rowId) {
    var i;
    var grid = jQuery("#commodityGrid");
    var totalPieces = 0;
    i = getColumnIndexByName(grid, 'pieces'); // nth-child need 1-based index so we use (i+1) below
    $("tbody > tr.jqgrow > td:nth-child(" + (i + 1) + ")", grid[0]).each(function () {
        totalPieces += Number(getTextFromCell(this));
    });
    document.getElementById("totalPieces").innerHTML = "Total Pieces: " + totalPieces;
}

function calculateTotalWeight(rowId) {
    var i;
    var grid = jQuery("#commodityGrid");
    var totalWeight = 0;
    i = getColumnIndexByName(grid, 'weight'); // nth-child need 1-based index so we use (i+1) below
    $("tbody > tr.jqgrow > td:nth-child(" + (i + 1) + ")", grid[0]).each(function () {
        totalWeight += Number(getTextFromCell(this));
    });
    document.getElementById("totalWeight").innerHTML = "Total Weight: " + totalWeight;
}

function calculateDensity(rowId) {
    var refWeight = '#' + rowId + '_weight';
    var refLength = '#' + rowId + '_length';
    var refWidth = '#' + rowId + '_width';
    var refHeight = '#' + rowId + '_height';
    var refPallet = '#' + rowId + '_pallet';
    var refDensity = '#' + rowId + '_density';
    var density;

    if ($(refWeight).val() != '' && $(refLength).val() != '' && $(refWidth).val() != '' && $(refHeight).val() != '' && $(refPallet).val()) {
        density = calculateNMFCDensity($(refLength).val(), $(refWidth).val(), $(refHeight).val(), $(refWeight).val(), $(refPallet).val());
        $(refDensity).val(density);
    }
    else {
        $(refDensity).val('');
    }
}

function createCommodityText() {
    var i;
    var grid = jQuery("#commodityGrid");
    var totalPieces = 0;
    var totalPallets = 0;
    //var refPackage = '#1' + '_packaging';
    //var refPallet = '#1' + '_pallet';
    var packageText = "";
    var text1 = "";
    var text2 = "";
    i = getColumnIndexByName(grid, 'pieces'); // nth-child need 1-based index so we use (i+1) below

    var count = $("#commodityGrid").getGridParam("reccount");
    if (count > 0)
    {
        var refPackage = '#1' + '_packaging';
        var refPallet = '#1' + '_pallet';

        $("tbody > tr.jqgrow > td:nth-child(" + (i + 1) + ")", grid[0]).each(function () {
            totalPieces += Number(getTextFromCell(this));
        });

        p = getColumnIndexByName(grid, 'pallet'); // nth-child need 1-based index so we use (i+1) below
        $("tbody > tr.jqgrow > td:nth-child(" + (p + 1) + ")", grid[0]).each(function () {
            totalPallets += Number(getTextFromCell(this));
        });

        if (getCookie("actionCookie") == 'create_booking') {
            packageText = $(refPackage).val();
        }
        else {
            packageText = document.getElementById("1_packaging").options[$(refPackage).val()].text;
        }

        if (totalPieces > 0 && $(refPackage).val() != 0) {
            if ($(refPallet).val() != '') {
                document.getElementById("commodityText").innerHTML = totalPieces + ' ' + packageText + '(s) in ' + totalPallets + ' Pallet(s)';
            }
            else {
                document.getElementById("commodityText").innerHTML = totalPieces + ' ' + packageText + '(s)';
            }
        }
        else {
            document.getElementById("commodityText").innerHTML = '';
        }
    }


    /* 1-9-16
    $("tbody > tr.jqgrow > td:nth-child(" + (i + 1) + ")", grid[0]).each(function () {
        totalPieces += Number(getTextFromCell(this));
    });

    p = getColumnIndexByName(grid, 'pallet'); // nth-child need 1-based index so we use (i+1) below
    $("tbody > tr.jqgrow > td:nth-child(" + (p + 1) + ")", grid[0]).each(function () {
        totalPallets += Number(getTextFromCell(this));
    });

    if (getCookie("actionCookie") == 'create_booking') {
        packageText = $(refPackage).val();
    }
    else {
        packageText = document.getElementById("1_packaging").options[$(refPackage).val()].text;
    }

    if (totalPieces > 0 && $(refPackage).val() != 0) {
        if ($(refPallet).val() != '') {
            document.getElementById("commodityText").innerHTML = totalPieces + ' ' + packageText + '(s) in ' + totalPallets + ' Pallet(s)';
        }
        else {
            document.getElementById("commodityText").innerHTML = totalPieces + ' ' + packageText + '(s)';
        }
    }
    else
    {
        document.getElementById("commodityText").innerHTML = '';
    }
    */
    

    /*
    if (totalPieces > 0 && $(refPackage).val() != 0 && $(refPallet).val() != '') {
        //document.getElementById("commodityText").innerHTML = totalPieces + ' ' + packageText + '(s) in ' + $(refPallet).val() + ' Pallet(s)';
        document.getElementById("commodityText").innerHTML = totalPieces + ' ' + packageText + '(s) in ' + totalPallets + ' Pallet(s)';
    }
    else {
        document.getElementById("commodityText").innerHTML = '';
    }
    */
}

function CheckFunctionRole(strFunction) {
    //get the location of the logged in user role id
    //to see if the role id is defined in the allowed table
    var retcode;
    $.ajax({
        type: "POST",
        url: "/IntraX2/Shipment/GetFunctionRoleCode",
        data: { RoleID: getCookie('userRoleIDCookie'), FunctionDesc: strFunction },
        traditional: true,
        dataType: 'json',
        async: false,
        success: function (data) {
            retcode = data.code;
        }
    });
    return retcode;
}

/*set elements to read only*/
function setElementsReadOnly() {
    $('input, select, textarea, button').attr('disabled', 'disabled');
    $("#btnEdit").removeAttr("disabled");
    $("#btnEditTop").removeAttr("disabled");
    $("#btnDelete").removeAttr("disabled");
    $("#btnPrint").removeAttr("disabled");
    $("#btnPrintTop").removeAttr('disabled');
    $("#bol").removeAttr("disabled");
    $("#shpconfirm").removeAttr("disabled");
    $("#routeladder").removeAttr("disabled");
    $("#btnInvoiceTop").removeAttr("disabled");
    $("#invoice_date").removeAttr("disabled");
    $("#btnOKPrintInvoiceTop").removeAttr("disabled");
    $("#lblInvoiceDate").removeAttr("disabled");
    $("#customer_invoice_date").removeAttr("disabled");
    //$("#customer_invoiced").removeAttr("disabled");

    //hide the add button on the commodity grid
    var $td = $('#add_' + 'commodityGrid');
    $td.hide();
}

/*calculate the density of each NMFC item*/
function calculateNMFCDensity(length, width, height, weight, pallet) {
    var density;
    density = (weight / (((length * width * height) / 1728)));
    return density.toFixed(3);
}

//calculate days in between dates
function calculateDaysLeft(endDate) {
    var days = $.ajax({
        url: '/IntraX2/Shipment/CalculateDaysLeft?expDate=' + endDate,
        async: false,
        success: function (data) {
            //alert(data);
        }
    });
    return days.responseText;
};

//displays a popup dialog with notes information
function DisplayDialog(notes, title) {
    document.getElementById("dialog_notes").value = notes;
    $("#dialog-message").attr('title', title);
    $("#dialog-message").dialog({
        modal: true,
        width: 430,
        height: 193,
        resizable: false,
        buttons: {
            Ok: function () {
                $(this).dialog("close");
            }
        }
    });
};

//get carrier ins expiration date
function getCarrierInsExpDate(carrierID) {
    var expDate = $.ajax({
        url: '/IntraX2/Shipment/GetCarrierInsExpdate?carrierId=' + carrierID,
        async: false,
        success: function (data) {
            //alert(data);
        }
    });
    return expDate.responseText;
};

function getCurrentDate() {
    var currentTime = new Date();
    var month = currentTime.getMonth() + 1;
    var day = currentTime.getDate();
    var year = currentTime.getFullYear();
    return currentTime;
}

function getFullCurrentDateTime() {
    var currentDate = new Date();
    var fullFormattedDateTime = currentDate.toString('M/d/yyyy') + ' ' + currentDate.toString('h:mm:ss tt');
    return fullFormattedDateTime;
}

//save all rows in the grid
function SaveGridRows(gridName) {
    var listRow;
    var grid = jQuery("#" + gridName);
    listRows = grid.getDataIDs();

    //save all rows to local array
    for (i = 1; i <= listRows.length; i++) {
        grid.saveRow(i, true, 'clientArray');
    }
};

//set all rows in the grid to edit
function EditGridRows(gridName) {
    var listRow;
    var grid = jQuery("#" + gridName);
    listRows = grid.getDataIDs();

    for (i = 1; i <= listRows.length; i++) {
        grid.editRow(i, true, 'clientArray');
    }
};

//return the largest number in the array list
function findMaxValue(aryList) {
    var maxValue = Math.max.apply(null, aryList);
    return maxValue;
};

//displays dialog based on code and text parameters
function showDialog(strCode, strText, strTitle, strBooking) {
    switch (strCode) {
        case 'deleteShipment':
            $.Zebra_Dialog(strText, {
                'type': 'confirmation',
                'title': strTitle,
                'width': 300,
                'modal': true,
                'buttons': ['Yes', 'No'],
                'onClose': function (caption) {
                    if (caption == 'Yes') {
                        $.ajax({
                            type: "POST",
                            url: "/IntraX2/Shipment/DeleteShipmentRow?Booking=" + strBooking,
                        })
                        //execute after delete has complete
                        .done(function (msg) {
                            window.location.href = "../Shipment/Shipments";
                        });
                    }
                }
            });
            break;
        case 'information':
            $.Zebra_Dialog(strText, {
                'type': 'information',
                'title': strTitle,
                'width': 500,
                'modal': true
            });
            break;
        case 'error':
            $.Zebra_Dialog(strText, {
                'type': 'error',
                'title': 'Error',
                'width': 300
            });
            break;
        case 'confirmation':
            $.Zebra_Dialog(strText, {
                'type': 'confirmation',
                'title': 'Exit',
                'width': 300,
                'modal': true,
                'buttons': ['Yes', 'No'],
                'onClose': function (caption) {
                    if (caption == 'Yes') {
                        $.cookies.del('userCookie');
                        $.cookies.del('userIDCookie');
                        $.cookies.del('userPassCookie');
                        $.cookies.set('userIDCookie', 'x0');
                        window.location.href = "../";
                        /*
                        if ($.cookies.get('userIDCookie') == 'x0') {
                            window.location.href = "";
                        }
                        else {
                            $.cookies.del('userCookie');
                            $.cookies.del('userIDCookie');
                            $.cookies.del('userPassCookie');
                            $.cookies.set('userIDCookie', 'x0');
                            window.location.href = "../";
                        }
                        */
                    }
                }
            });
            break;
        case 'clear':
            $.Zebra_Dialog(strText, {
                'type': 'confirmation',
                'title': 'Clear',
                'width': 300,
                'modal': true,
                'buttons': ['Yes', 'No'],
                'onClose': function (caption) {
                    if (caption == 'Yes') {
                        document.forms[0].reset();
                    }
                }
            });
            break;
        case 'resetCarrierGrid':
            $.Zebra_Dialog(strText, {
                'type': 'confirmation',
                'title': 'Reset',
                'width': 300,
                'modal': true,
                'buttons': ['Yes', 'No'],
                'onClose': function (caption) {
                    if (caption == 'Yes') {
                        $('#carrierGrid').jqGrid('clearGridData');
                        jQuery("#carrierGrid").trigger("reloadGrid");
                    }
                }
            });
            break;
        case 'resetEditCarrierGrid':
            $.Zebra_Dialog(strText, {
                'type': 'confirmation',
                'title': 'Reset',
                'width': 300,
                'modal': true,
                'buttons': ['Yes', 'No'],
                'onClose': function (caption) {
                    if (caption == 'Yes') {
                        $('#carrierGrid').jqGrid('clearGridData');

                        //hard delete shipment carriers from database
                        $.ajax({
                            type: "POST",
                            url: "/IntraX2/Shipment/DeleteShipmentCarrier",
                            data: { booking: strBooking },
                            traditional: true,
                            dataType: 'json',
                            async: false,
                            success: function (msg) {
                                alert(msg);
                            }
                        });

                        jQuery("#carrierGrid").trigger("reloadGrid");
                    }
                }
            });
            break;
    } //eo switch
}

$(function () {
    //create datepicker class
    $('.datepicker').datepicker({ dateFormat: 'mm/dd/yy' }),

    //function to iterate through grid and sum values in a column
    getColumnIndexByName = function (grid, columnName) {
        var cm = grid.jqGrid('getGridParam', 'colModel');
        for (var i = 0, l = cm.length; i < l; i++) {
            if (cm[i].name === columnName) {
                return i; // return the index
            }
        }
        return -1;
    },

    //function to iterate through grid and get cell value of each row/olumn
    getTextFromCell = function (cellNode) {
        return cellNode.childNodes[0].nodeName === "INPUT" ?
                        cellNode.childNodes[0].value :
                        cellNode.textContent || cellNode.innerText;
    }
}); //eo function
