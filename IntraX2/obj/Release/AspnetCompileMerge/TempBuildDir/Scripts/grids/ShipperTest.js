﻿
    var countries = { '1': 'US', '2': 'UK' },
    //allCountries = {'': 'All', '1': 'US', '2': 'UK'},
    // we use string form of allCountries to have control on the order of items
    allCountries = ':All;1:US;2:UK',
    states = { '1': 'Alabama', '2': 'California', '3': 'Florida', '4': 'Hawaii', '5': 'London', '6': 'Oxford' },
    allStates = ':All;1:Alabama;2:California;3:Florida;4:Hawaii;5:London;6:Oxford',
    statesOfUS = { '1': 'Alabama', '2': 'California', '3': 'Florida', '4': 'Hawaii' },
    statesOfUK = { '5': 'London', '6': 'Oxford' },
    // the next maps contries by ids to states
    statesOfCountry = { '': states, '1': statesOfUS, '2': statesOfUK },
    mydata = [
        { id: '0', country: '1', state: '1', name: "Louise Fletcher" },
        { id: '1', country: '1', state: '3', name: "Jim Morrison" },
        { id: '2', country: '2', state: '5', name: "Sherlock Holmes" },
        { id: '3', country: '2', state: '6', name: "Oscar Wilde" }
    ],
    lastSel = -1,
    grid = $("#list"),
    removeAllOption = function (elem) {
        if (typeof elem === "object" && typeof elem.id === "string" && elem.id.substr(0, 3) !== "gs_") {
            // in the searching bar
            $(elem).find('option[value=""]').remove();
        }
    },
    resetStatesValues = function () {
        // set 'value' property of the editoptions to initial state
        grid.jqGrid('setColProp', 'state', { editoptions: { value: states } });
    },
    setStateValues = function (countryId) {
        // to have short list of options which corresponds to the country
        // from the row we have to change temporary the column property
        grid.jqGrid('setColProp', 'state', { editoptions: { value: statesOfCountry[countryId] } });
    },
    changeStateSelect = function (countryId, countryElem) {
        // build 'state' options based on the selected 'country' value
        var stateId, stateSelect, parentWidth, $row,
            $countryElem = $(countryElem),
            sc = statesOfCountry[countryId],
            isInSearchToolbar = $countryElem.parent().parent().parent().hasClass('ui-search-toolbar'),
                              //$(countryElem).parent().parent().hasClass('ui-th-column')
            newOptions = isInSearchToolbar ? '<option value="">All</option>' : '';

        for (stateId in sc) {
            if (sc.hasOwnProperty(stateId)) {
                newOptions += '<option role="option" value="' + stateId + '">' +
                    states[stateId] + '</option>';
            }
        }

        setStateValues(countryId);

        // populate the subset of contries
        if (isInSearchToolbar) {
            // searching toolbar
            $row = $countryElem.closest('tr.ui-search-toolbar');
            stateSelect = $row.find(">th.ui-th-column select#gs_state");
            parentWidth = stateSelect.parent().width();
            stateSelect.html(newOptions).css({ width: parentWidth });
        } else if ($countryElem.is('.FormElement')) {
            // form editing
            $countryElem.closest('form.FormGrid').find("select#state.FormElement").html(newOptions);
        } else {
            // inline editing
            $row = $countryElem.closest('tr.jqgrow');
            $("select#" + $.jgrid.jqID($row.attr('id')) + "_state").html(newOptions);
        }
    },
    editGridRowOptions = {
        recreateForm: true,
        onclickPgButtons: function (whichButton, $form, rowid) {
            var $row = $('#' + $.jgrid.jqID(rowid)), countryId;
            if (whichButton === 'next') {
                $row = $row.next();
            } else if (whichButton === 'prev') {
                $row = $row.prev();
            }
            if ($row.length > 0) {
                countryId = grid.jqGrid('getCell', $row.attr('id'), 'country');
                changeStateSelect(countryId, $("#country")[0]);
            }
        },
        onClose: function () {
            resetStatesValues();
        }
    };

$(document).ready(function () {
    grid.jqGrid({
        data: mydata,
        datatype: 'local',
        colModel: [
            { name: 'name', width: 200, editable: true },
            {
                name: 'country', width: 100, editable: true, formatter: 'select', stype: 'select', edittype: 'select',
                searchoptions: {
                    value: allCountries,
                    dataInit: function (elem) { removeAllOption(elem); },
                    dataEvents: [
                        { type: 'change', fn: function (e) { changeStateSelect($(e.target).val(), e.target); } },
                        { type: 'keyup', fn: function (e) { $(e.target).trigger('change'); } }
                    ]
                },
                editoptions: {
                    value: countries,
                    dataInit: function (elem) { setStateValues($(elem).val()); },
                    dataEvents: [
                        { type: 'change', fn: function (e) { changeStateSelect($(e.target).val(), e.target); } },
                        { type: 'keyup', fn: function (e) { $(e.target).trigger('change'); } }
                    ]
                }
            },
            {
                name: 'state', width: 100, formatter: 'select', stype: 'select',
                editable: true, edittype: 'select', editoptions: { value: states },
                searchoptions: { value: allStates, dataInit: function (elem) { removeAllOption(elem); } }
            }
        ],
        onSelectRow: function (id) {
            if (id && id !== lastSel) {
                if (lastSel !== -1) {
                    $(this).jqGrid('restoreRow', lastSel);
                    resetStatesValues();
                }
                lastSel = id;
            }
        },
        ondblClickRow: function (id) {
            if (id && id !== lastSel) {
                $(this).jqGrid('restoreRow', lastSel);
                lastSel = id;
            }
            resetStatesValues();
            $(this).jqGrid('editRow', id, {
                keys: true,
                aftersavefunc: function () {
                    resetStatesValues();
                },
                afterrestorefunc: function () {
                    resetStatesValues();
                }
            });
            return;
        },
        editurl: 'clientArray',
        sortname: 'name',
        ignoreCase: true,
        height: '100%',
        viewrecords: true,
        rownumbers: true,
        sortorder: "desc",
        pager: '#pager',
        caption: "Demonstrate dependend select/dropdown lists (inline editing on double-click)"
    });
    grid.jqGrid('navGrid', '#pager', { del: false }, editGridRowOptions, editGridRowOptions);
    grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: true, defaultSearch: "cn" });
})  //eo document.ready