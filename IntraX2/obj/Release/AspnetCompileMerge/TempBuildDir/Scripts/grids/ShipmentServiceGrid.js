﻿var lastSel;
var oldAddRowData = $.fn.jqGrid.addRowData;

//define events for grid row edit and save
var myServicesEditOptions = {
    keys: true,
    oneditfunc: function (rowid) {
        //alert("row with rowid=" + rowid + " is editing.");
        //set the dropdown lists to empty
        //$("select#" + rowid + "_shipper_location").html('<option role="option" value="0"></option>');
        //$("select#" + rowid + "_shipper_contact").html('<option role="option" value="0"></option>');
    },

    aftersavefunc: function (rowid, response, options) {
        //alert("row with rowid=" + rowid + " is successfuly modified.");
    }
};

$(document).ready(function () {
    //get the special services data to populate the dropdown list
    var sName = '';
    var sOptions = '0:--Select Service--;';
    $.getJSON("/IntraX2/Shipment/GetSpecialServicesList", null, function (data) {
        var num_rec = data.length - 1;
        for (var i = 0; i <= num_rec; i++) {
            sName = data[i].service_description.replace(/;/g, ',');  //replaces semicolons in each service description with commas to resolve undefined text in dropdown list
            if (i < num_rec) {
                sOptions = sOptions + data[i].service_id + ":" + sName + ";";
            }
            else {
                sOptions = sOptions + data[i].service_id + ":" + sName;
            }
        }
        setupServicesGrid(sOptions);
    }); //eo getJSON     

    $.jgrid.extend({
        addRowData: function (rowid, rdata, pos, src) {
            if (pos === 'afterSelected' || pos === 'beforeSelected') {
                if (typeof src === 'undefined' && this[0].p.selrow !== null) {
                    src = this[0].p.selrow;
                    pos = (pos === "afterSelected") ? 'after' : 'before';
                } else {
                    pos = (pos === "afterSelected") ? 'last' : 'first';
                }
            }
            return oldAddRowData.call(this, rowid, rdata, pos, src);
        }
    });
});  //eo ready function

function setupServicesGrid(data) {
    $("#listServices").jqGrid({
        datatype: 'local',
        mtype: 'POST',
        colNames: [/*'Inv No', */'','Service', 'ServiceID'],
        colModel: [
           //{ name: 'id', index: 'id', width: 70, align: 'center', sorttype: 'int', searchoptions: { sopt: ['eq', 'ne']} },
           { name: 'act', index: 'act', width: 8, align: 'center', sortable: false, formatter: 'actions',
               formatoptions: {
                   editbutton: false,
                   keys: true, // we want use [Enter] key to save the row and [Esc] to cancel editing.
                   delOptions: {
                       // because I use "local" data I don't want to send the changes
                       // to the server so I use "processing:true" setting and delete
                       // the row manually in onclickSubmit
                       onclickSubmit: function (options, rowid) {
                           var grid_id = $.jgrid.jqID($("#listServices")[0].id),
                               grid_p = $("#listServices")[0].p,
                               newPage = grid_p.page;

                           // reset the value of processing option which could be modified
                           options.processing = true;

                           // delete the row
                           $("#listServices").delRowData(rowid);

                           $.jgrid.hideModal("#delmod" + grid_id,
                                             {
                                                 gb: "#gbox_" + grid_id,
                                                 jqm: options.jqModal, onClose: options.onClose
                                             });
                           if (grid_p.lastpage > 1) {// on the multipage grid reload the grid
                               if (grid_p.reccount === 0 && newPage === grid_p.lastpage) {
                                   // if after deliting there are no rows on the current page
                                   // which is the last page of the grid
                                   newPage--; // go to the previous page
                               }
                               // reload grid to make the row from the next page visable.
                               grid.trigger("reloadGrid", [{ page: newPage }]);
                           }
                           return true;
                       },
                       processing: true
                   } //eo delOptions
               } //eo formatoptions
           }, //eo act column
           { name: 'service', index: 'service', editable: true, width: 75, edittype: 'select', formatter: 'select', editoptions: { value: data,
               dataInit: function (elem) {
                   $(elem).width(410);  // set the width of the field
               }, //eo dataInt
               dataEvents: [
                    {
                        type: 'change',
                            fn: function (e) {
                            //popuplate service id hidden field
                            var row = $(e.target).closest('tr.jqgrow');
                            var rowId = row.attr('id');
                            $("#" + rowId + "_serviceID").val($(e.target).val());
                        } //eo function (e)
                    } //eo dataEvents curly    
                ] //eo dataEvents bracket
            } //eo editoptions
           }, //eo service column
           { name: 'serviceID', index: 'serviceID', width: 20, editable: true, hidden: true }
        ], //eo colModel
        rowNum: 10,
        //rowList: [5, 10, 20],
        pager: '#pagerServices',
        gridview: true,
        rownumbers: true,
        //multiselect: true,
        autoencode: true,
        width: 495,
        ignoreCase: true,
        sortable: true,
        viewrecords: true,
        //toppager: true, //displays the viewrecords info at the top of the grid
        sortname: 'invdate',
        pgbuttons: false,
        pgtext: null, //disable pager text like 'Page 0 of 10'
        sortorder: 'desc',
        height: '100%',
        //caption: 'Shipment Shippers',
        editurl: 'clientArray',
        gridComplete: function () {
            var objRows = $("#listServices tr");
            var objHeader = $("#listServices .jqgfirstrow td");

            if (objRows.length > 1) {
                var objFirstRowColumns = $(objRows[1]).children("td");
                for (i = 0; i < objFirstRowColumns.length; i++) {
                    $(objFirstRowColumns[i]).css("width", $(objHeader[i]).css("width"));
                }
            }
        }
    }); //eo listServices

    jQuery("#listServices").jqGrid('navGrid', '#pagerServices', {
        edit: false,
        add: false,
        del: false,
        view: false,
        search: false,
        refresh: false,
        viewtext: "View",
        closeOnEscape: true,
        edittext: "Edit",
        refreshtext: "Refresh",
        addtext: "Add",
        deltext: "Delete",
        searchtext: "Search"
    }, {}, {}, {}, { multipleSearch: true });
    //$grid.jqGrid('filterToolbar', {stringResult: true, searchOnEnter: true});

    jQuery("#listServices").jqGrid('inlineNav', '#pagerServices', {
        edit: true,
        add: true,
        del: false,
        view: false,
        search: false,
        refresh: false,
        cancel: true,
        save: true,
        edittext: "Edit",
        addtext: "Add",
        savetext: "Save",
        canceltext: "Cancel",
        addParams: {
            position: "afterSelected",
            addRowParams: myServicesEditOptions
        },
        addedrow: "last",
        editParams: myServicesEditOptions
    });
}; //eo setupServicesGrid function