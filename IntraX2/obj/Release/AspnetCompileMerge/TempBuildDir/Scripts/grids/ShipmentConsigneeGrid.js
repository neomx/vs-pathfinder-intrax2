﻿jQuery.extend({
    getConsigneeLocations: function (consigneeId, refLocation, locationId) {
        $.ajax({
            type: "GET",
            url: "/IntraX2/Shipment/GetConsigneeLocations?consigneeId=" + consigneeId,
            traditional: true,
            dataType: 'json',
            success: function (data) {
                //alert(JSON.stringify(data));  //display the returned json string for debug
                var num_rec = data.length;
                var selLocations = "<option value=0>" + "--Select--" + "</option>";
                $.each(
                    data,
                    function (Index, Data) {
                        selLocations = selLocations + "<option value=" + Data.seq_number + ">" + Data.LocationName + "</option>";
                    });
                //set the consignee locations dropdown
                $(refLocation).html(selLocations);
                $(refLocation).val(locationId);
            }
        }); //eo .ajax
    }, //eo getConsigneeLocations

    getConsigneeLocationContacts: function (consigneeId, refContact, locationId, contactId) {
        $.ajax({
            type: "POST",
            url: "/IntraX2/Shipment/GetConsigneeLocationContactList?consigneeId=" + consigneeId + "&locationId=" + locationId,
            traditional: true,
            dataType: 'json',
            success: function (data) {
                //alert(JSON.stringify(data));  //display the returned json string for debug
                var num_rec = data.length;
                var selContacts = "<option value=0>" + "--Select--" + "</option>";
                $.each(
                    data,
                    function (Index, Data) {
                        selContacts = selContacts + "<option value=" + Data.contact_id + ">" + Data.name + "</option>";
                    });
                //set the location contacts dropdown
                $(refContact).html(selContacts);
                $(refContact).val(contactId);
            }
        }); //eo .ajax
    } //eo getShipperLocationContacts
}); //eo extend

//data grid for consignee
var consLocationZip;
function setupConsigneeGrid(booking, consData) {
    var consigneeId = '';
    $("#consigneeGrid").jqGrid({
        url: "/IntraX2/Shipment/GetConsigneeRows?booking=" + booking,
        datatype: 'json',
        mtype: 'GET',
        loadonce: true,
        colNames: ['', '', 'PO', 'Name', 'Location', 'Contact', 'Rqst Dlvy', 'Time', 'Dlvy ETA', 'Actual Dlvy Date', 'Bill Amt', 'Bill To', 'Customer', 'Seq', 'LocationID', 'ContactID', ''],
        colModel: [
            {
                name: 'act', index: 'act', width: 13, align: 'center', resizable: false, sortable: false, formatter: 'actions',
                formatoptions: {
                    editbutton: false,
                    keys: false, // we want use [Enter] key to save the row and [Esc] to cancel editing.
                    onEdit: function (rowid) {
                        alert("in onEdit: rowid=" + rowid + "\nWe don't need return anything");
                    },
                    onSuccess: function (jqXHR) {
                        // the function will be used as "succesfunc" parameter of editRow function
                        // (see http://www.trirand.com/jqgridwiki/doku.php?id=wiki:inline_editing#editrow)
                        alert("in onSuccess used only for remote editing:" +
                                    "\nresponseText=" + jqXHR.responseText +
                                    "\n\nWe can verify the server response and return false in case of" +
                                    " error response. return true confirm that the response is successful");
                        // we can verify the server response and interpret it do as an error
                        // in the case we should return false. In the case onError will be called
                        return true;
                    },
                    onError: function (rowid, jqXHR, textStatus) {
                        // the function will be used as "errorfunc" parameter of editRow function
                        // (see http://www.trirand.com/jqgridwiki/doku.php?id=wiki:inline_editing#editrow)
                        // and saveRow function
                        // (see http://www.trirand.com/jqgridwiki/doku.php?id=wiki:inline_editing#saverow)
                        alert("in onError used only for remote editing:" +
                                    "\nresponseText=" + jqXHR.responseText +
                                    "\nstatus=" + jqXHR.status +
                                    "\nstatusText" + jqXHR.statusText +
                                    "\n\nWe don't need return anything");
                    },
                    afterSave: function (rowid) {
                        alert("in afterSave (Submit): rowid=" + rowid + "\nWe don't need return anything");
                    },
                    afterRestore: function (rowid) {
                        alert("in afterRestore (Cancel): rowid=" + rowid + "\nWe don't need return anything");
                    },
                    delOptions: consigneeDelOptions
                }
            },
            { key: false, name: 'pli_book_number', index: 'pli_book_number', editable: true, hidden: true, editoptions: { readonly: 'readonly' } },
            {
                key: false, name: 'po_number', index: 'po_number', width: 25, editable: true, resizable: false, editoptions: {
                    dataInit: function (el) {
                        $(el).width(78);  //set the width of the dropdown (not the grid column)
                    },
                }
            },
            {
                key: false, name: 'consignee_id', index: 'consignee_id', width: 70, editable: true, resizable: false, editrules: { required: false }, hidden: false, edittype: 'select', formatter: 'select', editoptions: {
                    value: consData,
                    dataInit: function (el) {
                        $(el).width(226);  //set the width of the dropdown (not the grid column)
                    },
                    dataEvents: [
                       {
                           type: 'change',
                           fn: function (e) {
                               consigneeId = $(e.target).val();
                               //var sel = $("#consigneeGrid").getGridParam('selrow');

                               //get a reference to the location_id dropdown field on this same row
                               var refLocation = '#' + $(this).attr("id").replace("_consignee_id", "_consignee_location");

                               //get a reference to the contact dropdown field on this same row
                               var refContact = '#' + $(this).attr("id").replace("_consignee_id", "_consignee_location_contact");

                               //reset the contact dropdown list
                               var res2 = '';
                               res2 += '<option role="option" value="0">--Select--</option>';
                               $(refContact).html(res2);

                               $.getJSON("/IntraX2/Shipment/GetConsigneeLocations?consigneeId=" + consigneeId, null, function (data) {
                                   var selConsLocations = "<option value=0>" + "--Select--" + "</option>";
                                   // Repopulate the project drop down with the results of the JSON call
                                   $.each(
                                       data,
                                       function (Index, Data) {
                                           selConsLocations = selConsLocations + "<option value=" + Data.seq_number + ">" + Data.LocationName + "</option>";
                                       });
                                   // don't use innerHTML as it is not supported properly by IE
                                   // insted use jQuery html to change the select list options
                                   $(refLocation).html(selConsLocations);
                               }); //eo getJSON
                           }
                       }
                    ] //eo dataEvents
                }
            },
            //the location colunm is only used to display the dropdown list, content does not get save to the db.
            {
                key: false, name: 'consignee_location', index: 'consignee_location', width: 45, editable: true, resizable: false, edittype: 'select', formatter: 'select', editoptions: {
                    value: "0:--Select--",
                    dataInit: function (el) {
                        $(el).width(143);  //set the width of the dropdown (not the grid column)
                    },
                    dataEvents: [
                        {
                            type: 'change',
                            fn: function (e) {
                                consLocationId = $(e.target).val();
                                //var sel = $("#consigneeGrid").getGridParam('selrow');

                                //get a reference to the contact_id dropdown field on this same row
                                var refContact = '#' + $(this).attr("id").replace("_consignee_location", "_consignee_location_contact");

                                //get a reference to the location_zip field on this same row
                                var refLocationZip = '#' + $(this).attr("id").replace("_consignee_location", "_consignee_location_zip");

                                //get a reference to the location_state field on this same row
                                var refLocationState = '#' + $(this).attr("id").replace("_consignee_location", "_consignee_location_state");

                                //get a reference to the location_id field on this same row
                                //assign the selected location id to the hidden location_id column, this value gets saved to the db
                                var refLocationID = '#' + $(this).attr("id").replace("_consignee_location", "_consignee_location_id");
                                $(refLocationID).val(consLocationId);

                                //get consignee state, zip and assign them to hidden fields
                                $.getJSON("/IntraX2/Shipment/GetConsigneeLocationStateZip?consigneeId=" + $(consigneeId).val() + "&locationId=" + consLocationId, null, function (data) {
                                    //update display fields
                                    $(refLocationZip).val(data[0].zip);
                                    $(refLocationState).val(data[0].state);

                                    document.getElementById("destzip_1").value = data[0].zip;
                                }); //eo getJSON

                                $.getJSON("/IntraX2/Shipment/GetConsigneeLocationContactList?consigneeId=" + $(consigneeId).val() + "&locationId=" + consLocationId, null, function (data) {
                                    var selConsContacts = "<option value='0'>" + "--Select--" + "</option>";
                                    // Repopulate the project drop down with the results of the JSON call
                                    $.each(
                                        data,
                                        function (Index, Data) {
                                            selConsContacts = selConsContacts + "<option value='" + Data.contact_id + "'>" + Data.name + "</option>";
                                        });
                                    // don't use innerHTML as it is not supported properly by IE
                                    // insted use jQuery html to change the select list options
                                    $(refContact).html(selConsContacts);
                                }); //eo getJSON

                                //get the selected location notes data
                                var notes = getConsigneeLocationNotes(consigneeId, consLocationId);
                                if (notes.length > 0) {
                                    DisplayDialog(notes, 'Consignee Location');
                                }

                            } //eo function
                        }  //eo dataEvents curly
                    ] //eo dataEvents bracket
                },
            },
            //the contact colunm is only used to display the dropdown list, content does not get save to the db.
            {
                key: false, name: 'consignee_location_contact', index: 'consignee_location_contact', width: 40, editable: true, resizable: false, edittype: 'select', formatter: 'select', editoptions: {
                    value: "0:--Select--",
                    dataInit: function (el) {
                        $(el).width(129);  //set the width of the dropdown (not the grid column)
                    },
                    dataEvents: [
                        {
                            type: 'change',
                            fn: function (e) {
                                contactId = $(e.target).val();
                                //get a reference to the location_contact_id field on this same row
                                //assign the selected location contacat id to the hidden location_contact_id column, this value gets saved to the db
                                var refContactID = '#' + $(this).attr("id").replace("_consignee_location_contact", "_consignee_location_contact_id");
                                $(refContactID).val(contactId);
                            }
                        }
                    ]
                }
            },
            {
                key: false, name: 'requested_delivery', index: 'requested_delivery', width: 25, editable: true, resizable: false, formatter: "date", formatoptions: { newformat: "m/d/Y" }, editoptions: {
                    dataInit: function (elem) { $(elem).datepicker(); $(elem).width(78); }
                }
            },
            {
                key: false, name: 'requested_delivery_time', index: 'requested_delivery_time', width: 30, editable: true, resizable: false, editoptions: {
                    dataInit: function (el) {
                        $(el).width(93);  //set the width of the dropdown (not the grid column)
                    },
                }
            },
            {
                key: false, name: 'delivery_eta', index: 'delivery_eta', width: 25, editable: true, resizable: false, formatter: "date", formatoptions: { newformat: "m/d/Y" }, editoptions: {
                    dataInit: function (elem) { $(elem).datepicker(); $(elem).width(77); }
                }
            },
            {
                key: false, name: 'actual_delivery', index: 'actual_delivery', width: 30, editable: true, resizable: false, formatter: "date", formatoptions: { newformat: "m/d/Y" }, editoptions: {
                    dataInit: function (elem) { $(elem).datepicker(); $(elem).width(91); }
                }
            },
            {
                key: false, name: 'bill_amount', index: 'bill_amount', width: 25, editable: true, resizable: false, formatter: 'currency', formatoptions: { prefix: '$', thousandsSeparator: ',' }, editoptions: {
                    defaultValue: 0,
                    dataInit: function (el) {
                        $(el).width(77);  //set the width of the dropdown (not the grid column)
                    },
                }
            },
            {
                key: false, name: 'bill_to_id', index: 'bill_to_id', width: 40, editable: true, resizable: false, hidden: false, edittype: 'select', formatter: 'select', editoptions: {
                    value: "NA:--Select--;CONS:Consignee;CUST:Customer;SHP:Shipper;OTH:Other",
                    dataInit: function (el) {
                        $(el).width(130);  //set the width of the dropdown (not the grid column)
                    },
                    /*
                    dataEvents: [
                        {
                            type: 'change',
                            fn: function (e) {
                                //alert($(e.target).val());
                            }
                        }
                    ]*/
                }
            },
            {
                key: false, name: 'bill_to_customer_id', index: 'bill_to_customer_id', width: 56, editable: true, resizable: false, hidden: false, edittype: 'select', editoptions: {
                    dataInit: function (el) {
                        //$(el).width(213);  //set the width of the dropdown (not the grid column)
                        $(el).width(180);
                    },
                    dataEvents: [
                        {
                            type: 'change',
                            fn: function (e) {
                                selCustomerId = $(e.target).val();

                                //get a reference to the customer_id field on this same row
                                var refCustomerID = '#' + $(this).attr("id").replace("_bill_to_customer_id", "_consignee_customer_id");
                                $(refCustomerID).val(selCustomerId);
                            }
                        }
                    ]
                }
            },
            { key: false, name: 'sequence', index: 'sequence', editable: true, hidden: true, editoptions: { readonly: 'readonly' } },
            { key: false, name: 'location_id', index: 'consignee_location_id', editable: true, hidden: true, editoptions: { readonly: 'readonly' } },
            { key: false, name: 'location_contact_id', index: 'consignee_location_contact_id', editable: true, hidden: true, editoptions: { readonly: 'readonly' } },
            {
                key: false, name: 'select', index: 'select', width: 10, editable: true, align: 'center', resizable: false, edittype: 'checkbox', formatter: 'checkbox', editoptions: {
                    value: "Yes:No",
                    /*
                    dataEvents: [
                        {
                            type: 'change',
                            fn: function (e) {
                                //var refPro = '#' + $(this).attr("id").replace("_select", "_carrier_id");
                                //$(refPro).val("100");
                                //document.getElementById("carrier_1").value = $(refPro).html;
                                //var cell = jQuery('#' + 1 + '_' + "carrier_id");
                                //var val = cell.val();
                                //alert(val);
                                //document.getElementById("carrier_1").value = val;
                            }
                        }
                    ]*/
                }
            },
        ],
        pager: jQuery('#consigneePager'),
        //rowNum: 10,
        //rowList: [10, 20, 30, 40],
        height: '100%',
        viewrecords: true,
        pgbuttons: false, // disable page control like next, back button
        pgtext: null, // disable pager text like 'Page 0 of 10'
        emptyrecords: 'No records to display',
        jsonReader: {
            root: "rows",
            page: "page",
            total: "total",
            records: "records",
            repeatitems: false,
            Id: "0"
        },
        width: 1480,
        multiselect: false,
        afterInsertRow: function (rowid, aData) {
            //var sequence = getConsigneeGridRowCount();
            //jQuery("#consigneeGrid").jqGrid('setCell', rowid, 'sequence', sequence);
            jQuery("#consigneeGrid").setColProp('bill_to_customer_id', { editoptions: { dataUrl: "/IntraX2/Shipment/GetCustomerList" } });
        },
        gridComplete: function () {
            var locationId;
            var contactId;
            //set the entire grid to edit mode
            var $this = $(this), ids = $this.jqGrid('getDataIDs'), i, l = ids.length;
            for (i = 0; i < l; i++) {
                $this.jqGrid('editRow', ids[i], true);

                consigneeId = '#' + (i + 1).toString() + '_consignee_id';
                locationId = '#' + (i + 1).toString() + '_location_id';
                contactId = '#' + (i + 1).toString() + '_location_contact_id';
                refLocation = '#' + (i + 1).toString() + '_consignee_location';
                refContact = '#' + (i + 1).toString() + '_consignee_location_contact';

                //get and populate location list data
                var sLocationSelect = $.getConsigneeLocations($(consigneeId).val(), refLocation, $(locationId).val());  //make sure to use the val() to get the actual value of the field

                //get and populate contact list data
                var sContactSelect = $.getConsigneeLocationContacts($(consigneeId).val(), refContact, $(locationId).val(), $(contactId).val());  //make sure to use the val() to get the actual value of the field
            }
        }
    }); //eo setupConsigneeGrid
} //eo setupConsigneeGrid
