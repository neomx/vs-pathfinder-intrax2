﻿//displays a popup dialog with notes information
function DisplayDialog(notes, title) {
    document.getElementById("dialog_notes").value = notes;
    $("#dialog-message").attr('title', title);
    $("#dialog-message").dialog({
        modal: true,
        width: 430,
        height: 193,
        resizable: false,
        buttons: {
            Ok: function () {
                $(this).dialog("close");
            }
        }
    });
};

//get the shipper location notes
function getShipperLocationNotes(shipperId, locationId) {
    var shipperNotes = $.ajax({
        url: '/IntraX2/Shipment/GetShipperLocationNotes?shipperId=' + shipperId + '&locationId=' + locationId,
        async: false,
        success: function (data) {
            //alert(data);
        }
    });
    return shipperNotes.responseText;
};

jQuery.extend({
    getShipperLocations: function (shipperId, refLocation, locationId) {
        $.ajax({
            type: "GET",
            url: "/IntraX2/Shipment/GetShipperLocations?shipperId=" + shipperId,
            traditional: true,
            dataType: 'json',
            success: function (data) {
                //alert(JSON.stringify(data));  //display the returned json string for debug
                var num_rec = data.length;
                var selLocations = "<option value=0>" + "--Select--" + "</option>";
                $.each(
                    data,
                    function (Index, Data) {
                        selLocations = selLocations + "<option value=" + Data.seq_number + ">" + Data.LocationName + "</option>";
                    });
                //set the shipper locations dropdown
                $(refLocation).html(selLocations);
                $(refLocation).val(locationId);
            }
        }); //eo .ajax
    }, //eo getShipperLocations

    getShipperLocationContacts: function (shipperId, refContact, locationId, contactId) {
        $.ajax({
            type: "POST",
            url: "/IntraX2/Shipment/GetShipperLocationContactList?shipperId=" + shipperId + "&locationId=" + locationId,
            traditional: true,
            dataType: 'json',
            success: function (data) {
                //alert(JSON.stringify(data));  //display the returned json string for debug
                var num_rec = data.length;
                var selContacts = "<option value=0>" + "--Select--" + "</option>";
                $.each(
                    data,
                    function (Index, Data) {
                        selContacts = selContacts + "<option value=" + Data.contact_id + ">" + Data.name + "</option>";
                    });
                //set the location contacts dropdown
                $(refContact).html(selContacts);
                $(refContact).val(contactId);
            }
        }); //eo .ajax
    } //eo getShipperLocationContacts
}); //eo extend

//setup the shipper grid
function setupShipperGrid(booking, data) {
    var shipperId = '';
    $("#shipperGrid").jqGrid({
        url: "/IntraX2/Shipment/GetShipperRows?booking=" + booking,
        datatype: 'json',
        mtype: 'GET',
        loadonce: true,
        colNames: ['', '', 'Reference', 'Name', 'Location', 'Contact', 'Actual PU Date', 'Seq', 'LocationID', 'ContactID'],
        colModel: [
            {
                name: 'act', index: 'act', width: 10, align: 'center', resizable: false, sortable: false, formatter: 'actions',
                formatoptions: {
                    editbutton: false,
                    keys: false, // we want use [Enter] key to save the row and [Esc] to cancel editing.
                    onEdit: function (rowid) {
                        alert("in onEdit: rowid=" + rowid + "\nWe don't need return anything");
                    },
                    onSuccess: function (jqXHR) {
                        // the function will be used as "succesfunc" parameter of editRow function
                        // (see http://www.trirand.com/jqgridwiki/doku.php?id=wiki:inline_editing#editrow)
                        alert("in onSuccess used only for remote editing:" +
                                    "\nresponseText=" + jqXHR.responseText +
                                    "\n\nWe can verify the server response and return false in case of" +
                                    " error response. return true confirm that the response is successful");
                        // we can verify the server response and interpret it do as an error
                        // in the case we should return false. In the case onError will be called
                        return true;
                    },
                    onError: function (rowid, jqXHR, textStatus) {
                        // the function will be used as "errorfunc" parameter of editRow function
                        // (see http://www.trirand.com/jqgridwiki/doku.php?id=wiki:inline_editing#editrow)
                        // and saveRow function
                        // (see http://www.trirand.com/jqgridwiki/doku.php?id=wiki:inline_editing#saverow)
                        alert("in onError used only for remote editing:" +
                                    "\nresponseText=" + jqXHR.responseText +
                                    "\nstatus=" + jqXHR.status +
                                    "\nstatusText" + jqXHR.statusText +
                                    "\n\nWe don't need return anything");
                    },
                    afterSave: function (rowid) {
                        alert("in afterSave (Submit): rowid=" + rowid + "\nWe don't need return anything");
                    },
                    afterRestore: function (rowid) {
                        alert("in afterRestore (Cancel): rowid=" + rowid + "\nWe don't need return anything");
                    },
                    delOptions: shipperDelOptions
                }
            },
            { key: false, name: 'pli_book_number', index: 'pli_book_number', editable: true, hidden: true, editoptions: { readonly: 'readonly' } },
            {
                key: false, name: 'reference_number', index: 'reference_number', width: 23, editable: true, resizable: false, editoptions: {
                    dataInit: function (el) {
                        $(el).width(95);  //set the width of the dropdown (not the grid column)
                    },
                }
            },
            {
                key: false, name: 'shipper_id', index: 'shipper_id', width: 68, resizable: false, editable: true, editrules: { required: false }, hidden: false, edittype: 'select', formatter: 'select', editoptions: {
                    value: data,
                    dataInit: function (el) {
                        $(el).width(300);  //set the width of the dropdown (not the grid column)
                    },
                    dataEvents: [
                       {
                           type: 'change',
                           fn: function (e) {
                               shipperId = $(e.target).val();
                               //var sel = $("#shipperGrid").getGridParam('selrow');

                               //get a reference to the location_id dropdown field on this same row
                               //var refLocation = '#' + $(this).attr("id").replace("_shipper_id", "_location_id");
                               var refLocation = '#' + $(this).attr("id").replace("_shipper_id", "_shipper_location");

                               //get a reference to the contact dropdown field on this same row
                               var refContact = '#' + $(this).attr("id").replace("_shipper_id", "_shipper_location_contact");

                               //reset the contact dropdown list
                               var res2 = '';
                               res2 += '<option role="option" value="0">--Select--</option>';
                               $(refContact).html(res2);

                               $.getJSON("/IntraX2/Shipment/GetShipperLocations?shipperId=" + shipperId, null, function (data) {
                                   var selLocations = "<option value=0>" + "--Select--" + "</option>";
                                   // Repopulate the project drop down with the results of the JSON call
                                   $.each(
                                       data,
                                       function (Index, Data) {
                                           selLocations = selLocations + "<option value=" + Data.seq_number + ">" + Data.LocationName + "</option>";
                                       });
                                   // don't use innerHTML as it is not supported properly by IE
                                   // insted use jQuery html to change the select list options
                                   $(refLocation).html(selLocations);
                               }); //eo getJSON

                               //get shipper specific nmfc list
                               /*
                               $.getJSON("/IntraX2/Shipment/GetShipperNMFCData?shipperId=" + shipperId, null, function (data) {
                                   selNMFC = "0:--Select--;";
                                   //build the shipper specific nmfc list
                                   $.each(
                                       data,
                                       function (Index, Data) {
                                           selNMFC = selNMFC + Data.Value + ":" + Data.Description + ";"
                                       });

                                   //replace the last semicolon with a blank
                                   var selList = selNMFC.replace(/;$/, "");

                                   var recCount = $("#commodityGrid").getGridParam("reccount");
                                   if (recCount = 1) {
                                       jQuery("#commodityGrid").saveRow(1, true, 'clientArray');
                                       //need to set the options before the row goes into edit mode
                                       jQuery("#commodityGrid").jqGrid('setColProp', 'item_value', { editoptions: { value: selList } });
                                       jQuery("#commodityGrid").jqGrid('editRow', 1, false);
                                   }
                               }); //eo getJSON
                               */
                           }
                       }
                    ] //eo dataEvents
                }
            },
            //the location colunm is only used to display the dropdown list, content does not get save to the db.
            {
                key: false, name: 'shipper_location', index: 'shipper_location', width: 50, resizable: false, editable: true, edittype: 'select', formatter: 'select', editoptions: {
                    value: "0:--Select--",
                    dataInit: function (el) {
                        $(el).width(220);  //set the width of the dropdown (not the grid column)
                    },
                    dataEvents: [
                        {
                            type: 'change',
                            fn: function (e) {
                                locationId = $(e.target).val();
                                shipperId = '#' + $(this).attr("id").replace("_shipper_location", "_shipper_id");
                                //var sel = $("#shipperGrid").getGridParam('selrow');

                                //get a reference to the contact_id dropdown field on this same row
                                //var refContact = '#' + $(this).attr("id").replace("_location", "_location_contact_id");
                                var refContact = '#' + $(this).attr("id").replace("_shipper_location", "_shipper_location_contact");

                                //get a reference to the location_zip field on this same row
                                var refLocationZip = '#' + $(this).attr("id").replace("_shipper_location", "_shipper_location_zip");

                                //get a reference to the location_state field on this same row
                                var refLocationState = '#' + $(this).attr("id").replace("_shipper_location", "_shipper_location_state");

                                //get a reference to the location_id field on this same row
                                //assign the selected location id to the hidden location_id column, this value gets saved to the db
                                var refLocationID = '#' + $(this).attr("id").replace("_shipper_location", "_shipper_location_id");
                                $(refLocationID).val(locationId);

                                //get shipper state, zip and assign them to hidden fields
                                $.getJSON("/IntraX2/Shipment/GetShipperLocationStateZip?shipperId=" + $(shipperId).val() + "&locationId=" + locationId, null, function (data) {
                                    //update hidden fields
                                    $(refLocationZip).val(data[0].zip);
                                    $(refLocationState).val(data[0].state);

                                    document.getElementById("orgzip_1").value = data[0].zip;
                                }); //eo getJSON

                                $.getJSON("/IntraX2/Shipment/GetShipperLocationContactList?shipperId=" + $(shipperId).val() + "&locationId=" + locationId, null, function (data) {
                                    var selContacts = "<option value='0'>" + "--Select--" + "</option>";
                                    // Repopulate the project drop down with the results of the JSON call
                                    $.each(
                                        data,
                                        function (Index, Data) {
                                            selContacts = selContacts + "<option value='" + Data.contact_id + "'>" + Data.name + "</option>";
                                        });
                                    // don't use innerHTML as it is not supported properly by IE
                                    // insted use jQuery html to change the select list options
                                    $(refContact).html(selContacts);
                                }); //eo getJSON

                                //get the selected location notes data
                                var notes = getShipperLocationNotes($(shipperId).val(), locationId);
                                if (notes.length > 0) {
                                    DisplayDialog(notes, 'Shipper Location');
                                }
                            } //eo function
                        }  //eo dataEvents curly
                    ] //eo dataEvents bracket
                },
            },
            //the contact colunm is only used to display the dropdown list, content does not get save to the db.
            {
                key: false, name: 'shipper_location_contact', index: 'shipper_location_contact', width: 30, resizable: false, editable: true, edittype: 'select', formatter: 'select', editoptions: {
                    value: "0:--Select--",
                    dataInit: function (el) {
                        $(el).width(130);  //set the width of the dropdown (not the grid column)
                    },
                    dataEvents: [
                        {
                            type: 'change',
                            fn: function (e) {
                                contactId = $(e.target).val();
                                //get a reference to the location_contact_id field on this same row
                                //assign the selected location contact id to the hidden location_contact_id column, this value gets saved to the db
                                var refContactID = '#' + $(this).attr("id").replace("_shipper_location_contact", "_shipper_location_contact_id");
                                $(refContactID).val(contactId);
                            }
                        }
                    ]

                }
            },
            {
                key: false, name: 'actual_pu_date', index: 'actual_pu_date', width: 25, resizable: false, editable: true, formatter: "date", formatoptions: { newformat: "m/d/Y" }, editoptions: {
                    dataInit: function (elem) {
                        $(elem).width(102);
                        $(elem).datepicker();
                    }
                }
            },
            { key: false, name: 'shipper_sequence', index: 'shipper_sequence', editable: true, hidden: true, editoptions: { readonly: 'readonly' } },
            { key: false, name: 'location_id', index: 'shipper_location_id', editable: true, hidden: true, editoptions: { readonly: 'readonly' } },
            { key: false, name: 'location_contact_id', index: 'shipper_location_contact_id', editable: true, hidden: true, editoptions: { readonly: 'readonly' } }
        ],
        pager: jQuery('#shipperPager'),
        //rowNum: 10,
        //rowList: [10, 20, 30, 40],
        height: '100%',
        viewrecords: true,
        pgbuttons: false, // disable page control like next, back button
        pgtext: null, // disable pager text like 'Page 0 of 10'
        emptyrecords: 'No records to display',
        jsonReader: {
            root: "rows",
            page: "page",
            total: "total",
            records: "records",
            repeatitems: false,
            Id: "0"
        },
        width: 950,
        multiselect: false,
        gridComplete: function () {
            var locationId;
            var refLocation;
            var contactId;
            var list;
            //set the entire grid to edit mode
            var $this = $(this), ids = $this.jqGrid('getDataIDs'), i, l = ids.length;
            for (i = 0; i < l; i++) {
                $this.jqGrid('editRow', ids[i], true);

                shipperId = '#' + (i + 1).toString() + '_shipper_id';
                locationId = '#' + (i + 1).toString() + '_location_id';
                contactId = '#' + (i + 1).toString() + '_location_contact_id';
                refLocation = '#' + (i + 1).toString() + '_shipper_location';
                refContact = '#' + (i + 1).toString() + '_shipper_location_contact';

                //get and populate location list data
                var sLocationSelect = $.getShipperLocations($(shipperId).val(), refLocation, $(locationId).val());  //make sure to use the val() to get the actual value of the field
                //$(refLocation).html(sLocationSelect);
                //$(refLocation).val($(locationId).val());  //make sure to use the val() to get the actual value.  set the value of the location id to the dropdown field as the option index

                //get and populate contact list data
                var sContactSelect = $.getShipperLocationContacts($(shipperId).val(), refContact, $(locationId).val(), $(contactId).val());  //make sure to use the val() to get the actual value of the field
                //$(refContact).html(sContactSelect);
                //$(refContact).val($(contactId).val());  //make sure to use the val() to get the actual value.  set the value of the location id to the dropdown field as the option index

                /*
                var str2 = '#' + (i + 1).toString() + '_location_id';  //get the current row location id
                //alert($(str2).val());
                var str3 = '#' + (i + 1).toString() + '_shipper_location';  //set the reference to the shipper location field
                $(str3).val($(str2).val());  //assign the value of the location id as the option index.  need to dynamically create the option list of locations for the shipper first.
                */
            }
        }
    }); //eo setupShipperGrid
    jQuery("#shipperGrid").jqGrid('navGrid', "#shipperPager", {
        edit: false, add: true, del: false, search: false, refresh: false,
        addtext: "Add",
        addfunc: function () {
            var recCount = $("#shipperGrid").getGridParam("reccount");
            nextRec = recCount + 1;
            var row = { ID: nextRec, pli_book_number: newBookingNumber, shipper_id: "0", shipper_location_id: "0", shipper_location_contact_id: "0", reference_number: "", actual_pu__date: "", shipper_sequence: nextRec, shipper_location_zip: "", shipper_location_state: "" };
            jQuery("#shipperGrid").addRowData(nextRec, row);
            jQuery("#shipperGrid").jqGrid('editRow', nextRec, false);  //set to 'false' to disable the enter key from saving the row to local array and keep the row in edit mode.
        }
    });
} //eo setupShipperGrid