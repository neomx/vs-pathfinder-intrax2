﻿//data grid for commodity
function setupCommodityGrid(booking, data, data2) {
    nmfcOptions = data;
    $("#commodityGrid").jqGrid({
        url: "/IntraX2/Shipment/GetCommodityRows?booking=" + booking,
        datatype: 'json',
        mtype: 'GET',
        loadonce: true,
        colNames: ['', '', '', 'NMFC', 'Pieces', 'Weight', 'Package', 'HazMat', 'Length (in.)', 'Width (in.)', 'Height (in.)', '', '', '', '',''],
        colModel: [
            {
                name: 'act', index: 'act', width: 30, align: 'center', resizable: false, sortable: false, formatter: 'actions',
                formatoptions: {
                    editbutton: false,
                    keys: false, // we want use [Enter] key to save the row and [Esc] to cancel editing.
                    onEdit: function (rowid) {
                        alert("in onEdit: rowid=" + rowid + "\nWe don't need return anything");
                    },
                    onSuccess: function (jqXHR) {
                        // the function will be used as "succesfunc" parameter of editRow function
                        // (see http://www.trirand.com/jqgridwiki/doku.php?id=wiki:inline_editing#editrow)
                        alert("in onSuccess used only for remote editing:" +
                                    "\nresponseText=" + jqXHR.responseText +
                                    "\n\nWe can verify the server response and return false in case of" +
                                    " error response. return true confirm that the response is successful");
                        // we can verify the server response and interpret it do as an error
                        // in the case we should return false. In the case onError will be called
                        return true;
                    },
                    onError: function (rowid, jqXHR, textStatus) {
                        // the function will be used as "errorfunc" parameter of editRow function
                        // (see http://www.trirand.com/jqgridwiki/doku.php?id=wiki:inline_editing#editrow)
                        // and saveRow function
                        // (see http://www.trirand.com/jqgridwiki/doku.php?id=wiki:inline_editing#saverow)
                        alert("in onError used only for remote editing:" +
                                    "\nresponseText=" + jqXHR.responseText +
                                    "\nstatus=" + jqXHR.status +
                                    "\nstatusText" + jqXHR.statusText +
                                    "\n\nWe don't need return anything");
                    },
                    afterSave: function (rowid) {
                        alert("in afterSave (Submit): rowid=" + rowid + "\nWe don't need return anything");
                    },
                    afterRestore: function (rowid) {
                        alert("in afterRestore (Cancel): rowid=" + rowid + "\nWe don't need return anything");
                    },
                    delOptions: consigneeDelOptions
                }
            },
            { key: false, name: 'pli_book_number', index: 'pli_book_number', editable: true, hidden: true, editoptions: { readonly: 'readonly' } },
            { key: false, name: 'item_seq_number', index: 'item_seq_number', editable: true, hidden: true, editoptions: { readonly: 'readonly' } },
            {
                key: false, name: 'item_value', index: 'item_value', width: 242, resizable: false, editable: true, editrules: { required: false }, edittype: 'select', formatter: 'select', editoptions: {
                    value: data,
                    dataInit: function (el) {
                        $(el).width(355);  //set the width of the field in the form edit
                    },  //eo dataInit
                    defaultValue: '0',
                    dataEvents: [
                        {
                            type: 'change',
                            fn: function (e) {
                                $.getJSON("/IntraX2/Shipment/GetNMFCItemDetail?value=" + $(e.target).val(), null, function (data) {
                                    //popuplate nmfc hidden fields
                                    var row = $(e.target).closest('tr.jqgrow');
                                    var rowId = row.attr('ID');
                                    $("#" + rowId + "_description").val(data[0].nmfc_description);
                                    $("#" + rowId + "_nmfc").val(data[0].nmfc_item);
                                    $("#" + rowId + "_sub").val(data[0].nmfc_sub);
                                    $("#" + rowId + "_class").val(data[0].nmfc_class);
                                    $("#" + rowId + "_nmfc_id").val(data[0].nmfc_id);

                                }); //eo getJSON
                            }
                        }
                    ]
                }
            },
            {
                key: false, name: 'pieces', index: 'pieces', width: 40, resizable: false, editable: true, editoptions: {
                    defaultValue: 0,
                    dataInit: function (el) {
                        $(el).width(55);  //set the width of the field in the form edit
                    }, //eo dataInit
                }
            },
            {
                key: false, name: 'weight', index: 'weight', width: 45, resizable: false, editable: true, editoptions: {
                    defaultValue: 0,
                    dataInit: function (el) {
                        $(el).width(62);  //set the width of the field in the form edit
                    }, //eo dataInit
                }
            },
            {
                key: false, name: 'packaging', index: 'packaging', width: 75, resizable: false, editable: true, editrules: { required: false }, edittype: 'select', editoptions: {
                    value: data2,
                    dataInit: function (el) {
                        $(el).width(110);  //set the width of the field in the form edit
                    }, //eo dataInit
                }
            },
            {
                key: false, name: 'hazmat_notes', index: 'hazmat_notes', width: 90, resizable: false, editable: true, editoptions: {
                    dataInit: function (el) {
                        $(el).width(130);  //set the width of the field in the form edit
                    }, //eo dataInit
                }
            },
            {
                key: false, name: 'length', index: 'length', width: 45, resizable: false, editable: true, editoptions: {
                    defaultValue: 0,
                    dataInit: function (el) {
                        $(el).width(63);  //set the width of the field in the form edit
                    }, //eo dataInit
                }
            },
            {
                key: false, name: 'width', index: 'width', width: 45, resizable: false, editable: true, editoptions: {
                    defaultValue: 0,
                    dataInit: function (el) {
                        $(el).width(63);  //set the width of the field in the form edit
                    }, //eo dataInit
                }
            },
            {
                key: false, name: 'height', index: 'height', width: 45, resizable: false, editable: true, editoptions: {
                    dataInit: function (el) {
                        $(el).width(63);  //set the width of the field in the form edit
                    }, //eo dataInit
                }
            },
            { key: false, name: 'description', index: 'description', width: 50, editable: true, hidden: true },
            { key: false, name: 'nmfc', index: 'nmfc', width: 30, editable: true, hidden: true },
            { key: false, name: 'sub', index: 'sub', width: 30, editable: true, hidden: true },
            { key: false, name: 'class', index: 'class', width: 30, editable: true, hidden: true },
            { key: false, name: 'nmfc_id', index: 'nmfc_id', width: 30, editable: true, hidden: true }
        ],
        pager: jQuery('#commodityPager'),
        //rowNum: 10,
        //rowList: [10, 20, 30, 40],
        height: '100%',
        viewrecords: true,
        pgbuttons: false, // disable page control like next, back button
        pgtext: null, // disable pager text like 'Page 0 of 10'
        emptyrecords: 'No records to display',
        jsonReader: {
            root: "rows",
            page: "page",
            total: "total",
            records: "records",
            repeatitems: false,
            Id: "0"
        },
        autowidth: false,
        width: 1018,
        multiselect: false,
        gridComplete: function () {
            var nmfcId;
            //set the entire grid to edit mode
            var $this = $(this), ids = $this.jqGrid('getDataIDs'), i, l = ids.length;
            for (i = 0; i < l; i++) {
                $this.jqGrid('editRow', ids[i], true);

                nmfcId = '#' + (i + 1).toString() + '_nmfc_id';
                refItem = '#' + (i + 1).toString() + '_item_value';
                $(refItem).val($(nmfcId).val());
            } //eo for
        }
    });
} //eo setupCommodityGrid