﻿/*This is a shared functions and scripts repository*/

/*This function displays the calendar icon next to the requested_pu_date field*/
$(function () {
    $("#requested_pu_date").datepicker({ dateFormat: 'mm/dd/yy', showOn: 'button', buttonText: "calendar", buttonImageOnly: true, buttonImage: '@Url.Content("~/Content/styles/img/icon-calendar.png")' });
});
