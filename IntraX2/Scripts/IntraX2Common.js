﻿/*Create a namespace*/
var IntraXCommon = {}

/*This function formats the JSON date string to the normal date format*/
IntraXCommon.formatJSONDate = function (value) {
    if (value == '') {
        return '';
    }
    else {
        var d = new Date(parseInt(value.substr(6)));
        return (d.toString("MM/dd/yyyy"));  //toString() method is from the Datejs library.
    };
};
