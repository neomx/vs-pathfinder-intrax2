﻿var sRole = '';
var sRoleOptions = '0:--Select--;';
var sCarrier = '';
var sCarrierOptions = ':--Select--;';

$(document).ready(function () {
    //initialize session variables
    sessionStorage.setItem("carrierID", '');

    //get the carrier roles to populate the carrier role dropdown
    $.getJSON("/IntraX2/Shipment/GetAllCarrierRoles", null, function (data) {
        var num_rec = data.length - 1;
        for (var i = 0; i <= num_rec; i++) {
            sRole = data[i].carrier_role_description.replace(/;/g, ',');  //replaces semicolons in each description with commas to resolve undefined text in dropdown list
            if (i < num_rec) {
                sRoleOptions = sRoleOptions + data[i].carrier_role_id + ":" + sRole + ";";
            }
            else {
                sRoleOptions = sRoleOptions + data[i].carrier_role_id + ":" + sRole;
            }
        }
        sessionStorage.setItem("carrierRoleList", sRoleOptions);
    }); //eo getJSON

    //get the carriers to populate the carrier dropdown
    $.getJSON("/IntraX2/Shipment/GetAllCarriers", null, function (data) {
        var num_rec = data.length - 1;
        for (var i = 0; i <= num_rec; i++) {
            sCarrier = data[i].CarrierName.replace(/;/g, ',');  //replaces semicolons in each name with commas to resolve undefined text in dropdown list
            if (i < num_rec) {
                sCarrierOptions = sCarrierOptions + data[i].CarrierID + ":" + sCarrier + ";";
            }
            else {
                sCarrierOptions = sCarrierOptions + data[i].CarrierID + ":" + sCarrier;
            }
        }
        sessionStorage.setItem("carrierList", sCarrierOptions);
    }); //eo getJSON

    //call setup CarrierGrid function
    setupCarrierGrid();

}); //eo document.ready

function initDateSearch(elem) {
    setTimeout(function () {
        $(elem).datepicker({
            dateFormat: 'dd-M-yy',
            autoSize: true,
            changeYear: true,
            changeMonth: true,
            showWeek: true,
            showButtonPanel: true
        });
    }, 100);
};

function setupCarrierGrid(data) {
    $("#listCarriers").jqGrid({
        datatype: 'local',
        mtype: 'POST',
        colNames: ['', 'Role', 'Carrier', 'Pro #', 'Charges', 'Transit Days', 'Invoiced Date', 'Date/Time Dispatched', 'Notes', 'RoleID', 'CarrierID'],
        colModel: [
            {
                name: 'act', index: 'act', width: 15, align: 'center', sortable: false, formatter: 'actions',
                formatoptions: {
                    editbutton: false,
                    keys: true, // we want use [Enter] key to save the row and [Esc] to cancel editing.
                    delOptions: {
                        // because I use "local" data I don't want to send the changes
                        // to the server so I use "processing:true" setting and delete
                        // the row manually in onclickSubmit
                        onclickSubmit: function (options, rowid) {
                            var grid_id = $.jgrid.jqID($("#listCarriers")[0].id),
                                grid_p = $("#listCarriers")[0].p,
                                newPage = grid_p.page;

                            // reset the value of processing option which could be modified
                            options.processing = true;

                            // delete the row
                            $("#listCarriers").delRowData(rowid);
                            $.jgrid.hideModal("#delmod" + grid_id,
                                              {
                                                  gb: "#gbox_" + grid_id,
                                                  jqm: options.jqModal, onClose: options.onClose
                                              });

                            if (grid_p.lastpage > 1) {// on the multipage grid reload the grid
                                if (grid_p.reccount === 0 && newPage === grid_p.lastpage) {
                                    // if after deliting there are no rows on the current page
                                    // which is the last page of the grid
                                    newPage--; // go to the previous page
                                }
                                // reload grid to make the row from the next page visable.
                                grid.trigger("reloadGrid", [{ page: newPage }]);
                            }
                            return true;
                        },
                        processing: true
                    } //eo delOptions
                } //eo formatoptions
            }, //eo column act

            { name: 'role', index: 'role', width: 38, align: 'left', editable: true, formatter: 'select', edittype: 'select', editoptions: { value: sessionStorage.getItem("carrierRoleList"),
                    dataInit: function (elem) {
                        $(elem).width(100);  // set the width of the dropdown
                    }, //eo dataInit
                    dataEvents: [
                        {
                            type: 'change',
                            fn: function (e) {
                                //popuplate role id hidden field
                                var row = $(e.target).closest('tr.jqgrow');
                                var rowId = row.attr('id');
                                $("#" + rowId + "_roleID").val($(e.target).val());
                            } //eo function(e)
                        } //eo dataEvents curly
                    ] //eo dataEvents bracket
                } //eo editoptions
            }, //eo role column

            {
                name: 'carrier', index: 'carrier', width: 80, align: 'left', editable: true, formatter: 'select', edittype: 'select', editoptions: { value: sessionStorage.getItem("carrierList"),
                    dataInit: function (elem) {
                        $(elem).width(221);  // set the width of the dropdown
                    }, //eo dataInit
                    dataEvents: [
                        {
                            type: 'change',
                            fn: function (e) {
                                //popuplate carrier id hidden field
                                var row = $(e.target).closest('tr.jqgrow');
                                var rowId = row.attr('id');
                                $("#" + rowId + "_carrierID").val($(e.target).val());
                            } //eo function(e)
                        } //eo dataEvents curly
                    ] //eo dataEvents bracket
                } //eo editoptions
            }, //eo carrier column

            { name: 'pro_number', index: 'pro_number', editable: true, width: 40 },
            { name: 'carrier_charges', index: 'carrier_charges', editable: true, width: 25, align: 'left' },
            { name: 'transit_days', index: 'transit_days', editable: true, width: 25, align: 'left' },
            { name: 'invoiced_date', index: 'invoiced_date', width: 30, align: 'center', sorttype: 'date', editable: true,
                editoptions: { dataInit: function (element) { $(element).datepicker({ dateFormat: 'mm/dd/yy' }) } },
                searchoptions: { sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge'], dataInit: initDateSearch }
            },
            { name: 'dispatched_notes', index: 'dispatched_notes', editable: true, width: 50 },
            { name: 'notes', index: 'notes', editable: true, width: 100 },
            { name: 'roleID', index: 'roleID', width: 20, editable: true, hidden: true },
            { name: 'carrierID', index: 'carrierID', width: 20, editable: true, hidden: true }
        ], //eo colModel

        rowNum: 10,
        //rowList: [5, 10, 20],
        pager: '#pagerCarriers',
        gridview: true,
        rownumbers: true,
        //multiselect: true,
        autoencode: true,
        width: 1210,
        ignoreCase: true,
        sortable: true,
        viewrecords: true,
        //toppager: true, //displays the viewrecords info at the top of the grid
        sortname: 'invdate',
        pgbuttons: false,
        pgtext: null, //disable pager text like 'Page 0 of 10'
        sortorder: 'desc',
        height: '100%',
        //caption: 'Shipment Commodity',
        editurl: 'clientArray',
        gridComplete: function () {
            var objRows = $("#listCarriers tr");
            var objHeader = $("#listCarriers .jqgfirstrow td");

            if (objRows.length > 1) {
                var objFirstRowColumns = $(objRows[1]).children("td");
                for (i = 0; i < objFirstRowColumns.length; i++) {
                    $(objFirstRowColumns[i]).css("width", $(objHeader[i]).css("width"));
                }
            }
        }
    }); //eo jqGrid

    jQuery("#listCarriers").jqGrid('navGrid', '#pagerCarriers', {
        cloneToTop: true,
        edit: false,
        add: false,
        view: false,
        del: false,
        refresh: false,
        search: false,
        viewtext: "View",
        closeOnEscape: true,
        edittext: "Edit",
        refreshtext: "Refresh",
        deltext: "Delete",
        searchtext: "Search",
        addtext: "Add",
        /*
        addfunc: function () {
            var recCount = $("#listCommodity").getGridParam("reccount");
            nextRec = recCount + 1;
            var row = { ID: nextRec, Description: "", Pallet: "", Pieces: "", Weight: "", Package: "", Length: "", Width: "", Height: "", Density: "" };
            jQuery("#listCommodity").addRowData(nextRec, row);
            jQuery("#listCommodity").jqGrid('editRow', nextRec, true);
            // document.getElementById("grid_reccount").value = nextRec;
        }*/ //eo addfunc
    },
        {}, {}, {}, { multipleSearch: true }
    );

    jQuery("#listCarriers").jqGrid('inlineNav', '#pagerCarriers', {
        edit: true,
        add: true,
        del: false,
        view: false,
        search: false,
        refresh: false,
        cancel: true,
        save: true,
        edittext: "Edit",
        addtext: "Add",
        savetext: "Save",
        canceltext: "Cancel",
        addParams: { position: "afterSelected" }
    });
}; //eo function setupCarrierGrid