﻿var totalVolume = 0;
var lastSel;
var oldAddRowData = $.fn.jqGrid.addRowData;
var sTypes = '0:--Select Type--;';
var aCommodity = new Array();

//define events for grid row edit and save
var myCommodityEditOptions = {
    keys: true,
    oneditfunc: function (rowid) {
        //alert("row with rowid=" + rowid + " is editing.");
    },

    aftersavefunc: function (rowid, response, options) {
        //alert($("#listCommodity").getCell(rowid, "weight"));
        aCommodity[0] = $("#listCommodity").getCell(rowid, "weight");
        Application["commodity"] = "75";
    }
};

$(document).ready(function () {
    //get the package types to populate the shipper dropdown
    var sPackage = '';
    $.getJSON("/IntraX2/Shipment/GetPackageList", null, function (data) {
        var num_rec = data.length - 1;
        for (var i = 0; i <= num_rec; i++) {
            sPackage = data[i].package_description.replace(/;/g, ',');  //replaces semicolons in each description with commas to resolve undefined text in dropdown list
            if (i < num_rec) {
                sTypes = sTypes + data[i].package_id + ":" + sPackage + ";";
            }
            else {
                sTypes = sTypes + data[i].package_id + ":" + sPackage;
            }
        }
        //sessionStorage.setItem("packageTypeList", sTypes);
        setupCommodityGrid(sTypes);
    }); //eo getJSON

    //display the Commodity grid
    //setupCommodityGrid();

    initDateEdit = function (elem) {
        setTimeout(function () {
            $(elem).datepicker({
                dateFormat: 'dd-M-yy',
                autoSize: true,
                //showOn: 'button', // it dosn't work in searching dialog
                changeYear: true,
                changeMonth: true,
                showButtonPanel: true,
                showWeek: true
            });
        }, 100);
    },

    initDateSearch = function (elem) {
        setTimeout(function () {
            $(elem).datepicker({
                dateFormat: 'dd-M-yy',
                autoSize: true,
                changeYear: true,
                changeMonth: true,
                showWeek: true,
                showButtonPanel: true
            });
        }, 100);
    },

    numberTemplate = {
        formatter: 'number', align: 'right', sorttype: 'number', editable: true,
        searchoptions: { sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge', 'nu', 'nn', 'in', 'ni'] }
    },
   
    $.jgrid.extend({
        addRowData: function (rowid, rdata, pos, src) {
            if (pos === 'afterSelected' || pos === 'beforeSelected') {
                if (typeof src === 'undefined' && this[0].p.selrow !== null) {
                    src = this[0].p.selrow;
                    pos = (pos === "afterSelected") ? 'after' : 'before';
                } else {
                    pos = (pos === "afterSelected") ? 'last' : 'first';
                }
            }
            return oldAddRowData.call(this, rowid, rdata, pos, src);
        }
    });

    //define functions to support calculateTotals function
    grid = jQuery("#listCommodity"),
    getColumnIndexByName = function (grid, columnName) {
        var cm = grid.jqGrid('getGridParam', 'colModel');
        for (var i = 0, l = cm.length; i < l; i++) {
            if (cm[i].name === columnName) {
                return i; // return the index
            }
        }
        return -1;
    },

    getTextFromCell = function (cellNode) {
        return cellNode.childNodes[0].nodeName === "INPUT" ?
                       cellNode.childNodes[0].value :
                       cellNode.textContent || cellNode.innerText;
    },

    calculateTotals = function () {
        totalPallets = 0;
        totalPieces = 0;
        totalWeight = 0;
        totalLength = 0;
        totalWidth = 0;
        totalHeight = 0;
        
        i = getColumnIndexByName(grid, 'pallet'); // nth-child need 1-based index so we use (i+1) below
        $("tbody > tr.jqgrow > td:nth-child(" + (i + 1) + ")", grid[0]).each(function () {
            totalPallets += Number(getTextFromCell(this));
        });
        //display total pallets on form
        document.getElementById("total_pallets").innerHTML = 'Pallet Count: ' + totalPallets;
        //document.getElementById("totalPieces").value = totalPieces;

        i = getColumnIndexByName(grid, 'pieces'); // nth-child need 1-based index so we use (i+1) below
        $("tbody > tr.jqgrow > td:nth-child(" + (i + 1) + ")", grid[0]).each(function () {
            totalPieces += Number(getTextFromCell(this));
        });
        //display total pieces on form
        document.getElementById("total_pieces").innerHTML = 'Total Pieces: ' + totalPieces;
        //document.getElementById("totalPieces").value = totalPieces;

        i = getColumnIndexByName(grid, 'weight'); // nth-child need 1-based index so we use (i+1) below
        $("tbody > tr.jqgrow > td:nth-child(" + (i + 1) + ")", grid[0]).each(function () {
            totalWeight += Number(getTextFromCell(this));
        });
        //display total weight on form
        document.getElementById("total_weight").innerHTML = 'Total Weight: ' + totalWeight;
        //document.getElementById("totalWeight").value = totalWeight;
    }; //eo calculateTotals

    calculateDensity = function () {
        var l = 0;
        var w = 0;
        var h = 0;
        var pallet = 0;
        var weight = 0;
        var density = 0.000;
        var result = 0.000;

        i = getColumnIndexByName(grid, 'pallet'); // nth-child need 1-based index so we use (i+1) below
        $("tbody > tr.jqgrow > td:nth-child(" + (i + 1) + ")", grid[0]).each(function () {
            pallet += Number(getTextFromCell(this));
        });
        
        i = getColumnIndexByName(grid, 'length'); // nth-child need 1-based index so we use (i+1) below
        $("tbody > tr.jqgrow > td:nth-child(" + (i + 1) + ")", grid[0]).each(function () {
            l = Number(getTextFromCell(this));
        });

        i = getColumnIndexByName(grid, 'width'); // nth-child need 1-based index so we use (i+1) below
        $("tbody > tr.jqgrow > td:nth-child(" + (i + 1) + ")", grid[0]).each(function () {
            w = Number(getTextFromCell(this));
        });

        i = getColumnIndexByName(grid, 'height'); // nth-child need 1-based index so we use (i+1) below
        $("tbody > tr.jqgrow > td:nth-child(" + (i + 1) + ")", grid[0]).each(function () {
            h = Number(getTextFromCell(this));
        });

        i = getColumnIndexByName(grid, 'weight'); // nth-child need 1-based index so we use (i+1) below
        $("tbody > tr.jqgrow > td:nth-child(" + (i + 1) + ")", grid[0]).each(function () {
            weight = Number(getTextFromCell(this));
        });

        if (weight > 0) {
            if (l > 0) {
                if (w > 0) {
                    if (h > 0) {
                        if (pallet > 0) {
                            density = (weight / (((l * w * h) / 1728) * pallet));
                            result = density.toFixed(3);
                        }
                    }
                }
            }
        };
        return result;
    }; //eo calculateDensity

    calculateVolume = function () {
        var length = 0;
        var width = 0;
        var height = 0;
        totalVolume = 0;

        i = getColumnIndexByName(grid, 'length'); // nth-child need 1-based index so we use (i+1) below
        $("tbody > tr.jqgrow > td:nth-child(" + (i + 1) + ")", grid[0]).each(function () {
            length += Number(getTextFromCell(this));
        });

        i = getColumnIndexByName(grid, 'width'); // nth-child need 1-based index so we use (i+1) below
        $("tbody > tr.jqgrow > td:nth-child(" + (i + 1) + ")", grid[0]).each(function () {
            width += Number(getTextFromCell(this));
        });

        i = getColumnIndexByName(grid, 'height'); // nth-child need 1-based index so we use (i+1) below
        $("tbody > tr.jqgrow > td:nth-child(" + (i + 1) + ")", grid[0]).each(function () {
            height += Number(getTextFromCell(this));
        });

        if (length > 0) {
            if (width > 0) {
                if (height > 0) {
                    totalVolume = (length * width * height)
                    document.getElementById("total_volume").innerHTML = 'Total Volume (cubic inches): ' + totalVolume.toFixed(2);
                }
            }
        };
    }; //eo calculateVolume
});  //eo ready function

function setupCommodityGrid(data) {
    $("#listCommodity").jqGrid({
        datatype: 'local',
        mtype: 'POST',
        colNames: [/*'Inv No', */'','Description', 'Pallet', 'Pieces', 'Weight', 'Package', 'HazMat', 'Length (in)', 'Width (in)', 'Height (in)', 'Density', 'NMFCID'],
        colModel: [
             //{ name: 'id', index: 'id', width: 70, align: 'center', sorttype: 'int', searchoptions: { sopt: ['eq', 'ne']} },        
             {
                 name: 'act', index: 'act', width: 20, align: 'center', sortable: false, formatter: 'actions',
                 formatoptions: {
                     editbutton: false,
                     keys: true, // we want use [Enter] key to save the row and [Esc] to cancel editing.
                     delOptions: {
                         // because I use "local" data I don't want to send the changes
                         // to the server so I use "processing:true" setting and delete
                         // the row manually in onclickSubmit
                         onclickSubmit: function (options, rowid) {
                             var grid_id = $.jgrid.jqID($("#listCommodity")[0].id),
                                 grid_p = $("#listCommodity")[0].p,
                                 newPage = grid_p.page;

                             // reset the value of processing option which could be modified
                             options.processing = true;

                             // delete the row
                             $("#listCommodity").delRowData(rowid);
                             $.jgrid.hideModal("#delmod" + grid_id,
                                               {
                                                   gb: "#gbox_" + grid_id,
                                                   jqm: options.jqModal, onClose: options.onClose
                                               });

                             //recalculate totals
                             calculateTotals();

                             if (grid_p.lastpage > 1) {// on the multipage grid reload the grid
                                 if (grid_p.reccount === 0 && newPage === grid_p.lastpage) {
                                     // if after deliting there are no rows on the current page
                                     // which is the last page of the grid
                                     newPage--; // go to the previous page
                                 }
                                 // reload grid to make the row from the next page visable.
                                 grid.trigger("reloadGrid", [{ page: newPage }]);
                             }
                             return true;
                         },
                         processing: true
                     } //eo delOptions
                 } //eo formatoptions
             },

            { name: 'Description', index: 'Description', width: 110, align: 'left', editable: true, formatter: 'select', edittype: 'select', editoptions: {
                dataInit: function (elem) {
                    $(elem).width(250);  // set the width of the dropdown
                }, //eo dataInit
                dataEvents: [
                    {
                        type: 'change',
                        fn: function (e) {
                            //popuplate nfmc id hidden field
                            var row = $(e.target).closest('tr.jqgrow');
                            var rowId = row.attr('id');
                            $("#" + rowId + "_nmfcID").val($(e.target).val());

                        } //eo function(e)
                    } //eo dataEvents curly
                ] //eo dataEvents bracket
                } //eo editoptions
            }, //eo description column
            { name: 'pallet', index: 'pallet', editable: true, width: 30, editrules: { required: true, integer: true },
                editoptions: {
                    dataInit: function (el) {
                        $(el).mask("9?999");
                    }, //eo dataInit
                    dataEvents: [
                        {
                            type: 'change',
                            fn: function (e) {
                                calculateTotals();

                                //calculate density
                                var densityVal = calculateDensity();

                                //popuplate density field
                                var row = $(e.target).closest('tr.jqgrow');
                                var rowId = row.attr('id');
                                $("#" + rowId + "_density").val(densityVal);
                            } //eo function
                        } //eo dataEvents curly
                    ] //eo dataEvents bracket
                } //eo editoptions
            }, //eo column name
            { name: 'pieces', index: 'pieces', editable: true, width: 30, editrules: { required: true, integer: true },
                editoptions: {
                    dataInit: function (el) {
                        $(el).mask("9?99");
                    }, //eo dataInit
                    dataEvents: [
                        {
                            type: 'change',
                            fn: function (e) {
                                calculateTotals();
                            } //eo function
                        } //eo dataEvents curly
                    ] //eo dataEvents bracket
                } //eo editoptions
            }, //eo column name
            { name: 'weight', index: 'weight', editable: true, width: 30, editrules: { required: true, integer: true },
                editoptions: {
                    dataInit: function (el) {
                        $(el).mask("9?99999");
                    }, //eo dataInint
                    dataEvents: [
                        {
                            type: 'change',
                            fn: function (e) {
                                calculateTotals();

                                //calculate density
                                var densityVal = calculateDensity();

                                //popuplate density field
                                var row = $(e.target).closest('tr.jqgrow');
                                var rowId = row.attr('id');
                                $("#" + rowId + "_density").val(densityVal);
                            } //eo function
                        } //eo dataEvents curly
                    ] //eo dataEvents bracket
                } //eo editoptions
            }, //eo column name
            {
                name: 'package', index: 'package', width: 55, align: 'left', editable: true, formatter: 'select', edittype: 'select', editoptions: { value: data,
                    dataInit: function (elem) {
                        $(elem).width(120);  // set the width of the dropdown
                    },
                }
            }, //eo column
            { name: 'hazmat', index: 'hazmat', editable: true, width: 20, align: 'center', formatter: 'checkbox', edittype: 'checkbox', editoptions : { value: "true:false" } },
            {
                name: 'length', index: 'length', editable: true, width: 30, editoptions: {
                    dataEvents: [
                        {
                            type: 'change',
                            fn: function (e) {
                                var densityVal = calculateDensity();

                                //calculate total volume
                                calculateVolume();

                                //popuplate density field
                                var row = $(e.target).closest('tr.jqgrow');
                                var rowId = row.attr('id');
                                $("#" + rowId + "_density").val(densityVal);

                            } //eo function
                        } //eo dataEvents curly
                    ] //eo dataEvents bracket
                } //eo editoptions
            }, //eo column name
            {
                name: 'width', index: 'width', editable: true, width: 30, editoptions: {
                    dataEvents: [
                        {
                            type: 'change',
                            fn: function (e) {
                                var densityVal = calculateDensity();

                                //calculate total volume
                                calculateVolume();

                                //popuplate density field
                                var row = $(e.target).closest('tr.jqgrow');
                                var rowId = row.attr('id');
                                $("#" + rowId + "_density").val(densityVal);

                            } //eo function
                        } //eo dataEvents curly
                    ] //eo dataEvents bracket
                } //eo editoptions
            }, //eo column name
            {
                name: 'height', index: 'height', editable: true, width: 30, editoptions: {
                    dataEvents: [
                        {
                            type: 'change',
                            fn: function (e) {
                                var densityVal = calculateDensity();

                                //calculate total volume
                                calculateVolume();

                                //popuplate density field
                                var row = $(e.target).closest('tr.jqgrow');
                                var rowId = row.attr('id');
                                $("#" + rowId + "_density").val(densityVal);

                            } //eo function
                        } //eo dataEvents curly
                    ] //eo dataEvents bracket
                } //eo editoptions
            }, //eo colunn name
            { name: 'density', index: 'density', editable: true, width: 30, editoptions: { readonly: 'readonly' } },
            { name: 'nmfcID', index: 'nmfcID', width: 20, editable: true, hidden: true }
        ],
        rowNum: 10,
        //rowList: [5, 10, 20],
        pager: '#pagerCommodity',
        gridview: true,
        rownumbers: true,
        //multiselect: true,
        autoencode: true,
        //width: 975,
        width: 1040,
        ignoreCase: true,
        sortable: true,
        viewrecords: true,
        //toppager: true, //displays the viewrecords info at the top of the grid
        sortname: 'invdate',
        pgbuttons: false,
        pgtext: null, //disable pager text like 'Page 0 of 10'
        sortorder: 'desc',
        height: '100%',
        //caption: 'Shipment Commodity',
        editurl: 'clientArray',
        afterInsertRow: function (rowid, rowdata, rowelem) {
            $("#listCommodity").jqGrid('setColProp', "Description", { editoptions: { value: sessionStorage.getItem("shipperNMFCList") } });
        },
        /*
        beforeSelectRow: function (rowid) {
            if (rowid !== lastSel) {
                $(this).jqGrid('restoreRow', lastSel);
                lastSel = rowid;
            }
            return true;
        },
        */
        /*
        ondblClickRow: function (rowid, iRow, iCol, e) {
            $(this).jqGrid('editRow', rowid, true, function () {
                $("input, select", e.target).focus();
            });
            return;
        },
        */
        gridComplete: function () {
            var objRows = $("#listCommodity tr");
            var objHeader = $("#listCommodity .jqgfirstrow td");

            if (objRows.length > 1) {
                var objFirstRowColumns = $(objRows[1]).children("td");
                for (i = 0; i < objFirstRowColumns.length; i++) {
                    $(objFirstRowColumns[i]).css("width", $(objHeader[i]).css("width"));
                }
            }
        }
    }); //eo listCommodity

    jQuery("#listCommodity").jqGrid('navGrid', '#pagerCommodity', {
        cloneToTop: true,
        edit: false,
        add: false,
        view: false,
        del: false,
        refresh: false,
        search: false,
        viewtext: "View",
        closeOnEscape: true,
        edittext: "Edit",
        refreshtext: "Refresh",
        deltext: "Delete",
        searchtext: "Search",
        addtext: "Add",
        /*
        addfunc: function () {
            var recCount = $("#listCommodity").getGridParam("reccount");
            nextRec = recCount + 1;
            var row = { ID: nextRec, Description: "", Pallet: "", Pieces: "", Weight: "", Package: "", Length: "", Width: "", Height: "", Density: "" };
            jQuery("#listCommodity").addRowData(nextRec, row);
            jQuery("#listCommodity").jqGrid('editRow', nextRec, true);
            // document.getElementById("grid_reccount").value = nextRec;
        }*/ //eo addfunc
        },
        {}, {}, {}, { multipleSearch: true }
    );

    jQuery("#listCommodity").jqGrid('inlineNav', '#pagerCommodity', {
        edit: true,
        add: true,
        del: false,
        view: false,
        search: false,
        refresh: false,
        cancel: true,
        save: true,
        edittext: "Edit",
        addtext: "Add",
        savetext: "Save",
        canceltext: "Cancel",
        addParams: {
            position: "afterSelected",
            addRowParams: myCommodityEditOptions
        },
        addedrow: "last",
        editParams: myCommodityEditOptions
    });
}; //eo function setupCommodityGrid