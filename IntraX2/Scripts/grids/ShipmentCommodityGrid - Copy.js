﻿$(document).ready(function () {
    var mydata = [
                    { id: "1", description: "TN", consignee_name: "Consignee One", consignee_location: "Location 1", consignee_contact: "Contact 1", requested_delv_date: "2007-10-01", requested_delv_time: "before 2pm", delv_eta: "2007-10-01", actual_delv_date: "2007-10-01", transit_days: '2', bill_to: true, bill_amount: '$500.00', customer: "Customer 1" },
                    //{ id: "2", descripiton: "", consignee_name: "Consignee Two", consignee_location: "Location 2", consignee_contact: "Contact 2", requested_delv_date: "2007-10-01", requested_delv_time: "before 2pm", delv_eta: "2007-10-01", actual_delv_date: "2007-10-01", transit_days: '3', bill_to: false, bill_amount: '$350.00', customer: "Customer 2" }
    ],
                $grid = $("#gridCommodity"),

                initDateEdit = function (elem) {
                    setTimeout(function () {
                        $(elem).datepicker({
                            dateFormat: 'dd-M-yy',
                            autoSize: true,
                            //showOn: 'button', // it dosn't work in searching dialog
                            changeYear: true,
                            changeMonth: true,
                            showButtonPanel: true,
                            showWeek: true
                        });
                    }, 100);
                },

                initDateSearch = function (elem) {
                    setTimeout(function () {
                        $(elem).datepicker({
                            dateFormat: 'dd-M-yy',
                            autoSize: true,
                            changeYear: true,
                            changeMonth: true,
                            showWeek: true,
                            showButtonPanel: true
                        });
                    }, 100);
                },
                numberTemplate = {
                    formatter: 'number', align: 'right', sorttype: 'number', editable: true,
                    searchoptions: { sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge', 'nu', 'nn', 'in', 'ni'] }
                },
                lastSel,
                oldAddRowData = $.fn.jqGrid.addRowData;

    $.jgrid.extend({
        addRowData: function (rowid, rdata, pos, src) {
            if (pos === 'afterSelected' || pos === 'beforeSelected') {
                if (typeof src === 'undefined' && this[0].p.selrow !== null) {
                    src = this[0].p.selrow;
                    pos = (pos === "afterSelected") ? 'after' : 'before';
                } else {
                    pos = (pos === "afterSelected") ? 'last' : 'first';
                }
            }
            return oldAddRowData.call(this, rowid, rdata, pos, src);
        }
    });

    $grid.jqGrid({
        datatype: 'local',
        data: mydata,
        colNames: [/*'Inv No', */'Description', 'Name', 'Location', 'Contact', 'Reqst Delv Date', 'Reqst Delv Time', 'Delv ETA', 'Actual Delv Date', 'Days', 'Bill To', 'Amount', 'Customer'],
        colModel: [
            //{ name: 'id', index: 'id', width: 70, align: 'center', sorttype: 'int', searchoptions: { sopt: ['eq', 'ne']} },
            {
                name: 'description', index: 'description', width: 75, align: 'center', editable: true, formatter: 'select',
                edittype: 'select', editoptions: { value: 'FE:FedEx;TN:TNT;IN:Intim', defaultValue: 'IN' },
                stype: 'select', searchoptions: { sopt: ['eq', 'ne'], value: ':Any;FE:FedEx;TN:TNT;IN:IN' }
            },
            //{ name: 'description', index: 'description', width: 50, editable: true, edittype: 'select', editoptions: { value: ":;nmfc descripiton 1:nmfc descripiton 1;nmfc descripiton 2:nmfc descripiton 2;nmfc descripiton 3:nmfc descripiton 3" } },
            { name: 'consignee_name', index: 'consignee_name', editable: true, width: 65 },
            { name: 'consignee_location', index: 'consignee_location', editable: true, width: 40 },
            { name: 'consignee_contact', index: 'consignee_contact', editable: true, width: 40 },
            {
                name: 'requested_delv_date', index: 'requested_delv_date', width: 30, align: 'center', sorttype: 'date',
                formatter: 'date', formatoptions: { newformat: 'd-M-Y' }, editable: true, datefmt: 'd-M-Y',
                editoptions: { dataInit: initDateEdit },
                searchoptions: { sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge'], dataInit: initDateSearch }
            },
            { name: 'requested_delv_time', index: 'requested_delv_time', editable: true, width: 50 },
            {
                name: 'delv_eta', index: 'delv_eta', width: 30, align: 'center', sorttype: 'date',
                formatter: 'date', formatoptions: { newformat: 'd-M-Y' }, editable: true, datefmt: 'd-M-Y',
                editoptions: { dataInit: initDateEdit },
                searchoptions: { sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge'], dataInit: initDateSearch }
            },
            {
                name: 'actual_delv_date', index: 'actual_delv_date', width: 30, align: 'center', sorttype: 'date',
                formatter: 'date', formatoptions: { newformat: 'd-M-Y' }, editable: true, datefmt: 'd-M-Y',
                editoptions: { dataInit: initDateEdit },
                searchoptions: { sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge'], dataInit: initDateSearch }
            },
            { name: 'transit_days', index: 'transit_days', editable: true, width: 10 },
            {
                name: 'bill_to', index: 'bill_to', width: 20, align: 'center', editable: true, formatter: 'checkbox',
                edittype: 'checkbox', editoptions: { value: 'Yes:No', defaultValue: 'Yes' },
                stype: 'select', searchoptions: { sopt: ['eq', 'ne'], value: ':Any;true:Yes;false:No' }
            },
            { name: 'bill_amount', index: 'bill_amount', editable: true, width: 20 },
            { name: 'customer', index: 'customer', editable: true, width: 60 }
        ],
        rowNum: 10,
        //rowList: [5, 10, 20],
        pager: '#pagerCommodity',
        gridview: true,
        rownumbers: true,
        //multiselect: true,
        autoencode: true,
        width: 1400,
        ignoreCase: true,
        sortable: true,
        viewrecords: true,
        //toppager: true, //displays the viewrecords info at the top of the grid
        sortname: 'invdate',
        pgbuttons: false,
        pgtext: null, //disable pager text like 'Page 0 of 10'
        sortorder: 'desc',
        height: '100%',
        //caption: 'Shipment Commodity',
        editurl: 'clientArray',
        beforeSelectRow: function (rowid) {
            if (rowid !== lastSel) {
                $(this).jqGrid('restoreRow', lastSel);
                lastSel = rowid;
            }
            return true;
        },
        ondblClickRow: function (rowid, iRow, iCol, e) {
            $(this).jqGrid('editRow', rowid, true, function () {
                $("input, select", e.target).focus();
            });
            return;
        }
    });
    $grid.jqGrid('navGrid', '#pagerCommodity', {
        edit: false,
        add: false,
        del: true,
        view: false,
        search: false,
        refresh: false,
        viewtext: "View",
        closeOnEscape: true,
        edittext: "Edit",
        refreshtext: "Refresh",
        addtext: "Add",
        deltext: "Delete",
        searchtext: "Search"
    }, {}, {}, {}, { multipleSearch: true });
    //$grid.jqGrid('filterToolbar', {stringResult: true, searchOnEnter: true});
    $grid.jqGrid('inlineNav', '#pagerCommodity', {
        edittext: "Edit",
        addtext: "Add",
        savetext: "Save",
        canceltext: "Cancel",
        addParams: { position: "afterSelected" }
    });
});  //eo ready function