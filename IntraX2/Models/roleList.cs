﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace IntraX2.Models
{
    public class roleList
    {
        public int carrier_role_id { get; set; }
        public string carrier_role_description { get; set; }

        public static IEnumerable<roleList> GetRoles()
        {
            const string cacheKey = "roleNames";
            List<roleList> roles = new List<roleList>();

            List<roleList> roleNames = CacheLayer.Get<List<roleList>>(cacheKey);

            if (roleNames == null)
            {
                string connectionStr = System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ConnectionString;
                using (SqlConnection sqlConn = new SqlConnection(connectionStr))
                using (SqlCommand sqlCmd = sqlConn.CreateCommand())
                {
                    sqlCmd.CommandText = "GetCarrierRoles";  //this is the stored procedure
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlConn.Open();

                    using (SqlDataReader dr = sqlCmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            roleList newRole = new roleList();
                            newRole.carrier_role_id = Convert.ToInt16(dr.GetValue(0));
                            newRole.carrier_role_description = dr.GetString(1);

                            roles.Add(newRole);
                        }
                    }
                }
                roleNames = roles;
                CacheLayer.Add(roleNames, cacheKey);
                return roleNames;
                //return roles;
            }
            else 
            {
                return roleNames;
            }
        }
    }
}