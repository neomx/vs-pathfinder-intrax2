﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace IntraX2.Models
{
    public class statesList
    {
        public string StateCode { get; set; }
        public string StateName { get; set; }

        public static IEnumerable<statesList> GetStates()
        {
            const string cacheKey = "StatesList";
            List<statesList> stateslist = new List<statesList>();

            List<statesList> stateNames = CacheLayer.Get<List<statesList>>(cacheKey);

            if (stateNames == null)
            {
                string connectionStr = System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ConnectionString;
                using (SqlConnection sqlConn = new SqlConnection(connectionStr))
                using (SqlCommand sqlCmd = sqlConn.CreateCommand())
                {
                    sqlCmd.CommandText = "GetStatesList";  //this is the stored procedure
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlConn.Open();

                    using (SqlDataReader dr = sqlCmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            statesList newState = new statesList();
                            newState.StateCode = Convert.ToString(dr.GetValue(0));
                            newState.StateName = dr.GetString(1);

                            stateslist.Add(newState);
                        }
                    }
                }
                stateNames = stateslist;
                CacheLayer.Add(stateNames, cacheKey);
                return stateNames;
            }
            else
            {
                return stateNames;
            }
        }
    }
}