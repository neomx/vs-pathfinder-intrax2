﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace IntraX2.Models
{
    public class packageList
    {
        public string package_id { get; set; }
        public string package_description { get; set; }

        public static IEnumerable<packageList> GetPackageTypes()
        {
            const string cacheKey = "PackageNames";
            List<packageList> packages = new List<packageList>();

            List<packageList> packageNames = CacheLayer.Get<List<packageList>>(cacheKey);

            if (packageNames == null)
            {
                string connectionStr = System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ConnectionString;
                using (SqlConnection sqlConn = new SqlConnection(connectionStr))
                using (SqlCommand sqlCmd = sqlConn.CreateCommand())
                {
                    sqlCmd.CommandText = "GetPackageTypes";  //this is the stored procedure
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlConn.Open();

                    using (SqlDataReader dr = sqlCmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            packageList newPackage = new packageList();
                            newPackage.package_id = dr.GetString(0);
                            newPackage.package_description = dr.GetString(1);
                            packages.Add(newPackage);
                        }
                    }
                }
                packageNames = packages;
                CacheLayer.Add(packageNames, cacheKey);
                return packageNames;
                //return package;
            }
            else
            {
                return packageNames;
            }
        }
    }
}