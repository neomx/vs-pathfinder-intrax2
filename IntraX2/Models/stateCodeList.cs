﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace IntraX2.Models
{
    public class stateCodeList
    {
        public string StateCodeId { get; set; }
        public string StateCodeName { get; set; }

        public static IEnumerable<stateCodeList> GetStateCode()
        {
            const string cacheKey = "StateCodeList";
            List<stateCodeList> statecodelist = new List<stateCodeList>();

            List<stateCodeList> stateCodeNames = CacheLayer.Get<List<stateCodeList>>(cacheKey);

            if (stateCodeNames == null)
            {
                string connectionStr = System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ConnectionString;
                using (SqlConnection sqlConn = new SqlConnection(connectionStr))
                using (SqlCommand sqlCmd = sqlConn.CreateCommand())
                {
                    sqlCmd.CommandText = "GetStateCodeList";  //this is the stored procedure
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlConn.Open();

                    using (SqlDataReader dr = sqlCmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            stateCodeList newStateCode = new stateCodeList();
                            newStateCode.StateCodeId = Convert.ToString(dr.GetValue(0));
                            newStateCode.StateCodeName = dr.GetString(1);

                            statecodelist.Add(newStateCode);
                        }
                    }
                }
                stateCodeNames = statecodelist;
                CacheLayer.Add(stateCodeNames, cacheKey);
                return stateCodeNames;
            }
            else
            {
                return stateCodeNames;
            }
        }
    }
}