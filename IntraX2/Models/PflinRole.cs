//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IntraX2.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class PflinRole
    {
        public int PflinRoleID { get; set; }
        public string Role { get; set; }
        public string Description { get; set; }
    }
}
