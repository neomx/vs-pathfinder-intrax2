﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace IntraX2.Models
{
    public class commodityList
    {
        public string Value { get; set; }
        public string Description { get; set; }

        public static IEnumerable<commodityList> GetAllCommodities()
        {
            const string cacheKey = "CommodityNames";
            List<commodityList> commodities = new List<commodityList>();

            List<commodityList> commodityNames = CacheLayer.Get<List<commodityList>>(cacheKey);

            if (commodityNames == null)
            {
                string connectionStr = System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ConnectionString;
                using (SqlConnection sqlConn = new SqlConnection(connectionStr))
                using (SqlCommand sqlCmd = sqlConn.CreateCommand())
                {
                    sqlCmd.CommandText = "pflin_GetNMFCMasterLookupData2";  //this is the stored procedure
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.Parameters.Add("@SelectedValue", SqlDbType.VarChar).Value = "";
                    sqlCmd.Parameters.Add("@displayMode", SqlDbType.TinyInt).Value = 0;
                    sqlConn.Open();

                    using (SqlDataReader dr = sqlCmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {   
                            commodityList newCommodity = new commodityList();
                            newCommodity.Value = dr.GetString(0);
                            newCommodity.Description = dr.GetString(1);
                            commodities.Add(newCommodity);
                        }
                    }
                }
                commodityNames = commodities;
                CacheLayer.Add(commodityNames, cacheKey);
                return commodityNames;
                //return commodities;
            }
            else
            {
                return commodityNames;
            }
        }
    }
}