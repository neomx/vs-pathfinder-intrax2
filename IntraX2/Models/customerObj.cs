﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntraX2.Models
{
    public class customerObj
    {
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string TariffName { get; set; }
        public string PLIRateEffective { get; set; }
        public bool IsRateException { get; set; }
    }
}