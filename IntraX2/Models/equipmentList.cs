﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace IntraX2.Models
{
    public class equipmentList
    {
        public string equipment_id { get; set; }
        public string equipment_description { get; set; }

        public static IEnumerable<equipmentList> GetEquipmentTypes()
        {
            const string cacheKey = "equipmentNames";
            List<equipmentList> equipment = new List<equipmentList>();

            List<equipmentList> equipmentNames = CacheLayer.Get<List<equipmentList>>(cacheKey);

            if (equipmentNames == null)
            {
                string connectionStr = System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ConnectionString;
                using (SqlConnection sqlConn = new SqlConnection(connectionStr))
                using (SqlCommand sqlCmd = sqlConn.CreateCommand())
                {
                    sqlCmd.CommandText = "GetAllEquipmentTypes";  //this is the stored procedure
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlConn.Open();

                    using (SqlDataReader dr = sqlCmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            equipmentList newEquipment = new equipmentList();
                            newEquipment.equipment_id = dr.GetString(0);
                            newEquipment.equipment_description = dr.GetString(1);
                            equipment.Add(newEquipment);
                        }
                    }
                }
                equipmentNames = equipment;
                CacheLayer.Add(equipmentNames, cacheKey);
                return equipmentNames;
                //return equipment;
            }
            else
            {
                return equipmentNames;
            }
        }
    }
}