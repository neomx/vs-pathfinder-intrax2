﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace IntraX2.Models
{
    public class billtoList
    {
        public decimal id { get; set; }
        public string description { get; set; }

        public static IEnumerable<billtoList> GetBillToParty()
        {
            const string cacheKey = "BillToNames";
            List<billtoList> list = new List<billtoList>();

            List<billtoList> billToNames = CacheLayer.Get<List<billtoList>>(cacheKey);
            if (billToNames == null)
            {
                string connectionStr = System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ConnectionString;
                using (SqlConnection sqlConn = new SqlConnection(connectionStr))
                using (SqlCommand sqlCmd = sqlConn.CreateCommand())
                {
                    sqlCmd.CommandText = "GetBillToParty";  //this is the stored procedure
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlConn.Open();

                    using (SqlDataReader dr = sqlCmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            billtoList newBillTo = new billtoList();
                            newBillTo.id = dr.GetDecimal(0);
                            newBillTo.description = dr.GetString(1);

                            list.Add(newBillTo);
                        }
                    }
                }
                billToNames = list;
                CacheLayer.Add(billToNames, cacheKey);

                return billToNames;
                //return list;
            }
            else
            {
                return billToNames;
            }
        }
    }
}