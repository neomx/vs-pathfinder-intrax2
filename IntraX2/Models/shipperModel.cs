﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntraX2.Models
{
    public class shipperModel
    {
        public shipper m_shipper { get; set; }
        public shipper_locations m_shipper_locations { get; set; }
        public shipper_location_contacts m_shipper_location_contacts { get; set; }
        public User m_user { get; set; }
    }
}