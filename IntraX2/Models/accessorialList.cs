﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace IntraX2.Models
{
    public class accessorialList
    {
        public int AccChargeId { get; set; }
        public string AccType { get; set; }
        public decimal Amount { get; set; }

        public static IEnumerable<accessorialList> GetAccessorialServices()
        {
            const string cacheKey = "accessorialNames";
            List<accessorialList> accessorials = new List<accessorialList>();

            List<accessorialList> accessorialNames = CacheLayer.Get<List<accessorialList>>(cacheKey);

            if (accessorialNames == null)
            {
                string connectionStr = System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ConnectionString;
                using (SqlConnection sqlConn = new SqlConnection(connectionStr))
                using (SqlCommand sqlCmd = sqlConn.CreateCommand())
                {
                    sqlCmd.CommandText = "GetAccessorialServices";  //this is the stored procedure
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlConn.Open();

                    using (SqlDataReader dr = sqlCmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            accessorialList newAccessorial = new accessorialList();
                            newAccessorial.AccChargeId = Convert.ToInt16(dr.GetValue(0));
                            newAccessorial.AccType = dr.GetString(1);
                            newAccessorial.Amount = dr.GetDecimal(2);

                            accessorials.Add(newAccessorial);
                        }
                    }
                }
                accessorialNames = accessorials;
                CacheLayer.Add(accessorialNames, cacheKey);
                return accessorialNames;
                //return accessorials;
            }
            else
            {
                return accessorialNames;
            }
        }
    }
}