//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IntraX2.Models
{
    using System;
    
    public partial class GetInvoiceBillTo_Result5
    {
        public string pli_book_number { get; set; }
        public string customer_name { get; set; }
        public string address { get; set; }
        public string location { get; set; }
        public string customer_terms { get; set; }
        public Nullable<System.DateTime> customer_invoice_date { get; set; }
        public Nullable<System.DateTime> customer_due_date { get; set; }
        public Nullable<decimal> customer_rate { get; set; }
        public Nullable<decimal> customer_fsc { get; set; }
        public Nullable<bool> additional_ins { get; set; }
        public Nullable<decimal> additional_ins_amt { get; set; }
        public Nullable<decimal> customer_accessorials { get; set; }
        public Nullable<decimal> customer_charge { get; set; }
        public string client_ref_number { get; set; }
    }
}
