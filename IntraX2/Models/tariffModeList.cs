﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace IntraX2.Models
{
    public class tariffModeList
    {
        public string CodeId { get; set; }
        public string Description { get; set; }

        public static IEnumerable<tariffModeList> GetModeList()
        {
            const string cacheKey = "TariffModeList";
            List<tariffModeList> modelist = new List<tariffModeList>();

            List<tariffModeList> modeList = CacheLayer.Get<List<tariffModeList>>(cacheKey);
            if (modeList == null)
            {
                string connectionStr = System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ConnectionString;
                using (SqlConnection sqlConn = new SqlConnection(connectionStr))
                using (SqlCommand sqlCmd = sqlConn.CreateCommand())
                {
                    sqlCmd.CommandText = "GetAdminCodeDescriptionsByType";  //this is the stored procedure
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.Parameters.Add("@strType", SqlDbType.NVarChar).Value = "TRMOD";
                    sqlConn.Open();
                    using (SqlDataReader dr = sqlCmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            tariffModeList newType = new tariffModeList();
                            newType.CodeId = Convert.ToString(dr.GetValue(0));
                            newType.Description = dr.GetString(1);

                            modelist.Add(newType);
                        }
                    }
                }
                modeList = modelist;
                CacheLayer.Add(modeList, cacheKey);
                return modeList;
            }
            else
            {
                return modeList;
            }
        }
    }
}