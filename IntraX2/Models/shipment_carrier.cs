//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IntraX2.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class shipment_carrier
    {
        public string pli_book_number { get; set; }
        public decimal carrier_id { get; set; }
        public string role_id { get; set; }
        public string pro_number { get; set; }
        public Nullable<decimal> charges { get; set; }
        public string transit_days { get; set; }
        public Nullable<System.DateTime> invoiced_date { get; set; }
        public string dispatched_date_time { get; set; }
        public string notes { get; set; }
        public int sequence { get; set; }
        public string service_point { get; set; }
        public Nullable<bool> invoice_received { get; set; }
        public Nullable<int> location_id { get; set; }
        public Nullable<int> location_contact_id { get; set; }
        public string record_state { get; set; }
    }
}
