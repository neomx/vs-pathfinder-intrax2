//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IntraX2.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class pflinQuoteTable
    {
        public int QuoteId { get; set; }
        public string CustomerName { get; set; }
        public string ContactName { get; set; }
        public string ContactEmail { get; set; }
        public string OriginZip { get; set; }
        public string DestinationZip { get; set; }
        public decimal TotalGrossCharge { get; set; }
        public decimal DiscountAmount { get; set; }
        public decimal GrossCharge { get; set; }
        public decimal FuelSurcharge { get; set; }
        public decimal TotalEstimatedCharges { get; set; }
        public bool ShipIt { get; set; }
        public System.DateTime InsertDate { get; set; }
    }
}
