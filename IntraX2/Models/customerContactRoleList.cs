﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace IntraX2.Models
{
    public class customerContactRoleList
    {
        public int CodeId { get; set; }
        public string Description { get; set; }

        public static IEnumerable<customerContactRoleList> GetCustomerContactRoleList()
        {
            const string cacheKey = "RoleNames";
            List<customerContactRoleList> roles = new List<customerContactRoleList>();

            List<customerContactRoleList> roleNames = CacheLayer.Get<List<customerContactRoleList>>(cacheKey);

            if (roleNames == null)
            {
                string connectionStr = System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ConnectionString;
                using (SqlConnection sqlConn = new SqlConnection(connectionStr))
                using (SqlCommand sqlCmd = sqlConn.CreateCommand())
                {
                    sqlCmd.CommandText = "GetAdminCodeDescriptionsByType";  //this is the stored procedure
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.Parameters.Add("@strType", SqlDbType.NVarChar).Value = "CUSCT";
                    sqlConn.Open();

                    using (SqlDataReader dr = sqlCmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            customerContactRoleList newRole = new customerContactRoleList();
                            newRole.CodeId = Convert.ToInt16(dr.GetValue(0));
                            newRole.Description = dr.GetString(1);
                            roles.Add(newRole);
                        }
                    }
                }
                roleNames = roles;
                CacheLayer.Add(roleNames, cacheKey);
                return roleNames;
            }
            else
            {
                return roleNames;
            }
        }
    }
}