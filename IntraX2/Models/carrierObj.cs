﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntraX2.Models
{
    public class carrierObj
    {
        public int CarrierId { get; set; }
        public string TariffName { get; set; }
        public string PLIRateEffective { get; set; }
    }
}