//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IntraX2.Models
{
    using System;
    
    public partial class GetNMFCCategory_Result
    {
        public decimal category_id { get; set; }
        public string category { get; set; }
    }
}
