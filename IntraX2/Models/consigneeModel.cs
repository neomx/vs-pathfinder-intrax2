﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntraX2.Models
{
    public class consigneeModel
    {
        public consignee m_consignee { get; set; }
        public consignee_locations m_consignee_locations { get; set; }
        public consignee_location_contacts m_consignee_location_contacts { get; set; }
    }
}