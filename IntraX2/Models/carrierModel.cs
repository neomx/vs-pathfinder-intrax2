﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntraX2.Models
{
    public class carrierModel
    {
        public carrier m_carrier { get; set; }
        public carrier_locations m_carrier_locations { get; set; }
        public carrier_location_contacts m_carrier_location_contacts { get; set; }
    }
}