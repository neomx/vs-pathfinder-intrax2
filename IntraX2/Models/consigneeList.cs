﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace IntraX2.Models
{
    public class consigneeList
    {
        public decimal consignee_id { get; set; }
        public string name { get; set; }

        public static IEnumerable<consigneeList> GetAllConsignees()
        {
            const string cacheKey = "ConsigneeNames";
            List<consigneeList> consignees = new List<consigneeList>();

            List<consigneeList> consigneeNames = CacheLayer.Get<List<consigneeList>>(cacheKey);

            if (consigneeNames == null)
            {
                string connectionStr = System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ConnectionString;
                using (SqlConnection sqlConn = new SqlConnection(connectionStr))
                using (SqlCommand sqlCmd = sqlConn.CreateCommand())
                {
                    sqlCmd.CommandText = "GetAllConsignees";  //this is the stored procedure
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlConn.Open();

                    using (SqlDataReader dr = sqlCmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            consigneeList newConsignee = new consigneeList();
                            newConsignee.consignee_id = dr.GetDecimal(0);
                            newConsignee.name = dr.GetString(1);

                            consignees.Add(newConsignee);
                        }
                    }
                }
                consigneeNames = consignees;
                CacheLayer.Add(consigneeNames, cacheKey);
                return consigneeNames;
                //return consignees;
            }
            else
            {
                return consigneeNames;
            }
        }
    }
}