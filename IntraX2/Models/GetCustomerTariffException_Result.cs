//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IntraX2.Models
{
    using System;
    
    public partial class GetCustomerTariffException_Result
    {
        public string From_country { get; set; }
        public string To_country { get; set; }
        public string ModuleName { get; set; }
        public System.DateTime PLIRateEffDate { get; set; }
    }
}
