﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntraX2.Models
{
    public class shipmentModel
    {
        public shipment m_shipment { get; set; }
        public shipment_items m_shipment_item { get; set; }
        public shipment_shipper m_shipment_shipper { get; set; }
        public shipment_consignee m_shipment_consignee { get; set; }
        public shipment_carrier m_shipment_carrier { get; set; }
    }
}