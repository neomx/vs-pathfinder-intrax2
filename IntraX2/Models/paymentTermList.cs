﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace IntraX2.Models
{
    public class paymentTermList
    {
        public string CodeId { get; set; }
        public string Description { get; set; }

        public static IEnumerable<paymentTermList> GetPaymentTerms()
        {
            const string cacheKey = "PaymentTermsList";
            List<paymentTermList> termslist = new List<paymentTermList>();

            List<paymentTermList> paymentTerms = CacheLayer.Get<List<paymentTermList>>(cacheKey);

            if (paymentTerms == null)
            {
                string connectionStr = System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ConnectionString;
                using (SqlConnection sqlConn = new SqlConnection(connectionStr))
                using (SqlCommand sqlCmd = sqlConn.CreateCommand())
                {
                    sqlCmd.CommandText = "GetAdminCodeDescriptionsByType";  //this is the stored procedure
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.Parameters.Add("@strType", SqlDbType.NVarChar).Value = "PAYTRM";
                    sqlConn.Open();

                    using (SqlDataReader dr = sqlCmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            paymentTermList newType = new paymentTermList();
                            newType.CodeId = Convert.ToString(dr.GetValue(0));
                            newType.Description = dr.GetString(1);

                            termslist.Add(newType);
                        }
                    }
                }
                paymentTerms = termslist;
                CacheLayer.Add(paymentTerms, cacheKey);
                return paymentTerms;
            }
            else
            {
                return paymentTerms;
            }
        }
    }
}