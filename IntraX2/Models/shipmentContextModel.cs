﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Objects;

namespace IntraX2.Models
{
    public class shipmentContextModel : DbContext
    {
        public DbSet<shipment> Shipments { get; set; }
        public DbSet<shipment_items> Shipment_Items { get; set; }
    }
}