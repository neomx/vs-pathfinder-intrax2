﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace IntraX2.Models
{
    public class customerList
    {
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }

        public static IEnumerable<customerList> GetAllCustomers()
        {
            const string cacheKey = "CustomerNames";
            List<customerList> customers = new List<customerList>();

            List<customerList> customerNames = CacheLayer.Get<List<customerList>>(cacheKey);
            if (customerNames == null)
            {
                /*
                StreamWriter log;
                log = new StreamWriter(@"C:\Temp\" + DateTime.Now.ToString("yyyyMMddHHmmssFFF_") + "first.txt");
                log.Close();*/
                
                string connectionStr = System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ConnectionString;
                using (SqlConnection sqlConn = new SqlConnection(connectionStr))
                using (SqlCommand sqlCmd = sqlConn.CreateCommand())
                {
                    sqlCmd.CommandText = "GetAllCustomers";  //this is the stored procedure
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlConn.Open();

                    using (SqlDataReader dr = sqlCmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            customerList newCustomer = new customerList();
                            newCustomer.CustomerId = Convert.ToInt32(dr.GetValue(0));
                            newCustomer.CustomerName = dr.GetString(1);

                            customers.Add(newCustomer);
                        }
                    }
                }
                customerNames = customers;
                CacheLayer.Add(customerNames, cacheKey);

                return customerNames;
                //return customers;
            }
            else
            {
                /*
                StreamWriter log;
                log = new StreamWriter(@"C:\Temp\" + DateTime.Now.ToString("yyyyMMddHHmmssFFF_") + "second.txt");
                log.Close();
                 */
                return customerNames;
            }
        }
    }
}