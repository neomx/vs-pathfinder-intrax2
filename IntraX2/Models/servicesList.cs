﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace IntraX2.Models
{
    public class servicesList
    {
        public string service { get; set; }
        public int id { get; set; }

        public static IEnumerable<servicesList> GetSpecialServices()
        {
            List<servicesList> services = new List<servicesList>();
            
            string connectionStr = System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ConnectionString;
            using (SqlConnection sqlConn = new SqlConnection(connectionStr))
            using (SqlCommand sqlCmd = sqlConn.CreateCommand())
            {
                sqlCmd.CommandText = "GetSpecialServices";  //this is the stored procedure
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlConn.Open();

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        servicesList newService = new servicesList();
                        newService.id = Convert.ToInt16(dr.GetValue(0));
                        newService.service = dr.GetString(1);
                        
                        services.Add(newService);
                    }
                }
            }
            return services;
        }
    }
}