//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IntraX2.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class pflinQuoteItemsTable
    {
        public int QuoteItemsId { get; set; }
        public string Class { get; set; }
        public string Weight { get; set; }
        public decimal Charge { get; set; }
        public System.DateTime InsertDate { get; set; }
        public int QuoteId { get; set; }
    }
}
