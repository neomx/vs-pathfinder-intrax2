﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace IntraX2.Models
{
    public class countryCodeList
    {
        public string CountryCode { get; set; }
        public string CountryName { get; set; }

        public static IEnumerable<countryCodeList> GetCountries()
        {
            const string cacheKey = "CountryList";
            List<countryCodeList> countrylist = new List<countryCodeList>();

            List<countryCodeList> countryNames = CacheLayer.Get<List<countryCodeList>>(cacheKey);

            if (countryNames == null)
            {
                string connectionStr = System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ConnectionString;
                using (SqlConnection sqlConn = new SqlConnection(connectionStr))
                using (SqlCommand sqlCmd = sqlConn.CreateCommand())
                {
                    sqlCmd.CommandText = "GetCountryCodeList";  //this is the stored procedure
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlConn.Open();

                    using (SqlDataReader dr = sqlCmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            countryCodeList newCountry = new countryCodeList();
                            newCountry.CountryCode = Convert.ToString(dr.GetValue(0));
                            newCountry.CountryName = dr.GetString(1);

                            countrylist.Add(newCountry);
                        }
                    }
                }
                countryNames = countrylist;
                CacheLayer.Add(countryNames, cacheKey);
                return countryNames;
            }
            else
            {
                return countryNames;
            }
        }
    }
}