﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace IntraX2.Models
{
    public class modeList
    {
        public decimal mode_id { get; set; }
        public string mode_description { get; set; }

        public static IEnumerable<modeList> GetShipmentModes()
        {
            const string cacheKey = "ModeNames";
            List<modeList> modes = new List<modeList>();

            List<modeList> modeNames = CacheLayer.Get<List<modeList>>(cacheKey);

            if (modeNames == null)
            {
                string connectionStr = System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ConnectionString;
                using (SqlConnection sqlConn = new SqlConnection(connectionStr))
                using (SqlCommand sqlCmd = sqlConn.CreateCommand())
                {
                    sqlCmd.CommandText = "GetShipmentMode";  //this is the stored procedure
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlConn.Open();

                    using (SqlDataReader dr = sqlCmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            modeList newMode = new modeList();
                            newMode.mode_id = dr.GetDecimal(0);
                            newMode.mode_description = dr.GetString(1);

                            modes.Add(newMode);
                        }
                    }
                }
                modeNames = modes;
                CacheLayer.Add(modeNames, cacheKey);
                return modeNames;
                //return modes;
            }
            else
            {
                return modeNames;
            }
        }
    }
}