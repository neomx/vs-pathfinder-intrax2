﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace IntraX2.Models
{
    public class customerContactTypeList
    {
        public string CodeId { get; set; }
        public string Description { get; set; }

        public static IEnumerable<customerContactTypeList> GetContactTypes()
        {
            const string cacheKey = "ContactTypeList";
            List<customerContactTypeList> typelist = new List<customerContactTypeList>();

            List<customerContactTypeList> typeList = CacheLayer.Get<List<customerContactTypeList>>(cacheKey);

            if (typeList == null)
            { 
                string connectionStr = System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ConnectionString;
                using (SqlConnection sqlConn = new SqlConnection(connectionStr))
                using (SqlCommand sqlCmd = sqlConn.CreateCommand())
                {
                    sqlCmd.CommandText = "GetAdminCodeDescriptionsByType";  //this is the stored procedure
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.Parameters.Add("@strType", SqlDbType.NVarChar).Value = "CUSCT";
                    sqlConn.Open();

                    using (SqlDataReader dr = sqlCmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            customerContactTypeList newType = new customerContactTypeList();
                            newType.CodeId = Convert.ToString(dr.GetValue(0));
                            newType.Description = dr.GetString(1);

                            typelist.Add(newType);
                        }
                    }
                }
                typeList = typelist;
                CacheLayer.Add(typeList, cacheKey);
                return typeList;
            }
            else
            {
                return typeList;
            }
        }
    }
}