﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace IntraX2.Models
{
    public class countryList
    {
        public int CountryId { get; set; }
        public string CountryName { get; set; }

        public static IEnumerable<countryList> GetCountryList()
        {
            const string cacheKey = "Countries";
            List<countryList> list = new List<countryList>();

            List<countryList> countries = CacheLayer.Get<List<countryList>>(cacheKey);
            if (countries == null)
            { 
                string connectionStr = System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ConnectionString;
                using (SqlConnection sqlConn = new SqlConnection(connectionStr))
                using (SqlCommand sqlCmd = sqlConn.CreateCommand())
                {
                    sqlCmd.CommandText = "GetCountryCodeList";  //this is the stored procedure
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlConn.Open();

                    using (SqlDataReader dr = sqlCmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            countryList newCountry = new countryList();
                            newCountry.CountryId = dr.GetInt32(0);
                            newCountry.CountryName = dr.GetString(1);

                            list.Add(newCountry);
                        }
                        
                        //add [Add New] option to list
                        countryList newCountryAdd = new countryList();
                        newCountryAdd.CountryId = 0;
                        newCountryAdd.CountryName = "[Add New]";

                        list.Add(newCountryAdd);
                    }
                }
                countries = list;
                CacheLayer.Add(countries, cacheKey);

                return countries;
            }
            else
            {
                return countries;
            }
        }
    }
}