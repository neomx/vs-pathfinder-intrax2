﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;
using System.ComponentModel;
using System.Data.SqlClient;

namespace IntraX2.Models
{
    public class Login
    {
        [Required(ErrorMessage = "User ID Required")]
        [Display(Name = "User ID")]
        public string UserID { get; set; }

        [Required(ErrorMessage = "Password Required")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string UserPassword { get; set; }

        public Int16 UserRoleID { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }

        public Int16 LoginID { get; set; }

        /*
        public int CustomerID { get; set; }
        public string CustomerRole { get; set; }
        public string CustomerName { get; set; }
        public int CustomerLocationID { get; set; }
        public int CustomerContactID { get; set; }
        public decimal CustomerIntraStateDiscount { get; set; }
        public decimal CustomerIntraStateMinCharge { get; set; }
        public decimal CustomerInterStateDiscount { get; set; }
        public decimal CustomerInterStateMinCharge { get; set; }
        public string CustomerRateEffectiveDateStr { get; set; }
        public string CustomerTariffName { get; set; }
        */ 

        public bool IsUserExist(string userid, string password)
        {
            bool flag = false;
            SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ConnectionString);
            connection.Open();
            SqlCommand command = new SqlCommand("select UserName, PflinUserID, PflinRoleID from PflinUser where UserName='" + userid + "' and Password='" + password + "'", connection);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                UserID = Convert.ToString(reader.GetValue(0)).ToLower();
                LoginID = Convert.ToInt16(reader.GetValue(1));
                UserRoleID = Convert.ToInt16(reader.GetValue(2));
                flag = true;
            }
            else
            {
                flag = false;
            }
            connection.Close();
            return flag;
        }
    }
    
    public class UsersContext : DbContext
    {
        public UsersContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<UserProfile> UserProfiles { get; set; }
    }

    [Table("UserProfile")]
    public class UserProfile
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        public string UserName { get; set; }
    }

    public class RegisterExternalLoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        public string ExternalLoginData { get; set; }
    }

    public class LocalPasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class ExternalLogin
    {
        public string Provider { get; set; }
        public string ProviderDisplayName { get; set; }
        public string ProviderUserId { get; set; }
    }
}
