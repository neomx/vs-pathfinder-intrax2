﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace IntraX2.Models
{
    public class invoiceTypeList
    {
        public string CodeId { get; set; }
        public string Description { get; set; }

        public static IEnumerable<invoiceTypeList> GetInvoiceTypes()
        {
            const string cacheKey = "InvoiceTypeList";
            List<invoiceTypeList> invoicelist = new List<invoiceTypeList>();

            List<invoiceTypeList> invoiceTypes = CacheLayer.Get<List<invoiceTypeList>>(cacheKey);

            if (invoiceTypes == null)
            {
                string connectionStr = System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ConnectionString;
                using (SqlConnection sqlConn = new SqlConnection(connectionStr))
                using (SqlCommand sqlCmd = sqlConn.CreateCommand())
                {
                    sqlCmd.CommandText = "GetAdminCodeDescriptionsByType";  //this is the stored procedure
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.Parameters.Add("@strType", SqlDbType.NVarChar).Value = "INVPRF";
                    sqlConn.Open();

                    using (SqlDataReader dr = sqlCmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            invoiceTypeList newType = new invoiceTypeList();
                            newType.CodeId = Convert.ToString(dr.GetValue(0));
                            newType.Description = dr.GetString(1);

                            invoicelist.Add(newType);
                        }
                    }
                }
                invoiceTypes = invoicelist;
                CacheLayer.Add(invoiceTypes, cacheKey);
                return invoiceTypes;
            }
            else
            {
                return invoiceTypes;
            }
        }
    }
}