﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace IntraX2.Models
{
    public class shipperList
    {
        public decimal shipper_id { get; set; }
        public string name { get; set; }

        public static IEnumerable<shipperList> GetAllShippers()
        {
            const string cacheKey = "ShipperNames";
            List<shipperList> shippers = new List<shipperList>();

            List<shipperList> shipperNames = CacheLayer.Get<List<shipperList>>(cacheKey);

            if (shipperNames == null)
            {
                string connectionStr = System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ConnectionString;
                using (SqlConnection sqlConn = new SqlConnection(connectionStr))
                using (SqlCommand sqlCmd = sqlConn.CreateCommand())
                {
                    sqlCmd.CommandText = "GetAllShippers";  //this is the stored procedure
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlConn.Open();

                    using (SqlDataReader dr = sqlCmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            shipperList newShipper = new shipperList();
                            newShipper.shipper_id = dr.GetDecimal(0);
                            newShipper.name = dr.GetString(1);

                            shippers.Add(newShipper);
                        }
                    }
                }
                shipperNames = shippers;
                CacheLayer.Add(shipperNames, cacheKey);
                return shipperNames;
                //return shippers;
            }
            else
            {
                return shipperNames;
            }
        }
    }
}