﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntraX2.Models
{
    public class invoiceSequence
    {
        public string pli_book_number { get; set; }
        public Int16 invoice_sequence { get; set; }
        public bool select { get; set; }
        public string bill_to_id { get; set; }
        public Int32 bill_to_customer_id { get; set; }
        public Int32 bill_party_id { get; set; }
        public Int16 bill_party_location_id { get; set; }
    }
}