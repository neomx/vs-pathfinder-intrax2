﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace IntraX2.Models
{
    public class invoiceCycleList
    {
        public string CodeId { get; set; }
        public string Description { get; set; }

        public static IEnumerable<invoiceCycleList> GetInvoiceCycle()
        {
            const string cacheKey = "InvoiceCycleList";
            List<invoiceCycleList> cyclelist = new List<invoiceCycleList>();

            List<invoiceCycleList> invoiceCycle = CacheLayer.Get<List<invoiceCycleList>>(cacheKey);

            if (invoiceCycle == null)
            {
                string connectionStr = System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ConnectionString;
                using (SqlConnection sqlConn = new SqlConnection(connectionStr))
                using (SqlCommand sqlCmd = sqlConn.CreateCommand())
                {
                    sqlCmd.CommandText = "GetAdminCodeDescriptionsByType";  //this is the stored procedure
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.Parameters.Add("@strType", SqlDbType.NVarChar).Value = "INVCYL";
                    sqlConn.Open();
                    using (SqlDataReader dr = sqlCmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            invoiceCycleList newType = new invoiceCycleList();
                            newType.CodeId = Convert.ToString(dr.GetValue(0));
                            newType.Description = dr.GetString(1);

                            cyclelist.Add(newType);
                        }
                    }
                }
                invoiceCycle = cyclelist;
                CacheLayer.Add(invoiceCycle, cacheKey);
                return invoiceCycle;
            }
            else
            {
                return invoiceCycle;
            }
        }
    }
}