//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IntraX2.Models
{
    using System;
    
    public partial class GetShipmentConfirmationShipper_Result2
    {
        public string pli_book_number { get; set; }
        public string shipper_name { get; set; }
        public Nullable<System.DateTime> actual_pu_date { get; set; }
        public string reference_number { get; set; }
        public string address { get; set; }
        public string location { get; set; }
        public string contact { get; set; }
        public string fax_number { get; set; }
    }
}
