//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IntraX2.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class WebUser
    {
        public decimal WebUserID { get; set; }
        public int WebRoleID { get; set; }
        public string user_id { get; set; }
        public Nullable<int> customer_id { get; set; }
        public Nullable<int> seq_number { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
    }
}
