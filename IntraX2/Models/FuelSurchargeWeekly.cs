//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IntraX2.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class FuelSurchargeWeekly
    {
        public int FscWeeklyId { get; set; }
        public System.DateTime WeekBeginningDate { get; set; }
        public decimal DOE_US_NatlAvgDieselPrice { get; set; }
        public decimal FscPercent { get; set; }
        public int CreatedById { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> UpdatedById { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
    }
}
