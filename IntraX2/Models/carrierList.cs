﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;


namespace IntraX2.Models
{
    public class carrierList
    {
        //public decimal CarrierID { get; set; }
        public int CarrierID { get; set; }
        public string CarrierName { get; set; }

        public static IEnumerable<carrierList> GetAllCarriers()
        {
            const string cacheKey = "CarrierNames";
            List<carrierList> carriers = new List<carrierList>();

            List<carrierList> carrierNames = CacheLayer.Get<List<carrierList>>(cacheKey);

            if (carrierNames == null)
            {
                string connectionStr = System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ConnectionString;
                using (SqlConnection sqlConn = new SqlConnection(connectionStr))
                using (SqlCommand sqlCmd = sqlConn.CreateCommand())
                {
                    sqlCmd.CommandText = "basp_GetCarrierListData";  //this is the stored procedure
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlConn.Open();

                    using (SqlDataReader dr = sqlCmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            carrierList newCarrier = new carrierList();
                            newCarrier.CarrierID = Convert.ToInt32(dr.GetValue(0));
                            newCarrier.CarrierName = dr.GetString(1);

                            carriers.Add(newCarrier);
                        }
                    }
                }
                carrierNames = carriers;
                CacheLayer.Add(carrierNames, cacheKey);
                return carrierNames;
                //return carriers;
            }
            else
            {
                return carrierNames;
            }
        }
    }
}