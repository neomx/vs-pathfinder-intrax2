﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntraX2.Models
{
    public class carrierRow
    {
        public string pli_book_number { get; set; }
        public string role_id { get; set; }
        public string carrier_id { get; set; }
        public string service_point { get; set; }
        public string pro_number { get; set; }
        public string carrier_charges { get; set; }
        public string transit_days { get; set; }
        public string invoiced_date { get; set; }
        public string dispatched_date_time { get; set; }
        public string notes { get; set; }
        public string carrier_sequence { get; set; }
    }
}