//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IntraX2.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class BOLTempParty
    {
        public string BookingNumber { get; set; }
        public Nullable<int> CreatedByWebUserID { get; set; }
        public string PartyTypeCode { get; set; }
        public string CompanyName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string StateCode { get; set; }
        public string Zip { get; set; }
        public string ContactName { get; set; }
        public string Phone { get; set; }
        public string PhoneExtension { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
    }
}
