﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace IntraX2.Models
{
    public class salesRepList
    {
        public int SalesRepID { get; set; }
        public string SalesRepAlias { get; set; }

        public static IEnumerable<salesRepList> GetAllSalesReps()
        {
            const string cacheKey = "SalesRepAlias";
            List<salesRepList> salesreps = new List<salesRepList>();

            List<salesRepList> salesRepNames = CacheLayer.Get<List<salesRepList>>(cacheKey);

            if (salesRepNames == null)
            { 
                string connectionStr = System.Configuration.ConfigurationManager.ConnectionStrings["pathfinderDB"].ConnectionString;
                using (SqlConnection sqlConn = new SqlConnection(connectionStr))
                using (SqlCommand sqlCmd = sqlConn.CreateCommand())
                {
                    sqlCmd.CommandText = "GetSalesRepList";  //this is the stored procedure
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlConn.Open();

                    using (SqlDataReader dr = sqlCmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            salesRepList newSalesRep = new salesRepList();
                            newSalesRep.SalesRepID = Convert.ToInt32(dr.GetValue(0));
                            newSalesRep.SalesRepAlias = dr.GetString(1);

                            salesreps.Add(newSalesRep);
                        }
                    }
                }
                salesRepNames = salesreps;
                CacheLayer.Add(salesRepNames, cacheKey);
                return salesRepNames;
            }
            else
            {
                return salesRepNames;
            }
        }
    }
}