﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntraX2.Models
{
    public class shipmentShipperObj
    {
        public string PLIBookingNumber { get; set; }
        public decimal ShipperId { get; set; }
        public Int16 LocationID { get; set; }
        public decimal ContactID { get; set; }
        public string Reference { get; set; }
        public DateTime PUDate { get; set; }
        public int Sequence { get; set; }
        public string Zip { get; set; }
        public string State { get; set; }
    }
}